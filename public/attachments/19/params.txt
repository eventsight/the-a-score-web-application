 $om =   \Magento\Framework\App\ObjectManager::getInstance();
            $cartData = $om->create('Magento\Checkout\Model\Cart')->getQuote();
            $quote = $om->create('\Magento\Quote\Model\Quote');
            $quote->load($cartData->getId());      
            $quote->getPayment()->setMethod('sample_gateway'); // Todo: Will Appear MOLPay Seamless

            $cartManagement = $om->create('\Magento\Quote\Api\CartManagementInterface'); 
            $order = $cartManagement->submit($quote);
            $merchantid = $this->_objectManager->create('Rev\RevPay\Helper\Data')->getMerchantID();
            $vkey = $this->_objectManager->create('Rev\RevPay\Helper\Data')->getVerifyKey();
            
            $base_url = $this->_objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore()->getBaseUrl();
        

        // Call PxOrder.Initialize8
        $params = [
            'ResponseURL' => '',
            'BackgroundURL' => '',
            'MerchantId' => $merchantid,
            'TxnModel' => "01",
            'RefNo' =>  $order->getId(),
            'TxnCur' => "MYR",
            'TxnAmt' =>$order->getGrandTotal(),
            'TxnDesc' => "\n-- Order Detail --",
            'CustName' =>$order->getCustomerFirstName()." ".$order->getCustomerLastName(),
            'CustEmail' =>$order->getCustomerEmail(),
            'CustContact' => $order->getTelephone(),
            'Lang' => "UTF-8",
            'Version' => "1.00",
           
        ];
            print_r($params);