function paypal_init(token , price , id, redirectL){
$('#paypal-loading').hide();
    // Fetch the button you are using to initiate the PayPal flow
var paypalButton = document.getElementById('paypal-button');

// Create a Client component

braintree.client.create({
  authorization: token
}, function (clientErr, clientInstance) {
  // Create PayPal component
  braintree.paypal.create({
    client: clientInstance
  }, function (err, paypalInstance) {
    paypalButton.addEventListener('click', function () {
      // Tokenize here!
      $('#paypal-button').hide();
      $('#paypal-loading').fadeIn();
      paypalInstance.tokenize({
        flow: 'checkout', // Required
        amount: price, // Required
        currency: 'USD', // Required
        locale: 'en_US',
        enableShippingAddress: false,
      }, function (err, tokenizationPayload) {
        // Tokenization complete
        // Send tokenizationPayload.nonce to server
        if(err){
              $('#paypal-button').fadeIn();
              $('#paypal-loading').hide();
        }else{
                    var settings = {
                      "async": true,
                      "crossDomain": true,
                      "url": "/paypal_trans",
                      "method": "POST",
                      "headers": {
                        "content-type": "application/x-www-form-urlencoded",
                        "cache-control": "no-cache",
                        "postman-token": "f313b3be-f5e6-1eed-fe48-2f7dba7575d7"
                      },
                      "data": {
                        "nonce": tokenizationPayload.nonce,
                        "amount": price,
                        "order_id": id
                      }
                    }

                    $.ajax(settings).done(function (response) {
                      response = JSON.parse(response);
                      if(response.success){
                        console.log("Success!");
                        if(redirectL !== 'undefined'){
                          window.location.href = redirectL;
                        }else{
                          location.reload();
                        }
                      }else{
                          $('#paypal-button').fadeIn();
                          $('#paypal-loading').hide();
                      }
                    });
        }

      });
    });
  });
});
}
