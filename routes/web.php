<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::post('paypal_status','OrderController@paypalStatus');
Route::get('paypal_invoice/{id}' , 'OrderController@paypalInvoice');


Route::get('register/verify/{token}', 'Auth\RegisterController@verify');

Route::get('/getrate/{service_id}/{urgency_id}', 'RateController@getrate');
Route::get('/faq' , 'HomeController@faq');
Route::get('/email','HomeController@email');

Route::group(['middleware' => ['web', 'auth']], function () {

	Route::get('/home', 'HomeController@index');
	Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
	Route::post('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);

	Route::get('/user/{id}','HomeController@userprofile'); 	
	Route::get('/placeorder', 'OrderController@placeorder');
	
	Route::post('imageUploadForm', 'HomeController@upload_photo' )->name('users.uploadpic');

	Route::get('/active', 'OrderController@active');
	Route::get('/completed', 'OrderController@completed');
	Route::get('/revision', 'OrderController@revision'); 
	Route::get('/shoppingcart', 'OrderController@shoppingcart');
	Route::get('/payment', 'OrderController@payment');

	Route::get('/dashboard-writer', 'HomeController@writer');

	Route::get('/changetopaid/{id}', 'OrderController@changetopaid')->name('orders.changetopaid');
	Route::get('/vieworder/{id}', 'OrderController@vieworder')->name('orders.vieworder');
	Route::get('/uploaddoc/{id}', 'OrderController@uploaddoc')->name('orders.uploaddoc');
	Route::get('/payorder/{id}', 'OrderController@payorder')->name('orders.payorder');
	Route::get('/cancel/{id}','OrderController@cancelorder')->name('orders.cancelorder');
	Route::post('/messeges/upload', 'MessageController@upload')->name('messages.upload');
	Route::get('/orders/{id}/download' , 'OrderController@download') ->name('orders.downloads');
	Route::get('/orders/{id}/in_revision' , 'OrderController@in_revision') ->name('orders.in_revision');
	Route::get('/orders/{id}/complete' , 'OrderController@complete') ->name('orders.complete');
	Route::resource("orders","OrderController");

	Route::get('/ordersstatus/{id}', 'OrderController@status');
	Route::get('/ordersstatus/', 'OrderController@searchstatus');

	//writers
	Route::get('/job/active' , 'WriterController@active'); 
	Route::get('/job/pending' , 'WriterController@pendingjob'); 
	Route::get('/job/completed' , 'WriterController@completed'); 
	Route::get('/job/revision' , 'WriterController@revision'); 
	Route::get('/writer/messages' , 'MessageController@writer'); 
	Route::get('/job' , 'WriterController@writer_view'); 
	
	Route::post('/job/uploaddoc' , 'WriterController@upload_final_doc') ->name('writers.upload_doc');
	Route::get('/job/{id}', 'WriterController@vieworder')->name('writers.vieworder');
	Route::get('/job/{id}/apply', 'WriterController@applyjob')->name('writers.applyjob');

	Route::get('/orders/message/{id}','MessageController@read')->name('messages.read');

});

Route::group(['middleware' => ['App\Http\Middleware\admin' ]] , function () {

	Route::get('/dashboard-admin', 'HomeController@admin');

	Route::resource("services","ServiceController");
	Route::resource("urgencies","UrgencyController");
	Route::resource("levels","LevelController");
	Route::resource("spacings","SpacingController");
	Route::resource("pages","PageController");
	Route::resource("citations","CitationController");
	Route::resource("sources","SourceController");
	Route::resource("statuses","StatusController");
	Route::resource("rates","RateController");
	Route::resource("messages","MessageController");


	//admin
	Route::get('/users' , 'HomeController@user');
  	Route::get('/users_download','HomeController@users_download');
  	Route::get('/users/filterby' , 'HomeController@filterby') ->name('admins.searchrole'); 
  	Route::get('/users/searchname' , 'HomeController@searchname') ->name('admins.searchname'); 
	Route::get('/users/{id}/{type?}' , 'HomeController@change_user') ->name('admins.change_user');

	//export 
	Route::get('/export','OrderController@export');
	
	Route::get('/messages/{id}/approve_message' , 'MessageController@approve') ->name('messages.approve');
	Route::get('/messages/{id}/decline_message' , 'MessageController@decline') ->name('messages.decline');

	Route::get('/add_writer' , 'WriterController@registerform');
	Route::post('/register_writer' , 'WriterController@register_writer');
	
	Route::get('/orders/approve/{id}/{writer_id}' , 'WriterController@approvejob') ->name('orders.approvejob');
	Route::get('/orders/decline/{id}/{writer_id}' , 'WriterController@decline') ->name('orders.decline');

	Route::get('/orders/{id}/cancel_writer' , 'OrderController@cancel_writer') ->name('orders.cancel_writer');
	Route::get('/orders/{id}/assign_writer' , 'OrderController@assign_writer') ->name('orders.assign_writer'); 
	Route::get('/orders/{id}/approve_doc' , 'OrderController@approve_doc') ->name('orders.approve_doc');
	Route::get('/orders/{id}/reject_doc' , 'OrderController@reject_doc') ->name('orders.reject_doc');
	Route::get('/orders/approve/{id}/{writer_id}' , 'WriterController@approvejob') ->name('orders.approvejob');
	Route::get('/orders/decline/{id}/{writer_id}' , 'WriterController@decline') ->name('orders.decline');

});
