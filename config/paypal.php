<?php
return array(
    // set your paypal credential
  'client_id' => 'AQDSF0QgUekLT9aSTXKUOrD-YhT64f5pF_F7KdNEPUex4QmEu_FNR8ugfNBQ3Ckwm0A7VvyRKEJCo2kL',
    'secret' => 'EBP6ylDv4_lUhKuso8zTe1ClSwjYyDHNBuGdz8mOZyoYa275wUa9EdAkVCmmXRFwuPf2Wl_vzkycf43Q',
    /**
     * SDK configuration 
     */
    'settings' => array(
        /**
         * Available option 'sandbox' or 'live'
         */
        'mode' => 'sandbox',
        /**
         * Specify the max request time in seconds
         */
        'http.ConnectionTimeOut' => 30,
        /**
         * Whether want to log to a file
         */
        'log.LogEnabled' => true,
        /**
         * Specify the file that want to write on
         */
        'log.FileName' => storage_path() . '/logs/paypal.log',
        /**
         * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the 'FINE' level and decreases as you
         * proceed towards ERROR
         */
        'log.LogLevel' => 'FINE'
    ),
);