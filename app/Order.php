<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function status(){
    	return $this->belongsTo(Status::class);
    }

    public function service(){
    	return $this->belongsTo(Service::class);
    }

    public function urgency(){
    	return $this->belongsTo(Urgency::class);
    }

    public function level(){
    	return $this->belongsTo(Level::class);
    }

    public function spacing(){
    	return $this->belongsTo(Spacing::class);
    }

    public function page(){
    	return $this->belongsTo(Page::class);
    }

    public function citation(){
    	return $this->belongsTo(Citation::class);
    }

    public function source(){
    	return $this->belongsTo(Source::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transactions::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function writer(){
        return $this->belongsTo(User::class);
    }


}
