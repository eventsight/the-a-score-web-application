<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
     public function service(){
    	return $this->belongsTo(Service::class);
    }

    public function urgency(){
    	return $this->belongsTo(Urgency::class);
    }

    public function level(){
    	return $this->belongsTo(Level::class);
    }
}
