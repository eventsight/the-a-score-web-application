<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
	const MessagesStatusArr = [
		"Read",
		"Unead",
		"Pending", 
		"Decline"
	];

    public function order(){
    	return $this->belongsTo(Order::class);
    }

    public function user(){
    	return $this->belongsTo(User::class);
    }

    public function MessageStatus(){
    	return Message::MessagesStatusArr[$this->approve_marker];

    }
}
