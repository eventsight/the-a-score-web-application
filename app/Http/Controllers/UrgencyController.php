<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Urgency;
use Illuminate\Http\Request;

class UrgencyController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$urgencies = Urgency::orderBy('id', 'desc')->paginate(10);

		return view('urgencies.index', compact('urgencies'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('urgencies.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$urgency = new Urgency();

		$urgency->title = $request->input("title");
        $urgency->description = $request->input("description");
        $urgency->value = $request->input("value");

		$urgency->save();

		return redirect()->route('urgencies.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$urgency = Urgency::findOrFail($id);

		return view('urgencies.show', compact('urgency'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$urgency = Urgency::findOrFail($id);

		return view('urgencies.edit', compact('urgency'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$urgency = Urgency::findOrFail($id);

		$urgency->title = $request->input("title");
        $urgency->description = $request->input("description");
        $urgency->value = $request->input("value");

		$urgency->save();

		return redirect()->route('urgencies.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$urgency = Urgency::findOrFail($id);
		$urgency->delete();

		return redirect()->route('urgencies.index')->with('message', 'Item deleted successfully.');
	}

}
