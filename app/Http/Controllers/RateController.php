<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Rate;
use Illuminate\Http\Request;

class RateController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$rates = Rate::orderBy('id', 'desc')->paginate(10);

		return view('rates.index', compact('rates'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('rates.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$rate = new Rate();

		$rate->service_id = $request->input("service_id");
        $rate->urgency_id = $request->input("urgency_id");
        $rate->level_id = $request->input("level_id");
        $rate->rate = $request->input("rate");

		$rate->save();

		return redirect()->route('rates.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$rate = Rate::findOrFail($id);

		return view('rates.show', compact('rate'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$rate = Rate::findOrFail($id);

		return view('rates.edit', compact('rate'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$rate = Rate::findOrFail($id);

		$rate->service_id = $request->input("service_id");
        $rate->urgency_id = $request->input("urgency_id");
        $rate->level_id = $request->input("level_id");
        $rate->rate = $request->input("rate");

		$rate->save();

		return redirect()->route('rates.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$rate = Rate::findOrFail($id);
		$rate->delete();

		return redirect()->route('rates.index')->with('message', 'Item deleted successfully.');
	}

	public function getrate($service_id,$urgency_id)
    {  	
		$rate = Rate::where('service_id','=',$service_id)
		 		->where('urgency_id','=',$urgency_id)
		 		->first();

		if ($rate){
			echo $rate->rate;
		} else {
			echo "Not found";
		}
    }

}
