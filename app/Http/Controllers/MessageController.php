<?php 

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth; 
use App\Message;
use Illuminate\Http\Request;

class MessageController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$messages = Message::orderBy('id', 'desc')->paginate(20);

		return view('messages.index', compact('messages'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('messages.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
			$message = new Message();

			$message = $request->input("message");
			$message = preg_replace('<([\w.]+)@([\w.]+)>', '***@$2', $message);	
			$raw = preg_replace('/(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?/','******',$message);
			$message->message = $raw ;  
	        $message->order_id = $request->input("order_id");
	        $message->user_id = $request->input("user_id");
	       	$message->approve_marker = 3 ; 
	   
	        if($request->file('attachment')){
	        //	echo "gt file";
		        $files = $request->file('attachment');
				$filename = $files->getClientOriginalName();
				$destinationPath = base_path().'/public/attachments/'.$message->order_id.'/';
				$files->move($destinationPath , $filename);
				$message->attachment = $filename; 
			}

			$message->save();

			return redirect('payorder/'.$message->order_id)->with('message', 'Item created successfully.');

			// return redirect('vieworder/'.$message->order_id)->with('message', 'Item created successfully.');
	}

	public function upload(Request $request)
	{
		if ($request->input("message") == NULL && $request->file('attachment') == "" ){
			
			if (Auth::user()->user_type == '1') {
				return redirect('vieworder/'.$request->input("order_id"))->with('message', 'Message fail created ');
			}elseif (Auth::user()->user_type == '2') { 
	       		return redirect('job/'.$request->input("order_id"))->with('message', 'Message fail created ');
			}
		}else{

			$message = new Message();

			$message1 = $request->input("message");
			$message1 = preg_replace('<([\w.]+)@([\w.]+)>', '***@$2', $message1);	
			$message1 = preg_replace('/(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?/','******',$message1);;
	       	$message->message = $message1;
	        $message->order_id = $request->input("order_id");
	        $message->user_id = $request->input("user_id");
	        //$message->attachment = $request->input("attachment");

	        if(Auth::user()->user_type == '2'){
	        	$message->approve_marker = 3 ; 
	        }else{
	        	$message->approve_marker = 2 ; 
	        }

	        if($request->file('attachment')){
	        //	echo "gt file";
		        $files = $request->file('attachment');
				$filename = $files->getClientOriginalName();
				$destinationPath = base_path().'/public/attachments/'.$message->order_id.'/';
				$files->move($destinationPath , $filename);
				$message->attachment = $filename; 
			}

			
			$message->save();
				// return redirect('payorder/'.$message->order_id)->with('message', 'Item created successfully.');
				return redirect('vieworder/'.$message->order_id)->with('message', 'Message upload successfully.');


		}

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$message = Message::findOrFail($id);

		return view('messages.show', compact('message'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$message = Message::findOrFail($id);

		return view('messages.edit', compact('message'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$message = Message::findOrFail($id);

		$message->message = $request->input("message");
        $message->order_id = $request->input("order_id");
        $message->user_id = $request->input("user_id");
        $message->attachment = $request->input("attachment");

		$message->save();

		return redirect()->route('messages.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$message = Message::findOrFail($id);
		$message->delete();

		return redirect()->route('messages.index')->with('message', 'Item deleted successfully.');
	}

	public function approve($id){

		$message = Message::findOrFail($id);
		$message->approve_marker = 2 ; 
		
		$message->save();

		return redirect('/messages');
	}

	public function decline($id){

		$message = Message::findOrFail($id);
		$message->approve_marker = 4 ; 

		$message->save();

		return redirect('/messages');
	}

    public function read($id)
    {
    	$message = Message::findOrFail($id);
    	$message->approve_marker = 1 ;

    	$message->save();

    	return redirect('/vieworder/'.$message->order_id); 
    }

    public function writer(){
    	return view('messages.writer');
    }

}
