<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Source;
use Illuminate\Http\Request;

class SourceController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$sources = Source::orderBy('id', 'desc')->paginate(10);

		return view('sources.index', compact('sources'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('sources.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$source = new Source();

		$source->title = $request->input("title");
        $source->description = $request->input("description");
        $source->value = $request->input("value");

		$source->save();

		return redirect()->route('sources.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$source = Source::findOrFail($id);

		return view('sources.show', compact('source'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$source = Source::findOrFail($id);

		return view('sources.edit', compact('source'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$source = Source::findOrFail($id);

		$source->title = $request->input("title");
        $source->description = $request->input("description");
        $source->value = $request->input("value");

		$source->save();

		return redirect()->route('sources.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$source = Source::findOrFail($id);
		$source->delete();

		return redirect()->route('sources.index')->with('message', 'Item deleted successfully.');
	}

}
