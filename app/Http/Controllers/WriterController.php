<?php

namespace App\Http\Controllers;

use App\User;
use App\Order;
use DB ;
use Mail;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Password;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;

class WriterController extends Controller
{
    
    use ResetsPasswords;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function writer()
    {
        $orders = Order::orderBy('id', 'desc')->paginate(10);

        return view('writers' , compact('orders'));
    } 

    public function registerform()
    {
        return view ('writers.form');
    }

    public function register_writer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect('/add_writer')
                        ->withErrors($validator)
                        ->withInput();
        }

        $user = new User();

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->user_type = $request->input('user_type');
        $user->verified = "1";
        $user->save();
        $response = Password::sendResetLink($request->only('email'), function (Message $message) {
           $message->subject($this->getEmailSubject());

       });

       switch ($response) {
           case Password::RESET_LINK_SENT:
               return redirect('/users')->with('status', trans($response));

           case Password::INVALID_USER:
               return view('users')->withErrors(['email' => trans($response)]);
       }
       
    }  
    public function vieworder($id)
  {
    $order = Order::findOrFail($id);

    return view('writers.writer_view_order', compact('order'));
  } 

  public function applyjob($id)
  {
    $order = Order::findOrFail($id);

    $order->status_id = 6 ; 
    $order->save();

    if(DB::table('active_orders')->where('writer_id','=',Auth::user()->id)->where('order_id','=',$order->id)->exists()){

    }else{
      DB::table('active_orders')->insert([
        ['order_id'  => $id , 'status_id' => 6  , 'writer_id' => Auth::user()->id , 'approve_status' => 1]
      ]);

     }
    $id = User::findOrFail(Auth::user()->id);
    Mail::send('emails.job_apply', ['order' => $order , 'id' => $id ], function ($m) {
            $m->from('theascore@gmail.com', 'Your Application');
            $m->to('theascore@gmail.com')->subject( Auth::user()->name.' have been apply a job');
        });
    return redirect('/job')->with('message' , 'Waiting approve from admin'); 
  }

  public function approvejob($id , $writer_id){

   
      $writer = DB::table('active_orders')->where('order_id','=',$id)->where('writer_id','=',$writer_id)->where('approve_status','=','1')->update(['status_id' =>' 1' , 'approve_status'=> '2']);
         $decline_writer = DB::table('active_orders')->where('order_id','=',$id)->where('writer_id','!=',$writer_id)->update(['status_id' =>' 1' , 'approve_status'=> '3']);

      $order = Order::findOrFail($id);
      $order->writer_id = $writer_id ;
      $order->status_id = 1 ; 
      // $order->save();

      $id = User::findOrFail($writer_id);
      Mail::send('emails.job_approve', ['order' => $order , 'id' => $id ], function ($m) use ($id) {
            $m->from('theascore@gmail.com', 'Your Approved');
            $m->to($id->email)->subject('Job Approve');
         
        });

    return redirect('orders')->with('message' , 'Successful approve writer '.$order->id); 
  }

  public function decline($id , $writer_id){

    $writer = DB::table('active_orders')->where('order_id','=',$id)->where('writer_id','=',$writer_id)->where('approve_status','=','2')->update(['status_id' =>' 1' , 'approve_status'=> '3']);

    $order = Order::findOrFail($id);
    $order->status_id = '5';
    $order->save();

     return redirect('orders')->with('message' , 'Successful decline writer '.$order->id); 
  }


  public function active(){
       $orders=DB::table('active_orders')
                    ->where('writer_id','=',Auth::user()->id)
                    ->where('status_id','=','1')
                    ->orderBy('id', 'desc')
                    ->get();
                    
        $status='Active';
        return view('orders.jobs' , compact('orders','status'));
  }

  public function pendingjob(){

     $orders=\App\Order::where('status_id', '=', '6')->orderBy('id', 'desc')->paginate(20);
     $active_orders=DB::table('active_orders')->where('writer_id','=',Auth::user()->id)->get();
        return view('orders.writer_PlaceOrder' , compact('orders','active_orders'));
  }

  public function completed(){

        $orders=DB::table('active_orders')
                    ->where('writer_id','=',Auth::user()->id)
                    ->where('status_id','=','2')
                    ->orderBy('id', 'desc')
                    ->get();

        $active_orders=DB::table('active_orders')->where('writer_id','=',Auth::user()->id)->get();
        return view('orders.jobs' , compact('orders','status'));
  }


  public function revision(){

      $orders=DB::table('active_orders')
                    ->where('writer_id','=',Auth::user()->id)
                    ->where('status_id','=','3')
                    ->orderBy('id', 'desc')
                    ->get();

        $status='revision';
        return view('orders.jobs' , compact('orders','status'));
  }

  public function writer_view()
  {
    $orders=\App\Order::where(function($query)
                        {
                            $query->where('status_id', '=', '5')
                                  ->orwhere('status_id', '=', '6');
                            })
                ->orderBy('id', 'desc')->paginate(20);

    $active_orders=DB::table('active_orders')->where('writer_id','=',Auth::User()->id)->get();

    return view('orders.writer_PlaceOrder', compact('orders', 'active_orders'));
  }

  public function upload_final_doc(Request $request)
  {
    if ($request->file('attachment')) {
    
       $order = Order::findOrFail($request->input('order_id'));
      if($request->file('attachment')){
          $files = $request->file('attachment');
          $filename = $files->getClientOriginalName();
          $destinationPath = base_path().'/public/attachments/'.$request->input('order_id').'/';
          $files->move($destinationPath , $filename);
          $order->attachment = $filename; 
        }
        $order->approve_final_doc = 1 ;
        $order->save();

      return redirect('/vieworder/'.$request->input('order_id'))->with('message' , 'Upload Successful'); 
    }else{ 
      return redirect('/vieworder/'.$request->input('order_id'))->with('message' , 'Upload fail'); 
    }
  }
}
