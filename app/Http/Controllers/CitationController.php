<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Citation;
use Illuminate\Http\Request;

class CitationController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$citations = Citation::orderBy('id', 'desc')->paginate(10);

		return view('citations.index', compact('citations'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('citations.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$citation = new Citation();

		$citation->title = $request->input("title");
        $citation->description = $request->input("description");
        $citation->value = $request->input("value");

		$citation->save();

		return redirect()->route('citations.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$citation = Citation::findOrFail($id);

		return view('citations.show', compact('citation'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$citation = Citation::findOrFail($id);

		return view('citations.edit', compact('citation'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$citation = Citation::findOrFail($id);

		$citation->title = $request->input("title");
        $citation->description = $request->input("description");
        $citation->value = $request->input("value");

		$citation->save();

		return redirect()->route('citations.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$citation = Citation::findOrFail($id);
		$citation->delete();

		return redirect()->route('citations.index')->with('message', 'Item deleted successfully.');
	}

}
