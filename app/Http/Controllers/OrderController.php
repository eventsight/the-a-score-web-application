<?php 

namespace App\Http\Controllers;


use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Order;
use App\Transactions ;
use Illuminate\Http\Request;
use Auth;
use DB ; 
use Carbon\Carbon;

use Config;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Address;
use PayPal\Api\BillingInfo;
use PayPal\Api\Cost;
use PayPal\Api\Currency;
use PayPal\Api\Invoice;
use PayPal\Api\InvoiceAddress;
use PayPal\Api\InvoiceItem;
use PayPal\Api\MerchantInfo;
use PayPal\Api\PaymentTerm;
use PayPal\Api\Phone;
use PayPal\Api\ShippingInfo;

class OrderController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	  private $_api_context;

	 public function __construct()
    {
        $this->middleware('auth');
        $paypal_conf = Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);
    }


	public function index()
	{
		$orders = Order::orderBy('id', 'desc')->paginate(20);

		return view('orders.index', compact('orders'));
	}

	public function status($id){
		$search = $id ; 
		$orders = Order::where('status_id' , '=' , $id)->orderBy('id','desc')->paginate(20);

		return view('orders.index', compact('orders', 'search')); 

	}

	public function searchstatus (Request $request){
		
        $search = $request->status;
        if($search == 0 ){
        	$orders = Order::orderBy('id','desc')->paginate(20);
        }else{
        	$orders = Order::where('status_id', '=', $search)->orderBy('id', 'desc')->paginate(20);
         }
        return view('orders.index', compact('orders','search'))->renderSections()['subcontent'];
    
    }

	public function shoppingcart()
	{
		$orders=\App\Order::where('user_id','=',Auth::user()->id)
                ->where('status_id','=','4')
                ->orderBy('id', 'desc')->paginate(6);

        $status='unpaid';

		return view('orders.shoppingcart', compact('orders','status'));
	}

	public function active()
	{
		$orders=\App\Order::where('user_id','=',Auth::user()->id)
                ->where(function($query)
                        {
                            $query->where('status_id', '=', '1')
                                  ->orwhere('status_id', '=', '5')
                                  ->orwhere('status_id', '=', '6');
                            })
                ->orderBy('id', 'desc')->get();

        $status='active';

		return view('orders.orders', compact('orders','status'));
	}

	public function approve()
	{
		$orders=\App\Order::where('user_id','=',Auth::user()->id)
                ->where('status_id','=','4')
                ->orderBy('id', 'desc')->paginate(6);


		// return view('orders.active', compact('orders'));
	}

	public function revision()
	{
		$orders=\App\Order::where('user_id','=',Auth::user()->id)
                ->where('status_id','=','3')
                ->orderBy('id', 'desc')->paginate(6);

		$status='revision';

		return view('orders.orders', compact('orders','status'));
	}

	public function in_revision($id){
		$order = Order::findOrFail($id);
    	
    	$order->status_id = 3 ; 
    	$order->revision_count= $order->revision_count+1;
    	$order->save();

    	DB::table('active_orders')->where('order_id','=',$id)->where('status_id','=',1)->update(['status_id' => 3]);

    	return redirect('/vieworder/'.$id) ->with('message' , ' Order Being Revision');

    }

	public function completed()
	{
		$orders=\App\Order::where('user_id','=',Auth::user()->id)
                ->where('status_id','=','2')
                ->orderBy('id', 'desc')->paginate(6);

		$status='completed';

		return view('orders.orders', compact('orders','status'));
	}

	public function received()
	{
		$orders=\App\Order::where('user_id','=',Auth::user()->id)
                ->where('status_id','=','7')
                ->orderBy('id', 'desc')->paginate(6);


		// return view('orders.active', compact('orders'));
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('orders.create');
	}

	public function placeorder()
	{
		return view('orders.placeorder');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$order = new Order();

		$order->title = $request->input("title");
        $order->description = $request->input("description");
        
        if ($request->input("date")) {
        	$order->date = $request->input("date");
 		} else {
 			$order->date = date("Y-m-d");
 		}    

 		
       	$order->status_id = 4;
        

        $order->service_id = $request->input("service_id");

        $order->due_date = $request->input("due_date");
        $order->urgency_id = $request->input("urgency_id");

        if ($request->input("due_date")) {
        	$order->due_date = $request->input("due_date");
 		} else {
 			$urgency = \App\Urgency::where('id','=',$order->urgency_id)->first();
 			$date=strtotime(date('Y-m-d h:i:s')); 
 			$order->due_date = date('Y-m-d h:i:s',strtotime('+'.$urgency->value.' days',$date));
 		}
        
        $order->level_id = $request->input("level_id");
        $order->spacing_id = $request->input("spacing_id");
        $order->page_id = $request->input("page_id");
        $order->citation_id = $request->input("citation_id");
        $order->source_id = $request->input("source_id");

        $rate = \App\Rate::where('service_id','=',$order->service_id)
		 		->where('level_id','=',$order->level_id)
		 		->where('urgency_id','=',$order->urgency_id)
		 		->first();
        $order->priceperpage = $rate->rate;

        $spacing = \App\Spacing::where('id','=',$order->spacing_id)->first();
        $page = \App\Page::where('id','=',$order->page_id)->first();

        $order->totalprice = $order->priceperpage*$spacing->value*$page->value;

        if ($request->input("user_id")) {
        	$order->user_id = $request->input("user_id");
        } else {
        	$order->user_id = \Auth::user()->id;;
        }
		
		$order->save();

		return redirect('/uploaddoc/'.$order->id)->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$order = Order::findOrFail($id);

		return view('orders.show', compact('order'));
	}

	public function vieworder($id)
	{
		if(Auth::user()->user_type == 1){
			$thisorder = Order::findOrFail($id);

			if($thisorder->status_id == 5 || $thisorder->status_id == 6 || $thisorder->status_id == 1 ){
				$orders=\App\Order::where(function($query)
                        {
                            $query->where('status_id', '=', '1')
                                  ->orwhere('status_id', '=', '5')
                                  ->orwhere('status_id', '=', '6');
                            })->orderBy('id', 'desc')->get();

                $status='Active';	
            }else{
				$orders=\App\Order::where('status_id','=',$thisorder->status_id)->where('user_id','=', Auth::user()->id)->orderBy('id', 'desc')->paginate(6);
            }


           }elseif(Auth::user()->user_type == 2){
           		$thisorder = Order::findOrFail($id);
 				$orders = DB::table('active_orders')->where('status_id','=',$thisorder->status_id)->where('writer_id','=',Auth::user()->id)->orderBy('id','desc')->get();

           }elseif(Auth::user()->user_type == 3 ){
           		
           		$thisorder = Order::findOrFail($id);           	
           		$orders=\App\Order::where('status_id','=',$thisorder->status_id)->orderBy('id', 'desc')->get();

           }
		\Braintree_Configuration::environment(env('ENVIRONMENT'));
		\Braintree_Configuration::merchantId(env('MERCHANTID'));
		\Braintree_Configuration::publicKey(env('PUBLICKEY'));
		\Braintree_Configuration::privateKey(env('PRIVATEKEY'));	

		return view('orders.vieworder', compact('thisorder','orders' ,'status'));
	}


	public function payorder($id)
	{
		$order = Order::findOrFail($id);
		\Braintree_Configuration::environment('sandbox');
		\Braintree_Configuration::merchantId('hzkf5g6q3bv46yws');
		\Braintree_Configuration::publicKey('35g2qfy4pbhxqj8j');
		\Braintree_Configuration::privateKey('709a0a4eba94928888d84388dfc614be');		

		return view('orders.payorder', compact('order'));
	}

	public function uploaddoc($id)
	{
		$order = Order::findOrFail($id);

		return view('orders.uploaddoc', compact('order'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$order = Order::findOrFail($id);

		return view('orders.edit', compact('order'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$order = Order::findOrFail($id);

		$order->title = $request->input("title");
        $order->description = $request->input("description");
        $order->date = $request->input("date");
        $order->status_id = $request->input("status_id");
        $order->service_id = $request->input("service_id");
        $order->due_date = $request->input("due_date");
        $order->urgency_id = $request->input("urgency_id");
        $order->level_id = $request->input("level_id");
        $order->spacing_id = $request->input("spacing_id");
        $order->page_id = $request->input("page_id");
        $order->citation_id = $request->input("citation_id");
        $order->source_id = $request->input("source_id");

		$order->save();

		return redirect()->route('orders.index')->with('message', 'Order #'.$id.' updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$order = Order::findOrFail($id);
		$order->delete();

		return redirect()->route('orders.index')->with('message', 'Item deleted successfully.');
	}

	public function payment()
	{
		\Braintree_Configuration::environment(env('ENVIRONMENT'));
		\Braintree_Configuration::merchantId(env('MERCHANTID'));
		\Braintree_Configuration::publicKey(env('PUBLICKEY'));
		\Braintree_Configuration::privateKey(env('PRIVATEKEY'));
		return view('orders.pay');
	}


	public function paypalStatus(Request $request){

		//to-do
		//need to double check status from paypal
		$arr = ['success'=>true];
		$orders = json_decode($request->input('order_id'));
		foreach($orders as $value){
			$order = Order::findOrFail($value);
			$order->status_id = 5;
			$order->save();
			
		}
		// // $order = Order::findOrFail($request->input('order_id'));
		// // $order->status_id = 5;
		// // $order->save();
		$this->paypalInvoice($request->input('order_id'));
		\Debugbar::disable();
		return json_encode($arr);

	}
	 
	public function update_writer(Request $request , $id){

		$order = Order::findOrFail($id);

		$order->writer_id = $request->input('writer_id');
		$order->save();

		return redirect('/orders')->with('message', 'Item created successfully.');
	}

    public function assign_writer(Request $request , $id){
    	$order = Order::findOrFail($id);

    	DB::table('active_orders')->insert([
	      	['order_id'  => $id , 'status_id' => 6  , 'writer_id' => $request->writer_id , 'approve_status' => 1]
	    ]);

	    $order = Order::findOrFail($id);
	    $order->status_id = '6';
    	$order->writer_id =$request->writer_id ; 
    	$order->save();

    	return redirect('/orders')->with('message' , 'Order '.$order->id.' have been assign to new writer'); 
    }

    public function cancel_writer($id){
    	$order = Order::findOrFail($id);

    	$order->writer_id = NULL ;
    	$order->Save();

    	return redirect('/orders')->with('message' , 'Writer for order '.$order->id.' have been remove'); 
    }

    public function approve_doc($id){
    	$order = Order::findOrFail($id);

    	$order->approve_final_doc = 2 ;
    	$order->status_id = 2 ; 
    	$order->updated_at = Carbon::now();
       	$order->Save();

    	return redirect('/orders')->with('message' , 'Final document is approved'); 
    }

     public function reject_doc($id){
    	$order = Order::findOrFail($id);

    	$order->attachment = NULL ; 
    	$order->approve_final_doc = 0 ;
    	$order->Save();

    	return redirect('/orders')->with('message' , 'Final document is rejected'); 
    }

    public function download($id){
    	$order = Order::findOrFail($id);

    	$pathToFile = base_path().'/public/attachments/'.$id.'/'.$order->attachment ; 
    	return response()->download($pathToFile);
    }


    public function changetopaid($id)
	{
		$order = Order::findOrFail($id);
        $order->status_id = 5;

		$order->save();

		return redirect()->route('orders.index')->with('message', 'Item updated successfully.');
	}

	public function complete($id){
		$order = Order::findOrFail($id);
		$order->status_id = 2 ; 
		$order->updated_at = Carbon::now();
		$order->save();

		DB::table('active_orders')->where('order_id' , $id)->where('status_id','1')->update(['status_id'=> 2]);

		return redirect('/vieworder/'.$id)->with('message','Thank for letting us helping you.');

	}
   
	public function cancelorder($id){
		$order = Order::findOrFail($id); 
		$order->status_id = 8 ; 
		$order->save();

		return redirect('/home')->with('message','Order being cancel');

	}

	public function paypalInvoice( $orders ){

		// $order = Order::findOrFail($order_id); 

		$invoice = new Invoice();
	
		$invoice->setMerchantInfo(new MerchantInfo())
			    ->setBillingInfo(array(new BillingInfo()))
			    ->setNote("The A Score Invoice 16 Jul, 2013 PST")
			    ->setPaymentTerm(new PaymentTerm())
			    ->setShippingInfo(new ShippingInfo());

		$invoice->getMerchantInfo()
				->setFirstName("Yean Tat")
    			->setLastName("Tan")
			    ->setEmail("onionmikelee@gmail.com")
			    ->setPhone(new Phone());

		$invoice->getMerchantInfo()->getPhone()
			    ->setCountryCode("909")
			    ->setNationalNumber("718-9223");

		$invoice->getPaymentTerm()
				->setTermType("NET_45");

		$billing = $invoice->getBillingInfo();
		$billing[0]
		    ->setEmail('sc.lee2711@gmail.com');

		$items = array();

		foreach(json_decode($orders) as $key=>$value){
			$details = Order::findOrFail($value);
			$items[$key] = new InvoiceItem();
			$items[$key]
		    	->setName($details->id)
		    	->setQuantity(1)
		    	->setUnitPrice(new Currency());
			$items[$key]->getUnitPrice()
		    	->setCurrency("USD")
		    	->setValue($details->totalprice);
			$invoice->setItems($items);
		}
		// invoice created as draft 
		try {
    		$invoice->create($this->_api_context);
		} catch (Exception $ex) {
    	
    		exit(1);
		}
		// dd($invoice);
		// send invoice to client
		try {
    		$sendStatus = $invoice->send($this->_api_context);
		} catch (Exception $ex) {
		   
		    exit(1);
		}
			
		try {
		    $invoice = Invoice::get($invoice->getId(), $this->_api_context);
	
		} catch (Exception $ex) {
		    	
		   exit(1);
		}
		
		$transaction = new Transactions();

		$transaction->invoice_id = $invoice->id ; 
		$transaction->trans_id = $invoice->number;
		$arr = []  ;
		$arr['order_id'] = $orders ; 
		$arr['status'] = $invoice->status ; 
		$transaction->msg = json_encode($arr) ; 

		$transaction->save() ;

		// dd($transaction);
	}

  	public function export()
  	{
  	  $filename = "A_Score_Order_" . date('Ymd') . ".xls";
      function escape_utf($str){
        return '"'.$str.'"';
      }

      $orders = \App\Order::get();
      $str = chr(239) . chr(187) . chr(191) . "ID,Title,User,Description,Status,Writer,Price\r\n";
      foreach($orders as $order){
        $str .= escape_utf($order->id).",".escape_utf($order->title).",".escape_utf($order->user->name).",".escape_utf($order->description).escape_utf($order->status->name).",".escape_utf($order->writer->name).",".escape_utf($order->totalprice)."\r\n";
      }
      return response($str)
              ->header('Content-Disposition', "filename=\"$filename\"")
              ->header('Content-Type', "application/vnd.ms-excel;charset=utf-8");
  	}

}


