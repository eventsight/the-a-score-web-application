<?php

namespace App\Http\Controllers;

use App\User;
use DB ;
use Auth;
use Image;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['faq']);
    }

    public function email(){
      return view('email');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function faq(){
      return view('faq');
    }

    public function writer()
    {
        return view('dashboard_writer');
    }

    public function admin()
    {
        return view('dashboard_admin');
    }

    public function user()
    {
        $users = User::orderBy('id', 'desc')->paginate(10);

        return view('admins.user', compact('users'));
    }

    public function users_download(){
      $filename = "A_Score_User_" . date('Ymd') . ".xls";
      function escape_utf($str){
        return '"'.$str.'"';
      }

      $users = User::orderBy('id', 'desc')->get();
      $str = chr(239) . chr(187) . chr(191) . "ID,Name,Email,Type\r\n";
      foreach($users as $user){
        $str .= escape_utf($user->id).",".escape_utf($user->name).",".escape_utf($user->email).",".escape_utf($user->typeName())."\r\n";
      }
      return response($str)
              ->header('Content-Disposition', "filename=\"$filename\"")
              ->header('Content-Type', "application/vnd.ms-excel;charset=utf-8");
    }


    public function change_user($id , $type)
    {
        $user = User::findOrFail($id);
            DB::table('users')
                -> where('id' , $id )
                -> update(['user_type' => $type]) ;


        $users = User::orderBy('id', 'desc')->paginate(10);
        return view('scaffold-interface.users.index' , compact('users'));
       
    }

    public function filterby (Request $request){

      $users= \App\User::where('user_type', '=', $request->input('filter-role'))
                     ->orderBy('id', 'desc')->paginate(6);

      return view('scaffold-interface.users.index' , compact('users'));

    }

    public function searchname (Request $request){

      $users= \App\User::where('name', 'LIKE', '%'.$request->input('user-name').'%')
                     ->orderBy('id', 'desc')->paginate(6);

      return view('scaffold-interface.users.index' , compact('users'));

    }

    public function upload_photo(Request $request){
      // dd($request);   
      $user = \App\User::findOrFail(Auth::user()->id);
      // dd($request->hasFile('profile'));
      if($request->hasFile('profile')) {
 
            $avatar = $request->file('profile');
            $filename = $avatar->getClientOriginalName();
            $destinationPath = base_path().'/public/images/'.$user->id.'/';
            $datetime = (date("Y-m-d_H-i-s"));
            $avatar->move($destinationPath , $filename);
            
            $user->profile_pic = $filename;  
        }
        $user->save();
      

      return redirect('/user/'.Auth::user()->id);
    }
    
    public function userprofile($id){
      $user  = \App\User::findOrFail($id); 
      $orders = \App\Order::findOrFail($user->id); 

      return view('user.userprofile' , compact('user' , 'orders'));

    }

}