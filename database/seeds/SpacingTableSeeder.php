<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class SpacingTableSeeder extends Seeder {

    public function run()
    {
        // TestDummy::times(20)->create('App\Post');

        DB::table('spacings')->insert([
            
        ['title' => 'Single','description' => '','value' => '2'],
        ['title' => 'Double','description' => '','value' => '1']

        ]);
    }

}