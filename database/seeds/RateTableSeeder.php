<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class RateTableSeeder extends Seeder {

    public function run()
    {
        // TestDummy::times(20)->create('App\Post');
        DB::table('rates')->insert([
        	['service_id' => '1','urgency_id' => '1','level_id' => '1','rate' => '15'],
        	['service_id' => '1','urgency_id' => '2','level_id' => '1','rate' => '15'],
        	['service_id' => '1','urgency_id' => '3','level_id' => '1','rate' => '15'],
        	['service_id' => '1','urgency_id' => '4','level_id' => '1','rate' => '15'],
        	['service_id' => '1','urgency_id' => '5','level_id' => '1','rate' => '15'],
        	['service_id' => '1','urgency_id' => '6','level_id' => '1','rate' => '16'],
        	['service_id' => '1','urgency_id' => '7','level_id' => '1','rate' => '17'],
        	['service_id' => '1','urgency_id' => '8','level_id' => '1','rate' => '18'],
        	['service_id' => '1','urgency_id' => '9','level_id' => '1','rate' => '19'],
        	['service_id' => '1','urgency_id' => '10','level_id' => '1','rate' => '20'],

        	['service_id' => '2','urgency_id' => '1','level_id' => '1','rate' => '26'],
        	['service_id' => '2','urgency_id' => '2','level_id' => '1','rate' => '27'],
        	['service_id' => '2','urgency_id' => '3','level_id' => '1','rate' => '28'],
        	['service_id' => '2','urgency_id' => '4','level_id' => '1','rate' => '29'],
        	['service_id' => '2','urgency_id' => '5','level_id' => '1','rate' => '30'],
        	['service_id' => '2','urgency_id' => '6','level_id' => '1','rate' => '31'],
        	['service_id' => '2','urgency_id' => '7','level_id' => '1','rate' => '32'],
        	['service_id' => '2','urgency_id' => '8','level_id' => '1','rate' => '33'],
        	['service_id' => '2','urgency_id' => '9','level_id' => '1','rate' => '36'],
        	['service_id' => '2','urgency_id' => '10','level_id' => '1','rate' => '40'],

        	['service_id' => '3','urgency_id' => '1','level_id' => '1','rate' => '32'],
        	['service_id' => '3','urgency_id' => '2','level_id' => '1','rate' => '33'],
        	['service_id' => '3','urgency_id' => '3','level_id' => '1','rate' => '34'],
        	['service_id' => '3','urgency_id' => '4','level_id' => '1','rate' => '35'],
        	['service_id' => '3','urgency_id' => '5','level_id' => '1','rate' => '36'],
        	['service_id' => '3','urgency_id' => '6','level_id' => '1','rate' => '37'],
        	['service_id' => '3','urgency_id' => '7','level_id' => '1','rate' => '38'],
        	['service_id' => '3','urgency_id' => '8','level_id' => '1','rate' => '39'],
        	['service_id' => '3','urgency_id' => '9','level_id' => '1','rate' => '42'],
        	['service_id' => '3','urgency_id' => '10','level_id' => '1','rate' => '45'],

        	['service_id' => '4','urgency_id' => '1','level_id' => '1','rate' => '45'],
        	['service_id' => '4','urgency_id' => '2','level_id' => '1','rate' => '45'],
        	['service_id' => '4','urgency_id' => '3','level_id' => '1','rate' => '45'],
        	['service_id' => '4','urgency_id' => '4','level_id' => '1','rate' => '45'],
        	['service_id' => '4','urgency_id' => '5','level_id' => '1','rate' => '45'],
        	['service_id' => '4','urgency_id' => '6','level_id' => '1','rate' => '45'],
        	['service_id' => '4','urgency_id' => '7','level_id' => '1','rate' => '45'],
        	['service_id' => '4','urgency_id' => '8','level_id' => '1','rate' => '50'],
        	['service_id' => '4','urgency_id' => '9','level_id' => '1','rate' => '55'],
        	['service_id' => '4','urgency_id' => '10','level_id' => '1','rate' => '60'],

        	['service_id' => '5','urgency_id' => '1','level_id' => '1','rate' => '10'],
        	['service_id' => '5','urgency_id' => '2','level_id' => '1','rate' => '10'],
        	['service_id' => '5','urgency_id' => '3','level_id' => '1','rate' => '10'],
        	['service_id' => '5','urgency_id' => '4','level_id' => '1','rate' => '10'],
        	['service_id' => '5','urgency_id' => '5','level_id' => '1','rate' => '10'],
        	['service_id' => '5','urgency_id' => '6','level_id' => '1','rate' => '10'],
        	['service_id' => '5','urgency_id' => '7','level_id' => '1','rate' => '10'],
        	['service_id' => '5','urgency_id' => '8','level_id' => '1','rate' => '10'],
        	['service_id' => '5','urgency_id' => '9','level_id' => '1','rate' => '10'],
        	['service_id' => '5','urgency_id' => '10','level_id' => '1','rate' => '10'],

        	//level 2
        	['service_id' => '1','urgency_id' => '1','level_id' => '2','rate' => '15'],
        	['service_id' => '1','urgency_id' => '2','level_id' => '2','rate' => '15'],
        	['service_id' => '1','urgency_id' => '3','level_id' => '2','rate' => '15'],
        	['service_id' => '1','urgency_id' => '4','level_id' => '2','rate' => '15'],
        	['service_id' => '1','urgency_id' => '5','level_id' => '2','rate' => '15'],
        	['service_id' => '1','urgency_id' => '6','level_id' => '2','rate' => '16'],
        	['service_id' => '1','urgency_id' => '7','level_id' => '2','rate' => '17'],
        	['service_id' => '1','urgency_id' => '8','level_id' => '2','rate' => '18'],
        	['service_id' => '1','urgency_id' => '9','level_id' => '2','rate' => '19'],
        	['service_id' => '1','urgency_id' => '10','level_id' => '2','rate' => '20'],

        	['service_id' => '2','urgency_id' => '1','level_id' => '2','rate' => '26'],
        	['service_id' => '2','urgency_id' => '2','level_id' => '2','rate' => '27'],
        	['service_id' => '2','urgency_id' => '3','level_id' => '2','rate' => '28'],
        	['service_id' => '2','urgency_id' => '4','level_id' => '2','rate' => '29'],
        	['service_id' => '2','urgency_id' => '5','level_id' => '2','rate' => '30'],
        	['service_id' => '2','urgency_id' => '6','level_id' => '2','rate' => '31'],
        	['service_id' => '2','urgency_id' => '7','level_id' => '2','rate' => '32'],
        	['service_id' => '2','urgency_id' => '8','level_id' => '2','rate' => '33'],
        	['service_id' => '2','urgency_id' => '9','level_id' => '2','rate' => '36'],
        	['service_id' => '2','urgency_id' => '10','level_id' => '2','rate' => '40'],

        	['service_id' => '3','urgency_id' => '1','level_id' => '2','rate' => '32'],
        	['service_id' => '3','urgency_id' => '2','level_id' => '2','rate' => '33'],
        	['service_id' => '3','urgency_id' => '3','level_id' => '2','rate' => '34'],
        	['service_id' => '3','urgency_id' => '4','level_id' => '2','rate' => '35'],
        	['service_id' => '3','urgency_id' => '5','level_id' => '2','rate' => '36'],
        	['service_id' => '3','urgency_id' => '6','level_id' => '2','rate' => '37'],
        	['service_id' => '3','urgency_id' => '7','level_id' => '2','rate' => '38'],
        	['service_id' => '3','urgency_id' => '8','level_id' => '2','rate' => '39'],
        	['service_id' => '3','urgency_id' => '9','level_id' => '2','rate' => '42'],
        	['service_id' => '3','urgency_id' => '10','level_id' => '2','rate' => '45'],

        	['service_id' => '4','urgency_id' => '1','level_id' => '2','rate' => '45'],
        	['service_id' => '4','urgency_id' => '2','level_id' => '2','rate' => '45'],
        	['service_id' => '4','urgency_id' => '3','level_id' => '2','rate' => '45'],
        	['service_id' => '4','urgency_id' => '4','level_id' => '2','rate' => '45'],
        	['service_id' => '4','urgency_id' => '5','level_id' => '2','rate' => '45'],
        	['service_id' => '4','urgency_id' => '6','level_id' => '2','rate' => '45'],
        	['service_id' => '4','urgency_id' => '7','level_id' => '2','rate' => '45'],
        	['service_id' => '4','urgency_id' => '8','level_id' => '2','rate' => '50'],
        	['service_id' => '4','urgency_id' => '9','level_id' => '2','rate' => '55'],
        	['service_id' => '4','urgency_id' => '10','level_id' => '2','rate' => '60'],

        	['service_id' => '5','urgency_id' => '1','level_id' => '2','rate' => '10'],
        	['service_id' => '5','urgency_id' => '2','level_id' => '2','rate' => '10'],
        	['service_id' => '5','urgency_id' => '3','level_id' => '2','rate' => '10'],
        	['service_id' => '5','urgency_id' => '4','level_id' => '2','rate' => '10'],
        	['service_id' => '5','urgency_id' => '5','level_id' => '2','rate' => '10'],
        	['service_id' => '5','urgency_id' => '6','level_id' => '2','rate' => '10'],
        	['service_id' => '5','urgency_id' => '7','level_id' => '2','rate' => '10'],
        	['service_id' => '5','urgency_id' => '8','level_id' => '2','rate' => '10'],
        	['service_id' => '5','urgency_id' => '9','level_id' => '2','rate' => '10'],
        	['service_id' => '5','urgency_id' => '10','level_id' => '2','rate' => '10'],

        	//level 3
        	['service_id' => '1','urgency_id' => '1','level_id' => '3','rate' => '15'],
        	['service_id' => '1','urgency_id' => '2','level_id' => '3','rate' => '15'],
        	['service_id' => '1','urgency_id' => '3','level_id' => '3','rate' => '15'],
        	['service_id' => '1','urgency_id' => '4','level_id' => '3','rate' => '15'],
        	['service_id' => '1','urgency_id' => '5','level_id' => '3','rate' => '15'],
        	['service_id' => '1','urgency_id' => '6','level_id' => '3','rate' => '16'],
        	['service_id' => '1','urgency_id' => '7','level_id' => '3','rate' => '17'],
        	['service_id' => '1','urgency_id' => '8','level_id' => '3','rate' => '18'],
        	['service_id' => '1','urgency_id' => '9','level_id' => '3','rate' => '19'],
        	['service_id' => '1','urgency_id' => '10','level_id' => '3','rate' => '20'],

        	['service_id' => '2','urgency_id' => '1','level_id' => '3','rate' => '26'],
        	['service_id' => '2','urgency_id' => '2','level_id' => '3','rate' => '27'],
        	['service_id' => '2','urgency_id' => '3','level_id' => '3','rate' => '28'],
        	['service_id' => '2','urgency_id' => '4','level_id' => '3','rate' => '29'],
        	['service_id' => '2','urgency_id' => '5','level_id' => '3','rate' => '30'],
        	['service_id' => '2','urgency_id' => '6','level_id' => '3','rate' => '31'],
        	['service_id' => '2','urgency_id' => '7','level_id' => '3','rate' => '32'],
        	['service_id' => '2','urgency_id' => '8','level_id' => '3','rate' => '33'],
        	['service_id' => '2','urgency_id' => '9','level_id' => '3','rate' => '36'],
        	['service_id' => '2','urgency_id' => '10','level_id' => '3','rate' => '40'],

        	['service_id' => '3','urgency_id' => '1','level_id' => '3','rate' => '32'],
        	['service_id' => '3','urgency_id' => '2','level_id' => '3','rate' => '33'],
        	['service_id' => '3','urgency_id' => '3','level_id' => '3','rate' => '34'],
        	['service_id' => '3','urgency_id' => '4','level_id' => '3','rate' => '35'],
        	['service_id' => '3','urgency_id' => '5','level_id' => '3','rate' => '36'],
        	['service_id' => '3','urgency_id' => '6','level_id' => '3','rate' => '37'],
        	['service_id' => '3','urgency_id' => '7','level_id' => '3','rate' => '38'],
        	['service_id' => '3','urgency_id' => '8','level_id' => '3','rate' => '39'],
        	['service_id' => '3','urgency_id' => '9','level_id' => '3','rate' => '42'],
        	['service_id' => '3','urgency_id' => '10','level_id' => '3','rate' => '45'],

        	['service_id' => '4','urgency_id' => '1','level_id' => '3','rate' => '45'],
        	['service_id' => '4','urgency_id' => '2','level_id' => '3','rate' => '45'],
        	['service_id' => '4','urgency_id' => '3','level_id' => '3','rate' => '45'],
        	['service_id' => '4','urgency_id' => '4','level_id' => '3','rate' => '45'],
        	['service_id' => '4','urgency_id' => '5','level_id' => '3','rate' => '45'],
        	['service_id' => '4','urgency_id' => '6','level_id' => '3','rate' => '45'],
        	['service_id' => '4','urgency_id' => '7','level_id' => '3','rate' => '45'],
        	['service_id' => '4','urgency_id' => '8','level_id' => '3','rate' => '50'],
        	['service_id' => '4','urgency_id' => '9','level_id' => '3','rate' => '55'],
        	['service_id' => '4','urgency_id' => '10','level_id' => '3','rate' => '60'],

        	['service_id' => '5','urgency_id' => '1','level_id' => '3','rate' => '10'],
        	['service_id' => '5','urgency_id' => '2','level_id' => '3','rate' => '10'],
        	['service_id' => '5','urgency_id' => '3','level_id' => '3','rate' => '10'],
        	['service_id' => '5','urgency_id' => '4','level_id' => '3','rate' => '10'],
        	['service_id' => '5','urgency_id' => '5','level_id' => '3','rate' => '10'],
        	['service_id' => '5','urgency_id' => '6','level_id' => '3','rate' => '10'],
        	['service_id' => '5','urgency_id' => '7','level_id' => '3','rate' => '10'],
        	['service_id' => '5','urgency_id' => '8','level_id' => '3','rate' => '10'],
        	['service_id' => '5','urgency_id' => '9','level_id' => '3','rate' => '10'],
        	['service_id' => '5','urgency_id' => '10','level_id' => '3','rate' => '10'],




        ]);
    }

}