<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CitationTableSeeder::class);
        $this->call(LevelTableSeeder::class);
        $this->call(PageTableSeeder::class);
        $this->call(ServiceTableSeeder::class);
        $this->call(SourceTableSeeder::class);
        $this->call(StatusTableSeeder::class);
        $this->call(SpacingTableSeeder::class);
        $this->call(UrgencyTableSeeder::class);
        $this->call(RateTableSeeder::class);
    }
}
