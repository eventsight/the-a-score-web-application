<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class LevelTableSeeder extends Seeder {

    public function run()
    {
        // TestDummy::times(20)->create('App\Post');

        DB::table('levels')->insert([
            
        ['title' => 'Basic','description' => '','value' => '1'],
        ['title' => 'Intermediate','description' => '','value' => '1'],
        ['title' => 'Advance','description' => '','value' => '1']

        ]);
    }

}