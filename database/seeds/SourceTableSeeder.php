<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class SourceTableSeeder extends Seeder {

    public function run()
    {
        DB::table('sources')->insert([
            
        ['title' => '10','description' => '','value' => '10'],
        ['title' => '9','description' => '','value' => '9'],
        ['title' => '8','description' => '','value' => '8'],
        ['title' => '7','description' => '','value' => '7'],
        ['title' => '6','description' => '','value' => '6'],
        ['title' => '5','description' => '','value' => '5'],
        ['title' => '4','description' => '','value' => '4'],
        ['title' => '3','description' => '','value' => '3'],
        ['title' => '2','description' => '','value' => '2'],
        ['title' => '1','description' => '','value' => '1']

        ]);
    }

}