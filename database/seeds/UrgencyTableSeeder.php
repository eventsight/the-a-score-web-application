<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class UrgencyTableSeeder extends Seeder {

    public function run()
    {
        // TestDummy::times(20)->create('App\Post');

        DB::table('urgencies')->insert([
            
        ['title' => '10 Days','description' => '','value' => '10'],
        ['title' => '9 Days','description' => '','value' => '9'],
        ['title' => '8 Days','description' => '','value' => '8'],
        ['title' => '7 Days','description' => '','value' => '7'],
        ['title' => '6 Days','description' => '','value' => '6'],
        ['title' => '5 Days','description' => '','value' => '5'],
        ['title' => '4 Days','description' => '','value' => '4'],
        ['title' => '3 Days','description' => '','value' => '3'],
        ['title' => '2 Days','description' => '','value' => '2'],
        ['title' => '1 Days','description' => '','value' => '1']

        ]);
    }

}