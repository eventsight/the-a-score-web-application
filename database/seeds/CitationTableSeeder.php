<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class CitationTableSeeder extends Seeder {

    public function run()
    {
        // TestDummy::times(20)->create('App\Post');
        DB::table('citations')->insert([
            
        ['title' => 'Harvard','description' => '','value' => '1'],
        ['title' => 'APA','description' => '','value' => '1'],
        ['title' => 'Oxford','description' => '','value' => '1'],
       

        ]);

    }

}