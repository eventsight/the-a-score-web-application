<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class ServiceTableSeeder extends Seeder {

    public function run()
    {
        // TestDummy::times(20)->create('App\Post');

        DB::table('services')->insert([
            
            ['title' => 'Editing 编辑服务',
            'description' => '',
            'value' => '15'
        ],
        [
            'title' => 'High School 高中定制写作',
            'description' => '',
            'value' => '26'

        ],
        [
            'title' => 'Undergraduate 本科定制写作',
            'description' => '',
            'value' => '32'

        ],
        [
            'title' => 'Master/研究生定制写作',
            'description' => '',
            'value' => '45'

        ],
        [
            'title' => 'SimpleGrammar 语法修改',
            'description' => '',
            'value' => '10'

        ]

        ]);

    }

}