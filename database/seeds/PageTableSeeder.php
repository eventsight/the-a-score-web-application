<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class PageTableSeeder extends Seeder {

    public function run()
    {
        // TestDummy::times(20)->create('App\Post');

        DB::table('pages')->insert([
            
        ['page' => '1','words' => '550','description' => '','value' => '1'],
        ['page' => '2','words' => '1100','description' => '','value' => '2'],
        ['page' => '3','words' => '1650','description' => '','value' => '3'],
        ['page' => '4','words' => '2200','description' => '','value' => '4'],
        ['page' => '5','words' => '2750','description' => '','value' => '5'],
        ['page' => '6','words' => '3300','description' => '','value' => '6'],
        ['page' => '7','words' => '3850','description' => '','value' => '7'],
        ['page' => '8','words' => '4400','description' => '','value' => '8'],
        ['page' => '9','words' => '4950','description' => '','value' => '9'],
        ['page' => '10','words' => '5500','description' => '','value' => '10'],

        ]);
    }

}