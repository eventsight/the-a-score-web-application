<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailVerification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::table('users', function (Blueprint $table) {
            // this column will be a TINYINT with a default value of 0 , [0 for false & 1 for true i.e. verified]
            $table->tinyInteger('verified')->default(0); 
            // this column will be a VARCHAR with no default value and will also BE NULLABLE
            $table->string('email_token')->index()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::table('users', function (Blueprint $table) {
           $table->dropColumn('verified');
           $table->dropColumn('email_token');
        });
    }
}
