<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDocumentRevisionRow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::table('orders', function (Blueprint $table) {
            $table->integer('revision_count')->nullable()->default(0);
            $table->integer('approve_final_doc')->nullable()->default(0); 
            $table->string('attachment')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::table('orders', function (Blueprint $table) {
           $table->dropColumn('revision_count');
           $table->dropColumn('approve_final_doc'); 
           $table->dropColumn('attachment');
        });
    }
}
