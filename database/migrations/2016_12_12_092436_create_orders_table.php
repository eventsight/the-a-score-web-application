<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->date('date');
            $table->integer('status_id')->unsigned()->default('1');
            $table->foreign('status_id')->references('id')->on('statuses');
            $table->integer('service_id')->unsigned()->default('1');
            $table->foreign('service_id')->references('id')->on('services');
            $table->datetime('due_date');
            $table->integer('urgency_id')->unsigned()->default('1');
            $table->foreign('urgency_id')->references('id')->on('urgencies');
            $table->integer('level_id')->unsigned()->default('1');
            $table->foreign('level_id')->references('id')->on('levels');
            $table->integer('spacing_id')->unsigned()->default('1');
            $table->foreign('spacing_id')->references('id')->on('spacings');
            $table->integer('page_id')->unsigned()->default('1');
            $table->foreign('page_id')->references('id')->on('pages');
            $table->integer('citation_id')->unsigned()->default('1');
            $table->foreign('citation_id')->references('id')->on('citations');
            $table->integer('source_id')->unsigned()->default('1');
            $table->foreign('source_id')->references('id')->on('sources');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
