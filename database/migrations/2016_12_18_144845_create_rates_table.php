<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rates', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('service_id')->unsigned()->default('1');
            $table->foreign('service_id')->references('id')->on('services');
            $table->integer('urgency_id')->unsigned()->default('1');
            $table->foreign('urgency_id')->references('id')->on('urgencies');
            $table->integer('level_id')->unsigned()->default('1');
            $table->foreign('level_id')->references('id')->on('levels');
            $table->double('rate');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rates');
	}

}
