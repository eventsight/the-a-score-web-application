@extends('layouts.admin')

@section('subcontent')
<div class="row">
    <div class="col-xs-12">
    <div class="col-xs-12" style="margin-top:15px">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="pull-left">Services</h3>
        </div>
        <div class="box-body">

            @if($services->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>TITLE</th>
                        <th>DESCRIPTION</th>
                        <th>VALUE</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($services as $service)
                            <tr>
                                <td>{{$service->id}}</td>
                                <td>{{$service->title}}</td>
                    <td>{{$service->description}}</td>
                    <td>{{$service->value}}</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('services.show', $service->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                    <a class="btn btn-xs btn-warning" href="{{ route('services.edit', $service->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <form action="{{ route('services.destroy', $service->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $services->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>
</div>
</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
$("#nav-others").addClass("active");
</script>
@endsection