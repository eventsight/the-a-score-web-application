@extends('layouts.app')

@section('content')

<div class="col-xs-12" style="margin-top: 15px;">
    <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{\App\Order::where('user_id','=',Auth::user()->id)->where(function($query)
                        {
                            $query->where('status_id', '=', '1')
                                  ->orwhere('status_id', '=', '5')
                                  ->orwhere('status_id', '=', '6');
                            })->count()}}</h3>

              <p>Active Orders</p>
            </div>
            <div class="icon">
              <i class="fa fa-pencil-square-o"></i>
            </div>
            <a href="{{ url('/active') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{\App\Order::where('user_id','=',Auth::user()->id)->where('status_id','=','2')->count()}}</h3>

              <p>Completed Orders</p>
            </div>
            <div class="icon">
              <i class="fa fa-check-square-o"></i>
            </div>
            <a href="{{ url('/completed') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{\App\Order::where('user_id','=',Auth::user()->id)->where('status_id','=','3')->count()}}</h3>

              <p>In Revision</p>
            </div>
            <div class="icon">
              <i class="fa fa-commenting-o"></i>
            </div>
            <a href="{{ url('/revision') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{\App\Order::where('user_id','=',Auth::user()->id)->where('status_id','=','4')->count()}}</h3>

              <p>Items in Shopping Cart</p>
            </div>
            <div class="icon">
              <i class="fa fa-shopping-cart"></i>
            </div>
            <a href="{{ url('/shoppingcart') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
</div>
<div class="col-xs-12">
<div class="box box-primary">
<div class="box-header">
    <h3 class="pull-left">Recent Orders</h3>
    <a href="{{url('/placeorder')}}" class = "btn btn-success pull-right"><i class="fa fa-plus fa-md" aria-hidden="true"></i> Place Order</a>
</div>
    <div class="box-body">
        
    @if($orders=\App\Order::where('user_id','=',Auth::user()->id)->where('status_id' , '!=' , '8')
                ->count()!=0)
        <table class = "table table-hover">
        <thead>
            <th>Title</th>
            <th>Order ID</th>
            <th>Due Date</th>
            <th>Status</th>
            <th>Actions</th>
        </thead>
        <tbody>
            <?php $orders=\App\Order::where('user_id','=',Auth::user()->id)->where('status_id','!=',8)
                ->orderBy('id', 'desc')->paginate(10); ?>
            @foreach($orders as $order)
            <tr>
                <td><a href="vieworder/{{$order->id}}">{{$order->title}}
                   <?php
                        $messages = \App\Message::where('order_id','=',$order->id)->where('approve_marker','=',2)->first();
                    ?>
                    @if($messages)
                     <i class="fa fa-envelope-o "></i></a>
                     @endif
                     </td>
                <td
                <td>Order #{{sprintf("%06s",$order->id)}}</td>
                <td>{{$order->due_date}}</td>
                <td style="text-align:left">
                @if($order->status_id==4)
                <small class="label label-danger">Unpaid</small>
                @elseif($order->status_id==2)
                <small class="label label-success">Completed</small>
                @elseif($order->status_id==3)
                <small class="label label-warning">In Revision</small>
                @else
                <small class="label label-primary">Active</small>
                @endif
                </td>
                <td>
                    @if($order->status_id==4)
                    <a href="payorder/{{$order->id}}" class = 'btn btn-success btn-xs'><i class="fa fa-paypal" aria-hidden="true"></i> Pay Now</a>
                    @else
                    <a href="vieworder/{{$order->id}}" class = 'btn btn-primary btn-xs'><i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i> View</a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @else

    <div class="col-xs-12 text-center">
      <i class="fa fa-times-circle fa-5x"></i>
      <h2>No Order Found</h2>
      <h4>Create new order now to get an A score!</h4>
      <a href="{{url('/placeorder')}}" class = "btn btn-success"><i class="fa fa-plus fa-md" aria-hidden="true"></i> Place Order</a>
    </div>
   
    @endif

</div>
</div>
</div>


@endsection

@section('scripts')
<script type="text/javascript">
$("#nav-dashboard").addClass("active");
</script>
@endsection