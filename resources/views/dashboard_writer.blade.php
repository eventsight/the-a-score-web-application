@extends('layouts.app')

@section('content')

<div class="col-xs-12" style="margin-top: 15px;">
    <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{ \App\Order::where('writer_id', '=' , Auth::user()->id)->where('status_id','=','1')->count()}}</h3>

              <p>Active Jobs</p>
            </div>
            <div class="icon">
              <i class="fa fa-pencil-square-o"></i>
            </div>
            <a href="{{ url('/job/active') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-orange">
            <div class="inner">
              <h3>{{ DB::table('active_orders')->where('writer_id','=',Auth::user()->id)->where('status_id','=','6')->count()}}</h3>

              <p>Pending Approval</p>
            </div>
            <div class="icon">
              <i class="fa fa-spinner"></i>
            </div>
            <a href="{{ url('/job/pending') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->


        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{ \App\Order::where('writer_id', '=' , Auth::user()->id)->where('status_id','=','2')->count() }}</h3>

              <p>Completed Jobs</p>
            </div>
            <div class="icon">
              <i class="fa fa-check-square-o"></i>
            </div>
            <a href="{{ url('/job/completed') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{ \App\Order::where('writer_id', '=' , Auth::user()->id)->where('status_id','=','3')->count()}}</h3>

              <p>In Revision</p>
            </div>
            <div class="icon">
              <i class="fa fa-commenting-o"></i>
            </div>
            <a href="{{ url('/job/revision') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
      </div>
</div>
<div class="row">
<div class="col-xs-12">
  <div class="col-sm-6">
  <div class="box box-primary">
  <div class="box-header">
      <h3 class="pull-left">Active Jobs</h3>
      <a href="{{url('/job')}}" class = "btn btn-success pull-right"><i class="fa fa-plus fa-md" aria-hidden="true"></i> Get Job</a>
  </div>
      <div class="box-body">
          
      @if($orders=\App\Order::where('writer_id','=',Auth::user()->id)->where('status_id','=','1')
                  ->count()!=0)
          <table class = "table table-hover">
          <thead>
              <th>Title</th>
              <th>Order ID</th>
              <th>Due Date</th>
              <th>Status</th>
              <th>Actions</th>
          </thead>
          <tbody>
              <?php $orders=\App\Order::where('writer_id','=',Auth::user()->id)->where('status_id','=','1')
                  ->orderBy('id', 'desc')->paginate(5); ?>
              @foreach($orders as $order)
              <tr>
                  <td>{{$order->title}}</td>
                  <td>Order #{{sprintf("%06s",$order->id)}}</td>
                  <td>{{$order->due_date}}</td>
                  <td style="text-align:left">
                  @if($order->status_id==4)
                  <small class="label label-danger">Unpaid</small>
                  @elseif($order->status_id==2)
                  <small class="label label-success">Completed</small>
                  @elseif($order->status_id==3)
                  <small class="label label-warning">In Revision</small>
                  @else
                  <small class="label label-primary">Active</small>
                  @endif
                  </td>
                  <td>
                      @if($order->status_id==4)
                      <a href="payorder/{{$order->id}}" class = 'btn btn-success btn-xs'><i class="fa fa-paypal" aria-hidden="true"></i> Pay Now</a>
                      @else
                      <a href="vieworder/{{$order->id}}" class = 'btn btn-primary btn-xs'><i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i> View</a>
                      @endif
                  </td>
              </tr>
              @endforeach
          </tbody>
      </table>
      @else

      <div class="col-xs-12 text-center">
        <i class="fa fa-times-circle fa-5x"></i>
        <h2>No Active Job Found</h2>
        <h4>Get a new job now!</h4>
        <a href="{{url('/job')}}" class = "btn btn-success"><i class="fa fa-plus fa-md" aria-hidden="true"></i> Get Job</a>
      </div>
     
      @endif

  </div>
  </div>
  </div>
  <div class="col-sm-6">
  <div class="box box-warning">
  <div class="box-header">
      <h3 class="pull-left">Pending Jobs</h3>
      <a href="{{url('/job')}}" class = "btn btn-success pull-right"><i class="fa fa-plus fa-md" aria-hidden="true"></i> Get Job</a>
  </div>
      <div class="box-body">
          
      @if($orders=DB::table('active_orders')
                    ->where('writer_id','=',Auth::user()->id)
                    ->where('status_id','=','6')
                    ->count() !=0)
          <table class = "table table-hover">
          <thead>
              <th>Title</th>
              <th>Order ID</th>
              <th>Due Date</th>
              <th>Status</th>
              <th>Actions</th>
          </thead>
          <tbody>
              <?php 
             $orders=DB::table('active_orders')
                    ->where('writer_id','=',Auth::user()->id)
                    ->where('status_id','=','6')
                    ->get();
                   ?>
              @foreach($orders as $order)
                <?php
                    $active_orders=\App\order::where('id','=',$order->order_id)->get();            
                ?>
                @foreach($active_orders as $active_order )
              <tr>
                  <td>{{$active_order->title}}</td>
                  <td>Order #{{sprintf("%06s",$active_order->id)}}</td>
                  <td>{{$active_order->due_date}}</td>
                  <td style="text-align:left">
                  @if($active_order->status_id==4)
                  <small class="label label-danger">Unpaid</small>
                  @elseif($active_order->status_id==2)
                  <small class="label label-success">Completed</small>
                  @elseif($active_order->status_id==3)
                  <small class="label label-warning">In Revision</small>
                  @elseif($active_order->status_id==6)
                  <small class="label label-warning">Pending for Approve</small>
                  @else
                  <small class="label label-primary">Active</small>
                  @endif
                  </td>
                  <td>
                      @if($active_order->status_id==4)
                      <a href="payorder/{{$order->id}}" class = 'btn btn-success btn-xs'><i class="fa fa-paypal" aria-hidden="true"></i> Pay Now</a>
                      @else
                      <a href="vieworder/{{$active_order->id}}" class = 'btn btn-primary btn-xs'><i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i> View</a>
                      @endif
                  </td>
              </tr>
                @endforeach
              @endforeach
          </tbody>
      </table>
      @else

      <div class="col-xs-12 text-center">
        <i class="fa fa-times-circle fa-5x"></i>
        <h2>No Pending Job Found</h2>
        <h4>Get a new job now!</h4>
        <a href="{{url('/job')}}" class = "btn btn-success"><i class="fa fa-plus fa-md" aria-hidden="true"></i> Get Job</a>
      </div>
     
      @endif

  </div>
  </div>
  </div>
  </div>
</div>


<div class="row">
<div class="col-xs-12">
  <div class="col-sm-6">
  <div class="box box-primary">
  <div class="box-header">
      <h3 class="pull-left">Recent Messages</h3>
  </div>
      <div class="box-body">
          
      @if($orders=\App\Message::where('user_id','=',Auth::user()->id)
                  ->count()!=0)
          <table class = "table table-hover">
          <thead>
              <th>Title</th>
              <th>Order ID</th>
              <th>Due Date</th>
              <th>Status</th>
              <th>Actions</th>
          </thead>
          <tbody>
              <?php $orders=\App\Order::where('user_id','=',Auth::user()->id)
                  ->orderBy('id', 'desc')->paginate(5); ?>
              @foreach($orders as $order)
              <tr>
                  <td>{{$order->title}}</td>
                  <td>Order #{{sprintf("%06s",$order->id)}}</td>
                  <td>{{$order->due_date}}</td>
                  <td style="text-align:left">
                  @if($order->status_id==4)
                  <small class="label label-danger">Unpaid</small>
                  @elseif($order->status_id==2)
                  <small class="label label-success">Completed</small>
                  @elseif($order->status_id==3)
                  <small class="label label-warning">In Revision</small>
                  @else
                  <small class="label label-primary">Active</small>
                  @endif
                  </td>
                  <td>
                      @if($order->status_id==4)
                      <a href="payorder/{{$order->id}}" class = 'btn btn-success btn-xs'><i class="fa fa-paypal" aria-hidden="true"></i> Pay Now</a>
                      @else
                      <a href="vieworder/{{$order->id}}" class = 'btn btn-primary btn-xs'><i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i> View</a>
                      @endif
                  </td>
              </tr>
              @endforeach
          </tbody>
      </table>
      @else

      <div class="col-xs-12 text-center">
        <i class="fa fa-times-circle fa-5x"></i>
        <h2>No Recent Message Found</h2>
      </div>
     
      @endif

  </div>
  </div>
  </div>
  <div class="col-sm-6">
  <div class="box box-warning">
  <div class="box-header">
      <h3 class="pull-left">Pending Messages</h3>
  </div>
      <div class="box-body">
          
      @if($orders=\App\Order::where('user_id','=',Auth::user()->id)
                  ->count()!=0)
          <table class = "table table-hover">
          <thead>
              <th>Title</th>
              <th>Order ID</th>
              <th>Due Date</th>
              <th>Status</th>
              <th>Actions</th>
          </thead>
          <tbody>
              <?php $orders=\App\Order::where('user_id','=',Auth::user()->id)
                  ->orderBy('id', 'desc')->paginate(5); ?>
              @foreach($orders as $order)
              <tr>
                  <td>{{$order->title}}</td>
                  <td>Order #{{sprintf("%06s",$order->id)}}</td>
                  <td>{{$order->due_date}}</td>
                  <td style="text-align:left">
                  @if($order->status_id==4)
                  <small class="label label-danger">Unpaid</small>
                  @elseif($order->status_id==2)
                  <small class="label label-success">Completed</small>
                  @elseif($order->status_id==3)
                  <small class="label label-warning">In Revision</small>
                  @else
                  <small class="label label-primary">Active</small>
                  @endif
                  </td>
                  <td>
                      @if($order->status_id==4)
                      <a href="payorder/{{$order->id}}" class = 'btn btn-success btn-xs'><i class="fa fa-paypal" aria-hidden="true"></i> Pay Now</a>
                      @else
                      <a href="vieworder/{{$order->id}}" class = 'btn btn-primary btn-xs'><i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i> View</a>
                      @endif
                  </td>
              </tr>
              @endforeach
          </tbody>
      </table>
      @else

      <div class="col-xs-12 text-center">
        <i class="fa fa-times-circle fa-5x"></i>
        <h2>No Recent Message Found</h2>
      </div>
      @endif

  </div>
  </div>
  </div>
  </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
$("#nav-dashboard-writer").addClass("active");
</script>
@endsection