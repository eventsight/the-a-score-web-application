@extends('layouts.writer')

@section('subcontent')
<div class="container">
    <div class="row">

        <div class="col-md-9">
            <a class="btn btn-link" href="{{ url('/writers') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
            <div class="panel panel-default">
                @if (session('message'))
                    <div class="alert alert-danger">
                        {{ session('message') }}
                    </div>
                @endif
                <div class="panel-heading">Order #{{sprintf("%06s",$order->id)}}</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12">
                           <h2>{{$order->title}}</h2>   
                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <div class="col-sm-6 col-xs-12">
                                        <p>{{$order->description}}</p>
                                        <p>Due Date : {{$order->due_date}}</p>
                                        <p>Order Date : {{$order->date}}</p>
                                        <!-- <p>Status : {{$order->status->title}}</p> -->
                                        <h4>Price per page : $ {{$order->priceperpage}}</h4>
                                        <h3>Total Price : $ {{$order->totalprice}}</h3>
                                    </div>

                                    <div class="col-sm-6 col-xs-12">
                                        <p>Service : {{$order->service->title}}</p>
                                        <p>Urgency : {{$order->urgency->title}}</p>
                                        <p>Level : {{$order->level->title}}</p>
                                        <p>Spacing : {{$order->spacing->title}}</p>
                                        <p>Page : {{$order->page->title}}</p>
                                        <p>Citation : {{$order->citation->title}}</p>
                                        <p>Source : {{$order->source->title}}</p>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                            <?php
                                $messages=\App\Message::where('order_id','=',$order->id)->get();
                            ?>
                            @foreach($messages as $message)
                            <div class="col-xs-9
                            @if($message->user_id==Auth::user()->id)
                            pull-right
                            @endif
                            ">
                                <div class="panel panel-default  @if($message->approve_marker == 1)
                                    panel-primary
                                    @elseif($message->approve_marker == 2)
                                    panel-danger
                                    @elseif($message->approve_marker == 3)
                                    panel-warning
                                    @endif">
                                  <div class="panel-heading"><p>{{$message->user->name}}
                                  @if($message->user_id==Auth::user()->id)
                                      @if($message->approve_marker == 1)
                                      (Approve)
                                      @elseif($message->approve_marker == 2)
                                      (Decline)
                                      @elseif($message->approve_marker == 3)
                                      (Waiting For Approve)
                                      @endif
                                  @endif 
                                  </p>
                                @if($message->approve_marker == 3)
                                  <a class="btn btn-success pull-right" href="">Edit</a>
                                @endif
                                  </div>
                                  <div class="panel-body">
                                    <p>{{$message->message}}</p>
                                    <p><a href="/attachments/{{$order->id}}/{{$message->attachment}}" target="_BLANK">{{$message->attachment}}</a></p>
                                    <h6 class="text-right">{{$message->created_at}}</h6>
                                  </div>
                                </div>
                            </div>
                            @endforeach
                            </div>
                            <div class="row">
                            <form action="{{ route('messages.upload') }}" method="POST" enctype="multipart/form-data">

                                <div class="col-xs-12">
                                
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <label for="message-field">Message</label>
                                    <textarea class="form-control" id="message-field" rows="3" name="message">{{ old("message") }}</textarea>
                                    <input type="hidden" id="order_id-field" name="order_id" class="form-control" value="{{$order->id}}" />
                                    <input type="hidden" id="user_id-field" name="user_id" class="form-control" value="{{Auth::user()->id}}"  />
                                    <label for="attachment-field">Attachment</label>
                                    <input type="file" id="attachment-field" name="attachment" class="form-control" value="{{ old("attachment") }}"/><br>


                                </div>

                                <div class="col-xs-12">
                                 <!--  <div class="well well-sm"> -->
                                           
                                             <button type="submit" class="btn btn-success pull-right">Send Message</button>
                                       <!--  </div> -->
                                </div>
                            </div>
                            </form>

                                    @if(Auth::user()->user_type == "2" && $order->status_id == "5")
                                      <?php
                                        $counts =\App\Order::where('writer_id','=', Auth::user()->id)
                                                           ->where('status_id','=','1')->count();
                                      ?>
                                      @if($counts > 4 )
                                        <p>You cannot find another job as having limit 5 jobs in active currently</p>
                                      @else
                                         <a  href="{{ route('writers.applyjob', $order->id) }}"><button type="submit" class="btn btn-primary">Apply</button></a>
                                      @endif
                                    @endif

                            @if($order->writer_id == Auth::user()->id && $order->status_id == "1")    
                            <div class="row">
                            <form action="{{ route('writers.upload_doc') }}" method="POST" enctype="multipart/form-data">

                                <div class="col-xs-12">
                                
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <label for="message-field">Final Documents</label>
                                    <input type="hidden" id="order_id-field" name="order_id" class="form-control" value="{{$order->id}}" />
                                    <label for="attachment-field">Attachment</label>
                                    <input type="file" id="attachment-field" name="attachment" class="form-control" value="{{ old("attachment") }}"/><br>


                                </div>

                                <div class="col-xs-12">
                                 <!--  <div class="well well-sm"> -->
                                           
                                             <button type="submit" class="btn btn-success pull-right">Upload Document</button>
                                       <!--  </div> -->
                                </div>
                            </div>
                            @endif
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
