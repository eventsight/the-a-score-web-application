<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>The A score</title>
  <meta name="description" content="Theascore">
  <meta name="author" content="Theascore">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>


<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <link rel="stylesheet" href="css/custom.css">
 <link rel="stylesheet" href="css/styles.css">
<link rel="stylesheet" href="css/awselect.css">
<link rel="icon" href="{{ asset('/img/favicon.png') }}" >
</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
        <!-- Collapsed Hamburger -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span> 
              </button>

        <a class="navbar-brand" href="{{ url('/  ') }}"><b><img src="/img/logo.png" class="img-responsive" style="max-width: 160px;"></b></a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar">
            <li><a href="#main-bg" class="active"><b>首页</b></a></li>
            <li><a href="#section-whyus"><b>关于我们</b></a></li>
            <li><a href="#section-service"><b>论文服务</b></a></li>
            <li><a href="#section-howto"><b>如何运作</b></a></li>
            <li><a href="#section-feedback"><b>客户好评</b></a></li>
            <li><a href="{{ url('/faq')}}"><b>常見問題</b></a></li>
            <li><a href="#section-contact"><b>联系我们</b></a></li>
        </ul>
      <ul class="nav navbar-nav navbar-right">
        @if (Auth::check())
            @if(Auth::user()->user_type == 1 )
            <li><a href="{{ url('/home') }}">我的账户</a></li>
            @elseif (Auth::user()->user_type == 2 )
            <li><a href="{{ url('/dashboard-writer') }}">我的账户</a></li>
            @elseif (Auth::user()->user_type == 3 )
            <li><a href="{{ url('/dashboard-admin') }}">我的账户</a></li>
            @endif
        @else
            <li><a href="{{ url('/login') }}">登入</a></li>
            <li><a href="{{ url('/register') }}">注册</a></li>
        @endif

      </ul>
    </div>
  </div>
</nav>
        @yield('header')
        @yield('content')
   

</body>
</html>