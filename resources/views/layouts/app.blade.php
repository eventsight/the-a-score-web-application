<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>The A Score</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="shortcut icon" type="image/png" href="/img/favicon.png"/>
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.5/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.5/css/skins/_all-skins.min.css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link href="/css/custom.css" rel="stylesheet">

        @yield('css')


    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="{{url('/')}}" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><img src="/img/favicon.png" class="img-responsive"></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><img src="/img/logo.png" class="img-responsive"></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                
                            <?php

                            $user = \App\Order::where('user_id' , '=' , Auth::user()->id)->pluck('id');
                            $writer = \App\Order::where('writer_id' , '=' , Auth::user()->id)->pluck('id'); 

                            if(Auth::user()->user_type=='3'){
                                $messages=\App\Message:: whereNotIn('user_id', [Auth::user()->id])
                                                         ->where('approve_marker','=','3')
                                                        ->get();

                                //All Message which are not approved yet
                            } elseif(Auth::user()->user_type=='2') {
                              $messages=\App\Message::where('user_id', '!=', Auth::user()->id)
                                                        ->whereIn('order_id', $writer)
                                                        ->where('approve_marker','=','2')
                                                        ->get();
                             
                              //Message sent by user that haven't read by writer yet
                              //Order UserID
                            } elseif ( Auth::user()->user_type == '1') {
                              $messages=\App\Message::where('user_id','!=', Auth::user()->id)
                                                        ->whereIn('order_id', $user)
                                                        ->where('approve_marker','=','2')
                                                        ->get();
                              // $messages = \App\Message::where('user_id' , '=' , Auth::user()->id)->get();

                              //Message sent by writer that haven't read by user yet
                              //Order writer ID
                            }
  
                            ?> 
                            <li class="dropdown messages-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                  <i class="fa fa-envelope-o"></i>
                                  <span class="label label-success">{{$messages->count()}}</span>
                                </a>
                                <ul class="dropdown-menu">
                                  <li class="header">
                                  @if(Auth::user()->user_type=='3')
                                  You have {{$messages->count() }} unapproved messages
                                  @else
                                  You have {{$messages->count() }} unread messages
                                  @endif
                                  </li>
                                  <li>
                                    <!-- inner menu: contains the actual data -->
                                    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 200px;"><ul class="menu" style="overflow-y: scroll; width: 100%; height: 200px;">
                                      
                                    @if($messages->count())
                                      @foreach($messages as $message)
                                      <li><!-- start message -->
                                      @if(Auth::user()->user_type == 3)
                                        <a href="{{ route('messages.index') }}">
                                      @else
                                        <a href="{{ route('messages.read', $message->id) }}">
                                      @endif
                                          <div class="pull-left">
                                            <img src="{{asset('img/user-default.png')}}" class="img-circle" alt="User Image">
                                          </div>
                                          <h4>
                                            {{$message->order->title}}
                                            <small><i class="fa fa-clock-o"></i> {{explode(" ",$message->created_at)[0]}}</small>
                                          </h4>
                                          <p style="text-overflow: ellipsis;">{{substr($message->message,0,30)}}
                                          @if(strlen($message->message)>30)
                                          ...
                                          @endif</p>
                                        </a>
                                      </li>
                                      @endforeach
                                    @endif
                                      <!-- end message -->
                                    </ul><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 3px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                                  </li>
                                  
                                </ul>
                              </li>
                            <?php
                              if(Auth::user()->user_type=='3'){
                                $active_orders=\App\Order::where('user_id','=',Auth::user()->id)
                                                        ->where(function($query)
                                                          {
                                                            $query->where('status_id', '=', '1')
                                                                  ->orwhere('status_id', '=', '5')
                                                                  ->orwhere('status_id', '=', '6');
                                                          })->get();

                                //All Orders which are active, pending for writers, and pending for approval for all users

                              }elseif(Auth::user()->user_type=='2'){
                                $active_orders=\App\Order::where('user_id','=',Auth::user()->id)
                                                        ->where(function($query)
                                                          {
                                                            $query->where('status_id', '=', '1')
                                                                  ->orwhere('status_id', '=', '6');
                                                          })->get();

                                //Orders which are active and pending for approval for this writers

                              }else{
                                $active_orders=\App\Order::where('user_id','=',Auth::user()->id)
                                                        ->where(function($query)
                                                          {
                                                            $query->where('status_id', '=', '1')
                                                                  ->orwhere('status_id', '=', '2')
                                                                  ->orwhere('status_id', '=', '3');
                                                          })->get();

                                //All Orders which are active, pending for writers, and pending for approval for this user

                              }
                            ?> 
                            <li class="dropdown tasks-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                  <i class="fa fa-pencil-square-o"></i>
                                  <span class="label label-success">{{ $active_orders->count() }}</span>
                                </a>
                                <ul class="dropdown-menu">
                                  <li class="header">
                                  @if(Auth::user()->user_type == 1 )
                                  You have {{$active_orders->count()}} active orders
                                  @elseif (Auth::user()->user_type == 2 )
                                  You have {{$active_orders->count()}} job pending approval 
                                  @elseif (Auth::user()->user_type == 3 )
                                  You have {{$active_orders->count()}} orders in active , pending writer and pending approval
                                  @endif 

                                  </li>
                                  <li>
                                    <!-- inner menu: contains the actual data -->
                                    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 200px;"><ul class="menu" style="overflow-y: scroll; width: 100%; height: 200px;">
                                    @if($active_orders->count())
                                      @foreach( $active_orders as $active_order)
                                      <li><!-- Task item -->
                                        <a href="/vieworder/{{$active_order->id}}">
                                          <h3>
                                            {{$active_order->title}}
                                            <small class="pull-right">Order #{{sprintf("%06s",$active_order->id)}}</small><br>
                                            @if(Auth::user()->user_type!='1')
                                            <small class="label label-primary pull-right">{{$active_order->status->title}} </small>
                                            @endif
                                          </h3>
                                         
                                        </a>
                                      </li>
                                      @endforeach
                                    @endif
                                      <!-- end task item -->
                                      
                                    </ul><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 3px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                                  </li>
                                  <li class="footer">
                                    <a href="#">View all active orders</a>
                                  </li>
                                </ul>
                            </li>
                          
                            <?php
                                if(Auth::user()->user_type == 1  ){
                                  $unpaid_orders=\App\Order::where('user_id','=',Auth::user()->id)
                                                        ->where('status_id', '=', '4')
                                                        ->get();
                                }elseif(Auth::user()->user_type == 2 ){
                                  $unpaid_order= DB::table('active_orders')->where('writer_id','=',Auth::user()->id)
                                                                            ->where('status_id','=','6')
                                                                            ->pluck('order_id');
                                                                            // dd($unpaid_order);
                                  $unpaid_orders=\App\Order::whereIn('id', $unpaid_order)->get();
                                }elseif(Auth::user()->user_type == 3 ){
                                    $unpaid_order= DB::table('active_orders')->where('status_id','=','6')
                                                                              ->pluck('order_id');
                                                                            // dd($unpaid_order);
                                  $unpaid_orders=\App\Order::whereIn('id', $unpaid_order)->get();
                                }


                            ?> 
                            <li class="dropdown tasks-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                  <i class="fa fa-shopping-cart"></i>
                                  <span class="label label-danger">{{ $unpaid_orders->count() }}</span>
                                </a>
                                <ul class="dropdown-menu">
                                  <li class="header">
                                    @if(Auth::user()->user_type == 1 )
                                      You have {{$unpaid_orders->count()}} items in cart
                                    @elseif(Auth::user()->user_type == 2)
                                      You have {{$unpaid_orders->count()}} job in pending
                                    @elseif(Auth::user()->user_type == 3 )
                                      You have {{$unpaid_orders->count()}} orders in waiting to approve
                                    @endif
                                  </li>
                                  <li>
                                    <!-- inner menu: contains the actual data -->
                                    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 200px;"><ul class="menu" style="overflow-y: scroll; width: 100%; height: 200px;">
                                    @if($unpaid_orders->count())
                                      @foreach( $unpaid_orders as $unpaid_order)
                                      <li><!-- Task item -->
                                        <a href="/shoppingcart">
                                          <h3>
                                            {{$unpaid_order->title}}
                                            <small class="pull-right">Order #{{sprintf("%06s",$unpaid_order->id)}}</small><br>
                                            <small class="pull-right">$ {{number_format($unpaid_order->totalprice,2)}}</small>
                                            
                                          </h3>
                                         
                                        </a>
                                      </li>
                                      @endforeach
                                    @endif
                                      <!-- end task item -->
                                      
                                    </ul><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 3px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 3px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                                  </li>
                                  <li class="footer">
                                   @if(Auth::user()->user_type == 1 )
                                      <a href="{{url('/shoppingcart')}}">Checkout Now</a>
                                    @elseif(Auth::user()->user_type == 2)
                                      <a href="{{url('/job/pending')}}">Checkout Now</a>
                                    @elseif(Auth::user()->user_type == 3 )
                                    <a href="{{url('/order')}}">Checkout Now</a>
                                    @endif
                                  </li>
                                </ul>
                            </li>
                  

                           @if (Auth::check())

                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="padding:10px 15px">
                                     @if(Auth::user()->profile_pic == "default.jpg")
                                            <img src="{{asset('img/user-default.png')}}" class="img-circle" alt="User Image"  style="max-width: 30px ; max-height:30px">
                                            @else
                                          <img src="{{ asset('images/').'/'.Auth::user()->id.'/'.Auth::user()->profile_pic }}" class="img-circle" alt="User  Image" style="max-width: 30px ; max-height: 30px">
                                            @endif
                                    <span class="hidden-xs">{{Auth::user()->name}}</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                       <!--  <form action="{{ route('users.uploadpic')}}" enctype="multipart/form-data" method="POST">
                                         {{ csrf_field() }}
                                           <input id="profile-image-upload" class="hidden" type="file" name="profile"> -->
                                            <div id="profile-image" name="profile"> 
                                            @if(Auth::user()->profile_pic == "default.jpg")
                                              <img src="{{asset('img/user-default.png')}}" class="img-circle " alt="User Image" style="max-width: 100px ; max-height: 100px">
                                            @else
                                              <img src="{{ asset('images/').'/'.Auth::user()->id.'/'.Auth::user()->profile_pic }}" class="img-circle" alt="User  Image" style="max-width: 100px ; max-height: 100px">
                                            @endif
                                            </div>
                                        <p>
                                            {{Auth::user()->name}}
                                        </p>
                                           <!--     <input type="hidden" name="name" value="here">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-success">Upload</button>
                                                        <p>Click on the picture to change profile picture</p>
                                                    </div>
                                                </div>
                                            </form -->

                                    </li>
                                    <li><a href="/user/{{Auth::user()->id}}" style="color: blue">My profile</a></li>
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-right">
                                            <a href="{{url('logout')}}" class="btn btn-default btn-flat"
                                                onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">Sign out</a>
                                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            @endif
                        </ul>
                    </div>
                </nav>
            </header>
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- search form -->
                    <div class="sidebar-form">
                        @if(Auth::user()->user_type == 1 )
                            <a class="btn btn-success" href="/placeorder"><i class="glyphicon glyphicon-plus"></i> Place Order</a>
                        @elseif(Auth::user()->user_type == 2 )
                            <a class="btn btn-success" href="/job"><i class="glyphicon glyphicon-plus"></i> Get Job</a>
          
                        @endif    

                        
                    </div>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                    @if(Auth::user()->user_type == 1 )
                        <li class="header">MAIN NAVIGATION</li>
                        <li class="treeview" id="nav-dashboard"><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i><span>Dashboard</a></span></li>
                        <li class="treeview" id="nav-active"><a href="{{ url('/active') }}"><i class="fa fa-pencil-square-o"></i><span>Active Orders</span><span class="badge pull-right">{{\App\Order::where('user_id','=',Auth::user()->id)->where(function($query)
                        {
                            $query->where('status_id', '=', '1')
                                  ->orwhere('status_id', '=', '5')
                                  ->orwhere('status_id', '=', '6');
                            })->count()}}</span></a></li>
                        <li class="treeview" id="nav-completed"><a href="{{ url('/completed') }}"><i class="fa fa-check-square-o"></i><span>Completed Orders</span><span class="badge pull-right">{{\App\Order::where('user_id','=',Auth::user()->id)->where('status_id','=','2')->count()}}</span></a></li>
                        <li class="treeview" id="nav-revision"><a href="{{ url('/revision') }}"><i class="fa fa-commenting-o "></i><span>In Revision</span><span class="badge pull-right">{{\App\Order::where('user_id','=',Auth::user()->id)->where('status_id','=','3')->count()}}</span></a></li>
                        <li class="treeview" id="nav-unpaid"><a href="{{ url('/shoppingcart') }}"><i class="fa fa-shopping-cart "></i><span>Cart</span><span class="badge pull-right">{{\App\Order::where('user_id','=',Auth::user()->id)->where('status_id','=','4')->count()}}</span></a></li>

                        @elseif(Auth::user()->user_type == 2 )
<!-- Writer Dashboard -->
                        <li class="header">WRITERS</li>
                        <li class="treeview" id="nav-dashboard-writer"><a href="{{ url('/dashboard-writer') }}"><i class="fa fa-dashboard"></i><span>Dashboard</a></span></li>
                        
                        @php
                          $except =  DB::table('active_orders')->where('writer_id','=',Auth::user()->id)->where('status_id','=','6')->pluck('id'); 
                        @endphp
                        <li class="treeview" id="nav-job-get"><a href="{{ url('/job') }}"><i class="fa fa-list-ul"></i><span>Get Jobs</span><span class="badge pull-right">{{\App\Order::where('status_id','=','5')->whereNotIn('id', $except)->count()}}</span></a></li>

                        <li class="treeview" id="nav-job-active"><a href="{{ url('/job/active') }}"><i class="fa fa-pencil-square-o"></i><span>Active Jobs</span><span class="badge pull-right">{{\App\Order::where('writer_id','=',Auth::user()->id)->where('status_id','=','1')->count()}}</span></a></li>
                        
                        <li class="treeview" id="nav-job-pending"><a href="{{ url('/job/pending') }}"><i class="fa fa-spinner"></i><span>Pending Jobs</span><span class="badge pull-right">{{ DB::table('active_orders')->where('writer_id','=',Auth::user()->id)->where('status_id','=','6')->count()}}</span></a></li>
                        <li class="treeview" id="nav-job-completed"><a href="{{ url('/job/completed') }}"><i class="fa fa-check-square-o"></i><span>Completed Jobs</span><span class="badge pull-right">{{\App\Order::where('writer_id','=',Auth::user()->id)->where('status_id','=','2')->count()}}</span></a></li>
                        <li class="treeview" id="nav-job-revision"><a href="{{ url('/job/revision') }}"><i class="fa fa-commenting-o "></i><span>In Revision</span><span class="badge pull-right">{{\App\Order::where('user_id','=',Auth::user()->id)->where('status_id','=','3')->count()}}</span></a></li>

                        <li class="treeview" id="nav-job-message"><a href="{{ url('/writer/messages') }}"><i class="fa fa-envelope-o "></i><span>Messages</span><span class="badge pull-right">{{\App\Message::where('user_id','=',Auth::user()->id)  ->count()}}</span></a></li>

                        @elseif(Auth::user()->user_type == 3 )
<!-- Admin dahsboard -->
                        <li class="header">ADMINISTRATOR</li>
                        <li class="treeview" id="nav-dashboard-admin"><a href="{{ url('/dashboard-admin') }}"><i class="fa fa-dashboard"></i><span>Dashboard</a></span></li>

                        <li class="treeview" id="nav-orders"><a href="{{ url('/orders') }}"><i class="fa fa-pencil-square-o"></i><span>Orders</span><span class="badge pull-right">{{\App\Order::get()->count()}}</span></a></li>
                        <li class="treeview" id="nav-messages"><a href="{{ url('/messages') }}"><i class="fa fa-commenting-o "></i><span>Messages</span><span class="badge pull-right">{{\App\Message::get()->count()}}</span></a></li>
                        <li class="treeview" id="nav-users"><a href="{{url('/users')}}"><i class="fa fa-users"></i> <span>Users</span></a></li>
                        <li class="treeview" id="nav-others">
                          <a href="#">
                            <i class="fa fa-bars"></i>
                            <span>Others</span>
                            <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                            </span>
                          </a>
                          <ul class="treeview-menu" style="display: none;">
                            <li><a href="{{ url('/rates') }}">Rates</a></li>
                            <li><a href="{{ url('/statuses') }}">Statuses</a></li>
                              <li><a href="{{ url('/services') }}">Services</a></li>
                              <li><a href="{{ url('/levels') }}">Levels</a></li>
                              <li><a href="{{ url('/urgencies') }}">Urgencies</a></li>
                              <li><a href="{{ url('/pages') }}">Pages</a></li>
                              <li><a href="{{ url('/spacings') }}">Spacings</a></li>
                              <li><a href="{{ url('/citations') }}">Citations</a></li>
                              <li><a href="{{ url('/sources') }}">Sources</a></li>

                          </ul>
                        </li>
                        @endif
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <div class="content-wrapper">
                @yield('content')
            </div>
        </div>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class = 'AjaxisModal'>
            </div>
        </div>
        
        <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.5/js/app.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.3.5/js/demo.js"></script>
        <script> var baseURL = "{{ URL::to('/') }}"</script>
        <script src = "{{URL::asset('js/AjaxisBootstrap.js') }}"></script>
        <script src = "{{URL::asset('js/scaffold-interface-js/customA.js') }}"></script>
        <script src="https://js.pusher.com/3.2/pusher.min.js"></script>
        <script>
        // pusher log to console.
        Pusher.logToConsole = true;
        // here is pusher client side code.
        var pusher = new Pusher("{{env("PUSHER_KEY")}}", {
        encrypted: true
        });
        var channel = pusher.subscribe('test-channel');
        channel.bind('test-event', function(data) {
        // display message coming from server on dashboard Notification Navbar List.
        $('.notification-label').addClass('label-warning');
        $('.notification-menu').append(
            '<li>\
                        <a href="#">\
                                    <i class="fa fa-users text-aqua"></i> '+data.message+'\
                        </a>\
            </li>'
            );
        });

      

        </script>

        @yield('scripts')

    </body>
</html>
