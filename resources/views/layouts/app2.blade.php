<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'The A Score') }}</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/custom.css" rel="stylesheet">

    
    
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

    @yield('css')
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
          <div class="container">
            <div class="navbar-header">
                <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span> 
                      </button>

                <a class="navbar-brand" href="{{ url('/logout') }}"><b><img src="/img/logo.png" class="img-responsive" style="max-width: 160px;"></b></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar">
                    <li><a href="/#main-bg" class="active"><b>首页</b></a></li>
                    <li><a href="/#section-whyus"><b>关于我们</b></a></li>
                    <li><a href="/#section-service"><b>论文服务</b></a></li>
                    <li><a href="/#section-howto"><b>如何运作</b></a></li>
                    <li><a href="/#section-feedback"><b>客户好评</b></a></li></li>
                    <li><a href="/#section-contact"><b>联系我们</b></a></li>
                </ul>
              <ul class="nav navbar-nav navbar-right">
                @if (Auth::check())
                   
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/home') }}"><i class="fa fa-btn fa-sign-out">                              
                            </i>我的账户</a></li>

                            @if(Auth::user()->user_type!=1)
                            <li><a href="{{ url('/writers') }}"><i class="fa fa-btn fa-sign-out">                              
                            </i>Writer Panel</a></li>
                            @endif
                            @if(Auth::user()->user_type==3)
                            <li><a href="{{ url('/admin') }}"><i class="fa fa-btn fa-sign-out">                              
                            </i>Admin Panel</a></li>
                            @endif
                            <li>
                                    <a href="{{ url('/logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>

                            
                        </ul>
                    </li>
                @else
                    <li><a href="{{ url('/login') }}">登入</a></li>
                    <li><a href="{{ url('/register') }}">注册</a></li>
                @endif

              </ul>
            </div>
          </div>
        </nav>

    <div id="app">
        

        <div class="container">
            @yield('header')
            @yield('content')
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="/js/app.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <!-- Scripts -->

    @yield('scripts')
    


    <script type="text/javascript">
        $(document).ready(function(){
            $('.dropdown-toggle').dropdown()

        });
    </script>
</body>
</html>
