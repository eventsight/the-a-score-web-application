@extends('layouts.admin')

@section('subcontent')
<div class="row">
    <div class="col-xs-12">
    <div class="col-xs-12" style="margin-top:15px">
    <div class="box box-primary">
        <div class="box-header">
            <div class="col-xs-12 col-sm-6 col-md-9">
                <h3 class="pull-left">Rates</h3>
            </div>
   
            <div class="col-xs-12 col-sm-6 col-md-3">
                <a class="btn btn-success pull-right" href="{{ route('rates.create') }}">Add Rate</a>  
            </div>
        </div>

        <div class="box-body">


            @if($rates->count())
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>SERVICE_ID</th>
                        <th>URGENCY_ID</th>
                        <th>LEVEL_ID</th>
                        <th>RATE</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($rates as $rate)
                            <tr>
                                <td>{{$rate->id}}</td>
                                <td>{{$rate->service->title}}</td>
                    <td>{{$rate->urgency->title}}</td>
                    <td>{{$rate->level->title}}</td>
                    <td>{{$rate->rate}}</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('rates.show', $rate->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                    <a class="btn btn-xs btn-warning" href="{{ route('rates.edit', $rate->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <form action="{{ route('rates.destroy', $rate->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $rates->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>
</div>
</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
$("#nav-others").addClass("active");
</script>
@endsection