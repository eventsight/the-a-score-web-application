@extends('layouts.admin')

@section('subcontent')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">

    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> Rates / Edit #{{$rate->id}}</h1>
    </div>


    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('rates.update', $rate->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('service_id')) has-error @endif">
                       <label for="service_id-field">Service_id</label>
                    <input type="text" id="service_id-field" name="service_id" class="form-control" value="{{ is_null(old("service_id")) ? $rate->service_id : old("service_id") }}"/>
                       @if($errors->has("service_id"))
                        <span class="help-block">{{ $errors->first("service_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('urgency_id')) has-error @endif">
                       <label for="urgency_id-field">Urgency_id</label>
                    <input type="text" id="urgency_id-field" name="urgency_id" class="form-control" value="{{ is_null(old("urgency_id")) ? $rate->urgency_id : old("urgency_id") }}"/>
                       @if($errors->has("urgency_id"))
                        <span class="help-block">{{ $errors->first("urgency_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('level_id')) has-error @endif">
                       <label for="level_id-field">Level_id</label>
                    <input type="text" id="level_id-field" name="level_id" class="form-control" value="{{ is_null(old("level_id")) ? $rate->level_id : old("level_id") }}"/>
                       @if($errors->has("level_id"))
                        <span class="help-block">{{ $errors->first("level_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('rate')) has-error @endif">
                       <label for="rate-field">Rate</label>
                    <input type="text" id="rate-field" name="rate" class="form-control" value="{{ is_null(old("rate")) ? $rate->rate : old("rate") }}"/>
                       @if($errors->has("rate"))
                        <span class="help-block">{{ $errors->first("rate") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('rates.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });

$("#nav-rates").addClass("active");
</script>
@endsection