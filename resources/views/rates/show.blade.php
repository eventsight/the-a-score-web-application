@extends('layouts.admin')

@section('subcontent')
<div class="page-header">
        <h1>Rates / Show #{{$rate->id}}</h1>
        <form action="{{ route('rates.destroy', $rate->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('rates.edit', $rate->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>

    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static"></p>
                </div>
                <div class="form-group">
                     <label for="service_id">SERVICE_ID</label>
                     <p class="form-control-static">{{$rate->service_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="urgency_id">URGENCY_ID</label>
                     <p class="form-control-static">{{$rate->urgency_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="level_id">LEVEL_ID</label>
                     <p class="form-control-static">{{$rate->level_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="rate">RATE</label>
                     <p class="form-control-static">{{$rate->rate}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('rates.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
$("#nav-rates").addClass("active");
</script>
@endsection