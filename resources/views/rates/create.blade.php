@extends('layouts.admin')

@section('subcontent')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">

    <div class="page-header">
        <h1><i class="glyphicon glyphicon-plus"></i> Rates / Create </h1>
    </div>


    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('rates.store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('service_id')) has-error @endif">
                       <label for="service_id-field">Service_id</label>
                    <select name="service_id" class="form-control">
                      @foreach(\App\Service::get() as $service)
                      <option value="{{$service->id}}">{{$service->title}}</option>
                      @endforeach 
                    </select>
                    </div>
                    <div class="form-group @if($errors->has('urgency_id')) has-error @endif">
                       <label for="urgency_id-field">Urgency_id</label>
                    <select name="urgency_id" class="form-control">
                      @foreach(\App\Urgency::get() as $urgency)
                      <option value="{{$urgency->id}}">{{$urgency->title}}</option>
                      @endforeach 
                    </select>
                    </div>
                    <div class="form-group @if($errors->has('level_id')) has-error @endif">
                       <label for="level_id-field">Level_id</label>
                       <select name="level_id" class="form-control">
                      @foreach(\App\Level::get() as $level)
                      <option value="{{$level->id}}">{{$level->title}}</option>
                      @endforeach 
                    </select>
                    </div>
                    <div class="form-group @if($errors->has('rate')) has-error @endif">
                       <label for="rate-field">Rate</label>
                    <input type="text" id="rate-field" name="rate" class="form-control" value="{{ old("rate") }}"/>
                       @if($errors->has("rate"))
                        <span class="help-block">{{ $errors->first("rate") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('rates.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });

$("#nav-rates").addClass("active");
</script>
@endsection