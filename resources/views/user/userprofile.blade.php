@extends('layouts.app')

@section('content')

<div class="container" style="padding-top: 30px;">

  <h1>User Profile</h1>
    
</div>

  <div class="">
     <div class="container user-bg">
         <div style="padding: 19px;">

                    <div>
                     <form action="{{ route('users.uploadpic')}}" enctype="multipart/form-data" method="POST" id="uploadform" >
                        {{ csrf_field() }}
                        <input id="profile-upload" class="hidden" type="file" name="profile" onchange="submit_profilepic()">
                          <div id="profile" name="profile"> 
                            @if(Auth::user()->profile_pic == "default.jpg")
                              <img src="{{asset('img/user-default.png')}}" alt="User Image" style="max-width: 150px ; max-height: 150px">
                            @else
                              <img src="{{ asset('images/').'/'.Auth::user()->id.'/'.Auth::user()->profile_pic }}"  alt="User  Image" style="max-width: 150px ; max-height: 150px">

                            
                            @endif
                          </div>
                            <input type="hidden" name="name" value="here">
                            
                      </form>
                     </div>
                     <div class="row">
                        <h2 class="col-lg-4">{{ $user->name }}</h2> 
                     </div>                                                                
                     <div>
                        <div class="user-info-b">{{$user->email}}</div>
                      </div>

                    <div>
                        Placed Orders       : {{ $orders->count() }}<br>

                        Active orders       : {{ $orders->count() }}<br>
                        Completed orders    : {{ $orders->count() }}<br>
                        In revision orders  : {{ $orders->count() }}<br>
                    </div>
          </div>                     
    </div>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
     $('#profile').on('click', function() {
        $('#profile-upload').click();
      });

function submit_profilepic() {
    var form = document.getElementById('uploadform');
    form.submit();
}
</script>


@endsection