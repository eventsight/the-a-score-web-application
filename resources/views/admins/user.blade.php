@extends('layouts.admin')

@section('subcontent')
     

<div class="panel panel-default">
       <div class="panel-heading">Customer List <a href="{{url('users_download')}}" class="btn btn-success pull-right" >Download Users Excel</a></div>
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-12">
                @if($users->count())
                    @foreach($users as $user)
                     @if ($user->user_type != '2')
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="panel panel-default panel-primary">
                          <div class="panel-body">

                            <p>{{ $user->id}}</p>
                            <p>{{ $user->name}}</p>
                            <p>{{ $user->email}}</p>
                            <p>{{ $user->typeName()}}</p>

                           
                                <a class="btn btn-xs btn-primary" href="{{ route('admins.change_user', ['id'=>$user->id,'type'=>'2'] ) }}"><i class="glyphicon glyphicon-eye-open"></i> Make Writers</a>
                                 @if ($user->user_type == '1')
                                    <a class="btn btn-xs btn-primary" href="{{route('admins.change_user',['id'=>$user->id ,'type'=>'3'] ) }}"><i class="glyphicon glyphicon-eye-open"></i> Make Admin</a>
                                 @elseif ($user->user_type == '3')
                                    <a class="btn btn-xs btn-primary" href="{{route('admins.change_user',['id'=>$user->id ,'type'=>'1'] ) }}"><i class="glyphicon glyphicon-eye-open"></i> Delete Admin</a>
                                 @endif
                          </div>
                        </div>
                    </div>

                     @endif

                    @endforeach
                @else
                    <h3 class="text-center alert alert-info">You have no active order.</h3>
                @endif

            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
 <div class="panel-heading">Writer List </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-12">
                @if($users->count())
                    @foreach($users as $user)
                     @if ($user->user_type == '2')
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="panel panel-default panel-primary">
                          <div class="panel-body">

                            <p>{{ $user->id}}</p>
                            <p>{{ $user->name}}</p>
                            <p>{{ $user->email}}</p>
                            <p>{{ $user->typeName()}}</p>

                           <!--  @if ($user->user_type == '1')
                                <a class="btn btn-xs btn-primary" href="{{route('admins.change_user', $user->id) }}"><i class="glyphicon glyphicon-eye-open"></i> Make Writers</a>
                            @elseif ($user->user_type == '2 ')
                                <a class="btn btn-xs btn-primary" href="{{route('admins.change_user', $user->id) }}"><i class="glyphicon glyphicon-eye-open"></i> Normal User </a>
                            @endif -->
                          </div>
                        </div>
                    </div>

                     @endif

                    @endforeach
                @else
                    <h3 class="text-center alert alert-info">You have no active order.</h3>
                @endif

            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
$("#nav-active").addClass("active");
</script>
@endsection
