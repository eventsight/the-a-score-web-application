@extends('layout1')

@section('content')
<div class="container" style="width: 100% ; padding-right: 0px;">
<div id="main-bg" class="row" style="margin-right: 0px">

  <div  class="col-xs-12">

    <div class="col-sm-6 col-xs-10 text-center" id="main-text">
        <div class="row">
          <h1><b>寫作服務</b></h1>
          <p>想讓您的學術文章能更加精湛突出嗎?</p>
          <p>達到寫作上的零失誤嗎？<p>
          <p>The A Score團隊給予您性價比高的寫作優質服務！<p>
      
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="col-xs-10 .col-sm-2 col-xs-offset-1 col-sm-offset-3 col-md-6" >
        <div class="panel panel-dark" style="margin-top:100px;margin-bottom:100px;">

                <div class="panel-body">
                <form action="{{ route('orders.store') }}" method="POST">
                 <div class="col-xs-12 text-center">
          
                 <h2>价钱计算</h2>
                   
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                   
                    <select id="service_id" name="service_id" class="form-control" data-placeholder="选择服务">
                      @foreach(\App\Service::get() as $service)
                      <option value="{{$service->id}}" data-value="{{$service->value}}" {{(old("service_id")==$service->id)? 'selected':''}} >{{$service->title}}</option>
                      @endforeach
                    </select>

                
                    

                    <select id="urgency_id" name="urgency_id" class="form-control" data-placeholder="选择期限">
                      @foreach(\App\Urgency::get() as $urgency)
                      <option value="{{$urgency->id}}" data-value="{{$urgency->value}}" {{(old("urgency_id")==$urgency->id)? 'selected':''}} >{{$urgency->title}}</option>
                      @endforeach
                    </select>

                        
                    <!--   <select id="level_id" name="level_id" class="form-control" data-placeholder="选择难度">
                      @foreach(\App\Level::get() as $level)
                      <option value="{{$level->id}}" data-value="{{$level->value}}" {{(old("level_id")==$level->id)? 'selected':''}} >{{$level->title}}</option>
                      @endforeach
                    </select>
 -->

                    <select id="page_id" name="page_id" class="form-control" data-placeholder="选择页数">
                      @foreach(\App\Page::get() as $page)
                      <option value="{{$page->id}}" data-value="{{$page->value}}" {{(old("page_id")==$page->id)? 'selected':''}} >{{$page->page}}</option>
                      @endforeach
                    </select>
                    
                      <h3>从 $ <span id="price-per-page">2.00</span> 起</h3>

                    <div class="row">
                      <button type="submit" class="btn btn-primary btn-lg" style="background-color: #ffcc00 ; ">估價</button>
                    </div>
                  </div>
              </form>
            </div>
      </div>
    </div>
    </div>
    <div class="text-center"><p style="padding-bottom: 40px ;">歡迎註冊成為我們的會員，立即體驗The A Score的優質服務!</p></div>
  </div>

</div>

<div id="section-promise2">
  <div class="container text-center">
    <div class="row" style="margin-top:100px">
      <h2 class="padding-botton-25"><b>The A Score教育團隊對於我們每一位客戶的承諾</b><br></h2>
      <div class="col-sm-4 col-xs-12">
        <i class="fa fa-truck fa-5x"></i>
        <h4>100%准时/提前送达</h4>
        <p>The A Score设身处地为客户着想，一旦接受订单绝不会拖延及推迟</p>
      </div>
      <div class="col-sm-4 col-xs-12">
        <i class="fa fa-globe fa-5x"></i>
        <h4>北美本土定制写手及编辑</h4>
        <p>总部以及所有员工均位于美国境内，给您最地道的语言帮助</p>
      </div>
      <div class="col-sm-4 col-xs-12">
        <i class="fa fa-pencil-square-o fa-5x"></i>
        <h4>无懈可击的质量控制</h4>
        <p>The A Score交付客户前重重把关确保万无一失</p>
      </div>
    </div>

    <div class="row" style="margin-top:30px">
      <div class="col-sm-4 col-xs-12">
        <i class="fa fa-handshake-o fa-5x"></i>
        <h4>不满意14天内无限次免费校订</h4>
        <p>对收到的结果不满意，可指定原写手无限次校定直至您满意</p>
      </div>
      <div class="col-sm-4 col-xs-12">
        <i class="fa fa-clock-o fa-5x"></i>
        <h4>7天24小时不间断写作</h4>
        <p>无论您身处任何国家，The A Score为您所急</p>
      </div>
      <div class="col-sm-4 col-xs-12">
        <i class="fa fa-search  fa-5x"></i>
        <h4>免费TurnItIn检测</h4>
        <p>100%无语法错误，无不当引用，无不良抄袭！</p>
      </div>
    </div>

  </div>
</div>

<div id="section-whyus">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
            <h2 class="text-center"><b>关于我们</b></h2>
            <p class="text-center">The A Score是一家正式註冊的實體教育機構，我們提供專人的私人寫作訂製服務，並給予各國學生學習及傳授寫作技巧的交流平台，帶給我們的客戶高優質寫作服務，至次達到學術文章訂最佳化的服務終旨。</p>
      </div>
      </div>
  </div>
</div>

<div id="section-service">
    <div class="container">
        <h2 class="text-center"><b>论文服务</b></h2>
        <br>
        <br>
            <div class="col-md-6 col-sm-12 col-xs-12 text-center">
              <img src="img/service-1.jpg" class="img-responsive">
              <br>
              <br>
                <h3><b>定制写作</b></h3>
                <p>无需再为Due Time 临近，语言不地道而烦恼,TheAScore 超过100名英语<br>母语的专业写手，强大的学术团队24小时待命，以标准化定制写作程序和高<br>
                质量客户服务体验为基准，量身打造精准高质量的专业学术文章。</p><br>
          <!-- <button class="btn btn-warning btn-send">更多详情</button> -->
        </div>

            <div class="col-md-6 col-sm-12 col-xs-12 text-center">
                <img src="img/service-2.jpg" class="img-responsive">
                <br>
                <br>
                <h3><b>求职培训</b></h3>
                <p>毕业临近，是时候该将多年所积累的专业知识带入职业实践中。The A<br>Score拥有多位就职于全美领先企业人力资源部门的职场专家，从简历关键<br>词，模拟面试以及 networking 为切入点，帮助您各个击破职场专业技能。</p><br>
                <!-- <button class="btn btn-warning btn-send">更多详情</button> -->
            </div>
          </div>
         
    </div>
</div>



<div id="section-howto">
  <div class="container">
    <h2 class="text-center"><b>如何运作</b></h2>
    <div class="row">



  <div id=section-howto-arg class="col-md-offset-1 col-md-2 col-sm-3 col-xs-12 text-center">
        <h1><b>1.</b></h1>
        <h4>登录/注册网站用户</h4>
      </div>
      <div id=section-howto-arg class="col-md-2 col-sm-3 col-xs-12 text-center">
        <h1><b>2.</b></h1>
        <h4>前往“Order No”<br>
         页面下单,支持上传
         文件</h4>
     </div>
        
      <div id=section-howto-arg class="col-md-2 col-sm-3 col-xs-12 text-center">
        <h1><b>3.</b></h1>
        <h4>订单加入购物车,<br>
        结账付款
        </h4>
      </div>
      <div id=section-howto-arg class="col-md-2 col-sm-3 col-xs-12 text-center">
        <h1><b>4.</b></h1>
        <h4>接受订单，指派<br>
        专家开始写作</h4>
      </div>
          <div id=section-howto-arg class="col-md-2 col-sm-3 col-xs-12 text-center">
        <h1><b>5.</b></h1>
        <h4>订单完成,<br>
        传送文稿</h4>
      </div>  
        </div>
    </div>
</div>
 

<div id="section-feedback" class="container">
  <div class="row">
        <h2 class="text-center"><b>客户好评</b></h2>


    <br>


    <div style="position:relative">
      <img src="img/customer.jpg" style="width:100%;height:auto">

      <div id="testimonial" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#testimonial" data-slide-to="0" class="active"></li>
          <li data-target="#testimonial" data-slide-to="1"></li>
          <li data-target="#testimonial" data-slide-to="2"></li>
          <li data-target="#testimonial" data-slide-to="3"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <p>"1写手将一个很杂乱的论文改编成了极容易理解又有条有理的论文。若你需要这样的帮助，选择The A Score的专业团队绝对没错！"</p>
            <h6>陈文彬</h6>
          </div>

          <div class="item">
            <p>"2写手将一个很杂乱的论文改编成了极容易理解又有条有理的论文。若你需要这样的帮助，选择The A Score的专业团队绝对没错！"</p>
            <h6>陈文彬</h6>
          </div>
        
          <div class="item">
            <p>"3写手将一个很杂乱的论文改编成了极容易理解又有条有理的论文。若你需要这样的帮助，选择The A Score的专业团队绝对没错！"</p>
            <h6>陈文彬</h6>
          </div>

          <div class="item">
            <p>"4写手将一个很杂乱的论文改编成了极容易理解又有条有理的论文。若你需要这样的帮助，选择The A Score的专业团队绝对没错！"</p>
            <h6>陈文彬</h6>
          </div>
        </div>
      </div>

  
    </div>

  </div>
</div>


<div id="section-contact">
    <div class="container" style="margin-bottom: 10px">
    <div class="col-xs-12">
    <h2 class="text-center"><b>联系我们</b></h2>
    <div class="col-md-3">
      <div class="row">
        <div class="col-md-12">
          <h6><b>电子邮件</h6><h5>theascore@gmail.com</b></h5><br>
          <h6><b>客服电话</h6><h5>0333123456</b></h5><br>
        </div>
      </div>
    </div>

    <div class="col-md-4">
      <div class="row">
        <div class="col-md-6">
          <h6><b>社交圈链接</b></h6>
          <i class="fa fa-facebook-square fa-2x" aria-hidden="true"></i> &nbsp;
          <i class="fa fa-instagram fa-2x" aria-hidden="true"></i> &nbsp; 
          <i class="fa fa-youtube-play fa-2x" aria-hidden="true"></i> &nbsp;  
        </div>
      </div>
      </div>
        <div class="col-md-5">

                   <form id="gform" method="post" action="https://script.google.com/macros/s/AKfycbwWUtCAwKbjdljpETtNDzXm6uA22V-_ZbA2RRigJYZlqI7foXs/exec" role="form" onsubmit="myFunction()">
      <div class="messages"></div>
      <div class="controls">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label for="form_name"></label>
              <input id="form_name" type="text" name="name" class="form-control" placeholder="姓名 " required="required" data-error="Firstname is required.">
              <div class="help-block with-errors"></div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label for="form_email"></label>
              <input id="form_email" type="email" name="email" class="form-control" placeholder="电子邮件" required="required" data-error="Valid email is required.">
              <div class="help-block with-errors"></div>
            </div>
          </div>  
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label for="form_message"></label>
              <textarea id="form_message" name="message" class="form-control" placeholder="你的信息" rows="4" required="required" data-error="Please,leave us a message."></textarea>
              <div class="help-block with-errors"></div>
            </div>
          </div>
          <div class="col-md-12">
            <input type="submit" class="btn btn-warning" value="呈交">
           <!--  <button type="button" class="btn btn-warning">呈交 </button> -->
          </div>

        </div>
      </div>
      </form>
    </div>
    </div>
  </div>
</div>

<div class="container" style="margin-bottom: 15px">
  <div class="row text-right">
    <a id="back-to-top" href="#" class="btn btn-warning btn-lg back-to-right" role="button" title=><span class="glyphicon glyphicon-chevron-up"></span></a>
  </div>
</div>



<div id="section-copy" style="background-color: #000000">
  <div class="container" style="padding: 30px">
    <div class="row">
      
      <!-- <div class="col-md-3 col-sm-6 paddingtop-bottom">
        <div class="fb-page" data-href="https://www.facebook.com/facebook" data-tabs="" data-height="100px" data-small-header="false" style="margin-top:15px;" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
          <div class="fb-xfbml-parse-ignore">
            <blockquote cite="https://www.facebook.com/facebook"><a href="https://www.facebook.com/facebook">Facebook</a></blockquote>
            <blockquote cite="https://www.facebook.com/facebook"><a href="https://www.facebook.com/facebook">Facebook</a></blockquote>
          </div>
        </div>
      </div> -->

        <div class="col-md-2 col-sm-6 footer-col" style="margin-top: 20px">
          <h5 class="heading5">GENERAL LINKS</h5>
          <ul class="footer-ul">
            <li><a href="#main-bg" class="active"><b>首页</b></a></li>
              <li><a href="#section-whyus"><b>关于我们</b></a></li>
              <li><a href="#section-service"><b>论文服务</b></a></li>
              <li><a href="#section-howto"><b>如何运作</b></a></li>
              <li><a href="#section-feedback"><b>客户好评</b></a></li>              
              <li><a href="#section-faq"><b>常見問題</b></a></li>
              <li><a href="#section-contact"><b>联系我们</b></a></li>
          </ul>
        </div>
      
        <div class="col-xs-10 col-md-2 col-sm-6 paddingtop-bottom" style="margin-top: 20px">
          <h5 class="heading5">About Us</h5>
          <ul class="footer-ul">
            <li><a href="#"> Privacy Policy</a></li>
            <li><a href="#"> Terms & Conditions</a></li>
            <li><a href="#"> Cookie Policy</a></li>
          </ul>
        </div>
      <!-- </div> -->

      <div class="col-md-4 col-sm-6 footerright padding-left" style="margin-top: 20px; text-align: left;">

        <div class="logofooter"></div>
        <h5>Attention</h5>
          <p style="font-size: 10px;" class="color-grey">Using this service is LEGAL and IS NOT prohibited <br>
          by any university/college policies.<br><br>
          You are allowed to use the original model paper you<br>
          will receive in the following ways: </p>

          <ol class="color-grey">
            <li>As a source for additional understanding of the subject
            </li>
            <li>As a source of idea/reasoning for your own research (if referenced properly)
            </li>
            <li>
              For proper parasing (see your educational institution's defination of plagiarism and acceptable parapharase)
            </li>
            <li>
              Direct citiong (if referenced properly)
            </li>
          </ol>
        
      </div>

       <div>
        <div class="col-xs-10 .col-sm-2 col-md-4 " style="margin-top: 20px;text-align: justify;">
          

                <div>
                  <h5>Cookie Policy</h5>
                  <p style="font-size: 10px;" class="color-grey">We use cookies to give you the best possible experience on our website.By continuing to browse the site, you give consent for cookies to be used. For more detais, including how you can amend your preferences, please read our Cookie Policy</p>
                </div>

                <div>
                  <h5>We Accept</h5>
                  <img src="img/weaccept.png" class="img-responsive" style="max-width: 160px;">
                </div>
            
            
        
        </div>
      </div>


      <div class="col-md-12 col-sm-12 col-xs-12 text-center color-grey">
        <h6>© 2015—2017 Tan Group LLC. All Rights Reserved.</h6>

        <p class="color-grey">Disclaimer: One Freelance Limited - custom writing service that provides online custom written papers, such as term<br>
        papers, research papers, thesis papers, essays, dissertations and other custom writing services inclusive of research</p>
      </div>

    </div>
  </div>
</div>




<script type="text/javascript">
$(document).ready(function(){
     $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
        
        $('#back-to-top').tooltip('show');

});

</script>

 <script src="js/scripts.js"></script>
<script src="js/awselect.min.js"></script>
<script>
$(function(){
    $('ul.nav').find('a').click(function(){
        var href = $(this).attr('href');
        //console.log(href);
        var anchor = $(href).offset();
        console.log(anchor);
        $('body').animate({ scrollTop: anchor.top });
        return false;
    });

    $('select').awselect({
        background: "#282828", //the dark blue background
        active_background:"rgba(0,0,0,0.9)", // the light blue background
        placeholder_color: "#e9af10", // the light blue placeholder color
        placeholder_active_color: "#ffcc00", // the dark blue placeholder color
        option_color:"#ffcc00", // the option colors
        vertical_padding: "10px", //top and bottom padding
        horizontal_padding: "20px", // left and right padding,
        immersive: false // immersive option, demonstrated at the next example
    }) 
})



$('select').change(function(){
      pricepage();
    });

    
function pricepage(){
  var serviceval = $("#service_id").val();
  var urgencyval = $("#urgency_id").val();
  
  console.log(serviceval)

  var getrate = "/getrate/"+serviceval+"/"+urgencyval+"/";
  console.log(getrate);
  $.getJSON(getrate, function(jsonData){
    
    priceperpage=jsonData;
    console.log(jsonData)

    var pageval = $("#page_id").find(':selected').attr('data-value');
    if (pageval>=1){
      totalprice=(priceperpage*pageval)-2
    } else {
      totalprice=priceperpage-2;
    }
    $("#price-per-page").html(totalprice)
    
  })
  
}



</script>


<script>
function myFunction() {
    alert("The form was submitted");
    setTimeout(function () { window.location.reload(); }, 30);
}
</script>

    
@endsection