@extends('layouts.app')

@section('content')
    
<?php
$orders=\App\Order::where('user_id','=',Auth::user()->id)
                ->where('status_id','=','1')
                ->orderBy('id', 'desc')->paginate(6);
?>
<div class="row">
    <div class="col-sm-3 left-container">
   
        <div class="col-xs-12">
           
                @if($orders->count())
                    @foreach($orders as $order)
                    
                        <div class="box 
                            @if($order->status_id==4)
                            box-danger
                            @elseif($order->status_id==2)
                            box-success
                            @elseif($order->status_id==3)
                            box-warning
                            @else
                            box-primary
                            @endif

                        ">
                          <div class="box-header with-border"><p class="pull-left">{{$order->title}}</p>
                          
                          @if($order->status_id==4)
                            <small class="label label-danger pull-right">Unpaid</small>
                            @elseif($order->status_id==2)
                            <small class="label label-success pull-right">Completed</small>
                            @elseif($order->status_id==3)
                            <small class="label label-warning pull-right">In Revision</small>
                            @else
                            <small class="label label-primary pull-right">Active</small>
                            @endif



                          
                          </div>
                          <div class="box-body">
                            
                            <h6 class="text-left pull-left">Order #{{sprintf("%06s",$order->id)}}</h6>
                            <h6 class="text-right">{{$order->due_date}}</h6>
                            

                            <a class="btn btn-block btn-primary btn-xs" href="{{ route('orders.vieworder', $order->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                          </div>
                        </div>
                        
                    
                    @endforeach

                  
                @else
                    <h3 class="text-center alert alert-info">You have no unpaid order.</h3>
                @endif

        </div>
        
    </div>
    
    <div class="col-sm-9 right-container">
        @hasSection('subcontent')
        @yield('subcontent')
        @else
        Please Select An Order
        @endif
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
$("#nav-unpaid").addClass("active");
</script>
@endsection