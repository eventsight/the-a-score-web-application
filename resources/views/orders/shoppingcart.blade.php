@extends('layouts.app')
@section('css')
  <link href="/css/progress-wizard.min.css" rel="stylesheet">
  <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')
    

<div class="row">
    <div class="col-xs-12">
    <div class="col-sm-12 left-container" style="padding-right:15px;">
        <div class="box box-danger">
            <div class="box-header">
                <h3 class="pull-left">Shopping Cart</h3>
                
            </div>
                <div class="box-body">
                <?php 
                     $totalprice = 0;
                    $total_order = []; 
                ?>
                @if($orders=\App\Order::where('user_id','=',Auth::user()->id)->where('status_id','!=' , '8')
                            ->count()!=0)
                    <table class = "table table-hover">
                    <thead>
                        <th>Title</th>
                        <th>Order ID</th>
                        <th class="text-right">Amount</th>
                        <th class="text-right">Actions</th>
                    </thead>
                    <tbody>
                        <?php $orders=\App\Order::where('user_id','=',Auth::user()->id)
                            ->where('status_id','=','4')
                            ->orderBy('id', 'desc')->paginate(10); 
                           ?>
                        @foreach($orders as $order)
                        <?php $totalprice=$totalprice+$order->totalprice; 
                            $total_order[] = $order->id ; 
                        ?>
                        <tr>
                            <td>{{$order->title}}</td>
                            <td>Order #{{sprintf("%06s",$order->id)}}</td>
                            <td class="text-right">$ {{number_format($order->totalprice,2)}}</td>
                           
                            <td class="text-right">
                                
                                <a href="payorder/{{$order->id}}" class = 'btn btn-success btn-xs'><i class="fa fa-paypal" aria-hidden="true"></i> Pay Now</a>

                                <a href="cancel/{{$order->id}}" class = 'btn btn-default btn-xs'> Cancel</a>
                                
                            </td>
                        </tr>
                        @endforeach

                        <tr>
                            <td></td>
                            <td></td>
                            <td class="text-right"><b>$ {{number_format($totalprice,2)}}</b></td>
                            <td class="text-right"><!-- <a href="#" class = 'btn btn-success btn-xs'><i class="fa fa-paypal" aria-hidden="true"></i> Pay All Now</a> -->
                                <div id="paypal-button"></div>
                            </td>

                        </tr>
                    </tbody>
                </table>
                @else

                <div class="col-xs-12 text-center">
                  <i class="fa fa-times-circle fa-5x"></i>
                  <h2>You have no orders in this category. </h2>
                  <h4>Create new order now to get an A score!</h4>
                  <a href="{{url('/placeorder')}}" class = "btn btn-success"><i class="fa fa-plus fa-md" aria-hidden="true"></i> Place Order</a>
                </div>
               
                @endif

            </div>
        </div>
    </div>
   
    
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
$("#nav-unpaid").addClass("active");
</script>
    <script src="https://www.paypalobjects.com/api/checkout.js"></script>

    <script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
$('#paypal-loading').hide();
       
        paypal.Button.render({
            
            env: 'sandbox', // Optional: specify 'sandbox' environment
        
            client: {
                sandbox:    'ARv0BONehXF1Bl-YOqCCBBqzOv0tkzN50F0uCxBC8S-sFCGualEbPNSld1fKqhuAue_DG3i8zMZ8k7Ah',
                production: 'AS-0IC4BK5GzIEnADq4uI5ksXx_MTuXJwTnPL9R4owM2nnJEtWP5ZGObZjZYF1LAxywL1Ik4bna-deXL'
            },

            payment: function() {
                  
                var env    = this.props.env;
                var client = this.props.client;

                return paypal.rest.payment.create(env, client, {
                    transactions: [
                        {
                            amount: { total: '{{number_format($totalprice,2)}}', currency: 'USD' }
                        }
                    ]

                });
            },

            commit: true, // Optional: show a 'Pay Now' button in the checkout flow

            onAuthorize: function(data, actions) {
            
                // Optional: display a confirmation page here
                return actions.payment.execute().then(function() {

                    var settings = {
                      "async": true,
                      "crossDomain": true,
                      "url": "/paypal_status",
                      "method": "POST",
                      "headers": {
                        "content-type": "application/x-www-form-urlencoded",
                        "cache-control": "no-cache",
                      },
                      "data": {
                        "order_id": "{!!json_encode($total_order)!!}"
                      }
                    }

                    $.ajax(settings).done(function (response) {
                      response = JSON.parse(response);
                      if(response.success){
                        console.log(response);
                        window.location.href = "{!!url('/active/')!!}";
                      }else{
                          $('#paypal-button').fadeIn();
                          $('#paypal-loading').hide();
                      }
                    });
                });
            }

        }, '#paypal-button');
    </script>
@endsection