@extends( Auth::user()->user_type == '2'  ?   'orders.jobs' : 'orders.orders'   )

@section('subcontent')
       

        <div class="col-sm-12">
            <div class="box 

                @if($thisorder->status_id==4)
                box-danger
                @elseif($thisorder->status_id==2)
                box-success
                @elseif($thisorder->status_id==3)
                box-warning
                @else
                box-primary
                @endif

            ">
                <div class="box-header with-border"><p class="pull-left box-title">{{$thisorder->title}}</p>

                @if(Auth::user()->user_type == 1)

                  @if($thisorder->status_id==4)
                  <small class="label label-danger pull-right">Unpaid</small>
                  @elseif($thisorder->status_id==2)
                  <small class="label label-success pull-right">Completed</small>
                  @elseif($thisorder->status_id==3)
                  <small class="label label-warning pull-right">In Revision</small>
                  @else
                  <small class="label label-primary pull-right">Active</small>
                  @endif
                @else
                  @if($thisorder->status_id==4)
                  <small class="label label-danger pull-right">Unpaid</small>
                  @elseif($thisorder->status_id==2)
                  <small class="label label-success pull-right">Completed</small>
                  @elseif($thisorder->status_id==3)
                  <small class="label label-warning pull-right">In Revision</small>
                  @elseif($thisorder->status_id==5)
                  <small class="label label-warning pull-right bg-orange">Pending Writer</small>
                  @elseif($thisorder->status_id==6)
                  <small class="label label-warning pull-right bg-yellow">Pending Approve</small>
                  @else
                  <small class="label label-primary pull-right">Active</small>
                  @endif
                @endif


                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12">
                             @if (session('message'))
                                <div class="alert alert-info">
                                 {{ session('message') }}
                                </div>
                             @endif
                            <h2>{{$thisorder->title}}</h2>
                            <div class="panel panel-default">
                                <div class="panel-body">

                          
                                    <div class="col-sm-6 col-xs-12">
                                        <p>{{$thisorder->description}}</p>  
                                        <p>Due Date : {{$thisorder->due_date}}</p>
                                        <p>Order Date : {{$thisorder->date}}</p>
                                        @if(Auth::user()->user_type == 1 )
                                          @if($thisorder->status_id == 6 || $thisorder->status_id == 5 || $thisorder->status_id == 1)
                                          <p>Status : {{ $status}}</p>
                                          @else
                                          <p>Status : {{$thisorder->status->title}}</p> 
                                          @endif
                                        @else
                                          <p>Status : {{$thisorder->status->title}}</p> 
                                        @endif
                                        <h4>Price per page : $ {{$thisorder->priceperpage}}</h4>
                                        <h3>Total Price : $ {{$thisorder->totalprice}}</h3>
                                    </div>

                                    <div class="col-sm-6 col-xs-12">
                                        <p>Service : {{$thisorder->service->title}}</p>
                                        <p>Urgency : {{$thisorder->urgency->title}}</p>
                                        <p>Level : {{$thisorder->level->title}}</p>
                                        <p>Spacing : {{$thisorder->spacing->title}}</p>
                                        <p>Page : {{$thisorder->page->title}}</p>
                                        <p>Citation : {{$thisorder->citation->title}}</p>
                                        <p>Source : {{$thisorder->source->title}}</p>
                                    </div>
                                    @if($thisorder->user_id == Auth::user()->id)
                                      @if($thisorder->attachment != NULL && $thisorder->approve_final_doc == '2')
                                        <div class="panel-body">
                                          <div class="col-sm-6 col-xs-12">
                                            <div  class="well">
                                            <h3>Final document</h3>
                                            {{$thisorder->attachment}}
                                            <a href="{{ route('orders.downloads' , $thisorder->id) }}" class="btn btn-primary btn-sm btn-flat">Downloads</a> 
                                            @if(\Carbon\Carbon::now() < $thisorder->updated_at->addDays(7) || $thisorder->revision_count <= 3  )
                                              <a href="{{ route('orders.in_revision' , $thisorder->id) }}" class="btn btn-primary btn-sm btn-flat">In Revision</a>
                                            
                                              <a href="{{ route('orders.complete' , $thisorder->id) }}" class="btn btn-primary btn-sm btn-flat">Mark Complete</a>
                                            @endif
                                            </div>
                                          </div>
                                      
                                      </div>
                                      @endif
                                    @endif
                                </div>
                            </div>
                           @if(Auth::user()->user_type == 3 ) 
                             @if($thisorder->status_id == 6)
                                <?php 
                                  $job_applies = DB::table('active_orders')->where('order_id','=',$thisorder->id)->where('status_id','=',$thisorder->status_id)->orderBy('id','desc')->get();
                                ?>
                                <h3>Writer Applied</h3>
                                @foreach( $job_applies as $job_apply)
                                <?php 
                                  $name = \App\User::where('id','=',$job_apply->writer_id)->first();
                                ?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">{{$name->name}}</div>
                                    <div class="panel-body">  <a class="btn btn-xs btn-success" href="{{ route('orders.approvejob',  [ $id=$thisorder->id , $writer_id=$job_apply->writer_id]) }}"><i class="fa fa-check"></i> Approve</a>
                                    <a class="btn btn-xs btn-danger" href="{{ route('orders.decline',  [ $id=$thisorder->id , $writer_id=$job_apply->writer_id]) }}"><i class="fa fa-times"></i> Decline</a>
                                </div>
                                </div>
                                 
                                @endforeach
                              @endif
                            @endif
                            
                            <div class="row">
                                <div class="col-xs-12">
                             <!--    @if($thisorder->status_id == 4)
                                    <img id="paypal-loading" src="/img/ajax-loader.gif">
                                    <script src="https://www.paypalobjects.com/api/button.js?"
                                        data-merchant="braintree"
                                        data-id="paypal-button"
                                        data-button="checkout"
                                        data-color="gold"
                                        data-size="medium"
                                        data-shape="pill"
                                        data-button_type="button"
                                        data-button_disabled="false"
                                    </script>
                                @endif -->
                      

                                    @if(Auth::user()->user_type == "2" || Auth::user()->user_type==3)
                                         <?php
                                            $active = DB::table('active_orders')->where('writer_id','=',Auth::user()->id)->where('order_id','=',$thisorder->id)->get();
                                        ?>
                                      @if($active->isEmpty())
                                        <?php
                                          $counts =\App\Order::where('writer_id','=', Auth::user()->id)
                                                             ->where('status_id','=','1')->count();
                                        ?>
                                        @if($counts > 4 )
                                          <p>You cannot find another job as having limit 5 jobs in active currently</p>
                                        @else
                                           <a  href="{{ route('writers.applyjob', $thisorder->id) }}"><button type="submit" class="btn btn-xs btn-success">Apply</button></a>
                                         @endif
                                       @endif


                                      @if(($thisorder->writer_id == Auth::user()->id && ($thisorder->status_id == "1" || $thisorder->status_id == "3") ) || Auth::user()->user_type==3 )  

                                          
                                          
                                          @if($thisorder->attachment!='')
                                          <table class="table table-bordered">
                                          <tr>
                                          <td>
                                          <a href="/attachments/{{$thisorder->id}}/{{$thisorder->attachment}}" target="_BLANK"><i class="fa fa-file"></i> {{$thisorder->attachment}}</a>

                                            @if($thisorder->approve_final_doc==3)
                                            <small class="label label-danger pull-right">Declined</small>


                                            @elseif($thisorder->approve_final_doc==2)
                                            <small class="label label-success pull-right">Approved</small>
                                            @else
                                            <small class="label label-warning pull-right">Pending</small>
                                              @if(Auth::user()->user_type==3)
                                              </td>
                                              <td>
                                                <a class="btn btn-xs btn-success" href="{{ route('orders.approve_doc' , $thisorder->id )  }}">Accept</a>
                                                <a class="btn btn-xs btn-danger" href="{{ route('orders.reject_doc' , $thisorder->id )  }}">Reject</a>
                                              
                                              @endif
                                            @endif
                                            </td>

                                            
                                          </tr>
                                          </table>
                                          @endif
                                          

                                          <div class="row">
                                          <form action="{{ route('writers.upload_doc') }}" method="POST" enctype="multipart/form-data">

                                              <div class="col-xs-12">
                                              
                                                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                  <label for="message-field">@if($thisorder->attachment!='') Reupload @endif Final Documents</label>
                                                  <input type="hidden" id="order_id-field" name="order_id" class="form-control" value="{{$thisorder->id}}" />
                                                  <label for="attachment-field">Attachment</label>
                                                  <input type="file" id="attachment-field" name="attachment" class="form-control" value="{{ old("attachment") }}"/><br>


                                              </div>

                                              <div class="col-xs-12">
                                               <!--  <div class="well well-sm"> -->
                                                         
                                                           <button type="submit" class="btn btn-success pull-right">Upload Document</button>
                                                     <!--  </div> -->
                                              </div>
                                                @if(Auth::user()->user_type == 3 )
                                                  <a class="btn btn-warning pull-right" href="{{route('orders.edit' , $thisorder->id)}}"> Edit</a>
                                                @endif
                                          </div>
                                          </form>
                            @endif   
                                    @endif


                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <div class="box box-primary direct-chat direct-chat-warning">
                <div class="box-header with-border">
                  <h3 class="box-title">Messages</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <!-- Conversations are loaded here -->
                  <div class="direct-chat-messages">
                    <!-- Message. Default to the left -->

                    @php
                      if(Auth::user()->id == 1 ) {
                        $messages=\App\Message::where('order_id','=',$thisorder->id)->where('user_id','=', Auth::user()->id)
                                    ->where(function ($query) {
                                       $query->where('approve_marker', '!=', 3)
                                             ->where('approve_marker', '!=', 4);
                        })->get();
                      }else{
                        $messages=\App\Message::where('order_id','=',$thisorder->id)->where('user_id','=', Auth::user()->id)
                                 ->get();
                      }
                    @endphp
                    @foreach($messages as $message)
                    <div class="direct-chat-msg @if($message->user_id==Auth::user()->id) right @endif">
                      <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name @if($message->user_id==Auth::user()->id) pull-right @else pull-left @endif">{{$message->user->name}}</span>
                        <span class="direct-chat-timestamp @if($message->user_id==Auth::user()->id) pull-left @else pull-right @endif">{{$message->created_at}}</span>
                      </div>
                      <!-- /.direct-chat-info -->
                      <img class="direct-chat-img" src="{{asset('img/user-default.png')}}" alt="message user image"><!-- /.direct-chat-img -->
                      <div class="direct-chat-text">
                        {{$message->message}}

                        @if($message->approve_marker == 3)
                                  <a class="btn btn-success pull-right" href="">Edit</a>
                        @endif
                        @if($message->attachment!='')
                        <div class="attachment" style="">
                          <h4>Attachments:</h4>

                          <p class="filename pull-left">
                            {{$message->attachment}}
                          </p>

                          <div class="text-right">
                            <a href="/attachments/{{$thisorder->id}}/{{$message->attachment}}" target="_BLANK" class="btn btn-primary btn-sm btn-flat">Download</a>
                          </div>
                        </div>
                        @endif

                      </div>
                      <!-- /.direct-chat-text -->
                    </div>
                    <!-- /.direct-chat-msg -->
                    @endforeach
                    

                  </div>
                  <!--/.direct-chat-messages-->

                  
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                @if( ($thisorder->user_id == Auth::user()->id || $thisorder->writer_id == Auth::user()->id) && ($thisorder->status_id == '1' || $thisorder->status_id == '5' || $thisorder->status_id == '6' || $thisorder->status_id == '3' ) )
                  <form action="{{ route('messages.upload') }}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" id="order_id-field" name="order_id" class="form-control" value="{{$thisorder->id}}" />
                    <input type="hidden" id="user_id-field" name="user_id" class="form-control" value="{{Auth::user()->id}}"  />
                    <label id="attachment" for="attachment-field" class="btn btn-flat btn-primary"><i class="fa fa-paperclip"></i> Add Attachment</label>
                    <div class="input-group">
                      <input type="file" id="attachment-field" name="attachment" class="form-control" style="width: 0.1px;
                  }
    height: 0.1px;
    opacity: 0;
    overflow: hidden;
    position: absolute;
    z-index: -1;" value="{{ old("attachment") }}"/>
                      <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                          <span class="input-group-btn">
                            <button type="submit" class="btn btn-warning btn-flat">Send</button>
                          </span>
                    </div>
                  </form>

                  @endif
                </div>
                <!-- /.box-footer-->
              </div>


              

            
        </div>
 

@endsection
@section('scripts')
@if($thisorder->status_id == 4)
<!-- Load the client component. -->
<script src="https://js.braintreegateway.com/web/3.8.0/js/client.min.js"></script>

<!-- Load the PayPal component. -->
<script src="https://js.braintreegateway.com/web/3.8.0/js/paypal.min.js"></script>

<script type="text/javascript">
$('#paypal-loading').hide();
    // Fetch the button you are using to initiate the PayPal flow
var paypalButton = document.getElementById('paypal-button');

// Create a Client component

braintree.client.create({
  authorization: '{!!$clientToken = Braintree_ClientToken::generate()!!}'
}, function (clientErr, clientInstance) {
  // Create PayPal component
  braintree.paypal.create({
    client: clientInstance
  }, function (err, paypalInstance) {
    paypalButton.addEventListener('click', function () {
      // Tokenize here!
      $('#paypal-button').hide();
      $('#paypal-loading').fadeIn();
      paypalInstance.tokenize({
        flow: 'checkout', // Required
        amount: {!!number_format($thisorder->totalprice,2,'.','')!!}, // Required
        currency: 'USD', // Required
        locale: 'en_US',
        enableShippingAddress: false,
      }, function (err, tokenizationPayload) {
        // Tokenization complete
        // Send tokenizationPayload.nonce to server
        if(err){
              $('#paypal-button').fadeIn();
              $('#paypal-loading').hide();
        }else{
                    var settings = {
                      "async": true,
                      "crossDomain": true,
                      "url": "/paypal_trans",
                      "method": "POST",
                      "headers": {
                        "content-type": "application/x-www-form-urlencoded",
                        "cache-control": "no-cache",
                        "postman-token": "f313b3be-f5e6-1eed-fe48-2f7dba7575d7"
                      },
                      "data": {
                        "nonce": tokenizationPayload.nonce,
                        "amount": "{!!number_format($thisorder->totalprice,2,'.','')!!}",
                        "order_id": "{{$thisorder->id}}"
                      }
                    }

                    $.ajax(settings).done(function (response) {
                      response = JSON.parse(response);
                      if(response.success){
                        console.log("Success!");
                        location.reload();
                      }else{
                          $('#paypal-button').fadeIn();
                          $('#paypal-loading').hide();
                      }
                    });
        }

      });
    });
  });
});


</script>

@endif
<script>
  $('input[type=file]').change(function(){
    var filename = $(this).val().split('\\').pop();
     $('#attachment').html('<i class="fa fa-paperclip"></i> '+filename);
    console.log(filename);
  });

  $('#{{$thisorder->id}}').addClass('active-order');
</script>
@endsection
