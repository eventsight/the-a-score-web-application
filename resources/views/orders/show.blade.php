@extends('layouts.admin')

@section('subcontent')
<div class="page-header">
        <h1>Orders / Show #{{$order->id}}</h1>
        <form action="{{ route('orders.destroy', $order->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('orders.edit', $order->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>

    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static"></p>
                </div>
                <div class="form-group">
                     <label for="title">TITLE</label>
                     <p class="form-control-static">{{$order->title}}</p>
                </div>
                    <div class="form-group">
                     <label for="description">DESCRIPTION</label>
                     <p class="form-control-static">{{$order->description}}</p>
                </div>
                    <div class="form-group">
                     <label for="date">DATE</label>
                     <p class="form-control-static">{{$order->date}}</p>
                </div>
                    <div class="form-group">
                     <label for="status_id">STATUS_ID</label>
                     <p class="form-control-static">{{$order->status_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="service_id">SERVICE_ID</label>
                     <p class="form-control-static">{{$order->service_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="due_date">DUE_DATE</label>
                     <p class="form-control-static">{{$order->due_date}}</p>
                </div>
                    <div class="form-group">
                     <label for="urgency_id">URGENCY_ID</label>
                     <p class="form-control-static">{{$order->urgency_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="level_id">LEVEL_ID</label>
                     <p class="form-control-static">{{$order->level_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="spacing_id">SPACING_ID</label>
                     <p class="form-control-static">{{$order->spacing_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="page_id">PAGE_ID</label>
                     <p class="form-control-static">{{$order->page_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="citation_id">CITATION_ID</label>
                     <p class="form-control-static">{{$order->citation_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="source_id">SOURCE_ID</label>
                     <p class="form-control-static">{{$order->source_id}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('orders.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection
@section('scripts')
<script type="text/javascript">
$("#nav-orders").addClass("active");
</script>
@endsection