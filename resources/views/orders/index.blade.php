@extends('layouts.admin')

@section('subcontent')

<div class="row">
<div class="col-xs-12" style="margin-top:15px ; left: 15px;" id="orders">
<div class="box box-primary">
<div class="box-header">
    <div class="col-xs-12 col-sm-6 col-md-8">

        <h3 class="pull-left">Orders</h3>
    </div>
   
    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="col-md-4">
            <a class="btn btn-success" href="{{ url('/export') }}">Export to CSV file </a>  
        </div>
       <div class="col-md-8">
       @if (session('message'))
                    <div class="alert alert-danger">
                        {{ session('message') }}
                    </div>
                @endif
        <select class="form-control" name="name" id="status">
            <option name="id" value="0">View All Order</option>
            @foreach(\App\Status::get() as $status)
            <option name="id" value="{{$status->id}}" 
                @if(isset($search) && $search==$status->id)
                selected
                @endif
            >{{$status->title}}</option>
            @endforeach
        </select>
        </div>
       
    </div>
</div>
    <div class="box-body">
        
    @if(\App\Order::get()->count()!=0)
        <table class = "table table-hover" id="show_order">
        <thead>
            <th>Title</th>
            <th>Order ID</th>
            <th>Due Date</th>
            <th>Status</th>
            <th>Writer</th>
            <th>Final Document</th>
            <th>Actions</th>
        </thead>
        <tbody>
            
            @foreach($orders as $order)
            <tr>
                <td><a href="{{url('vieworder/'.$order->id)}}">{{$order->title}}
                    <?php
                        $messages = \App\Message::where('order_id','=',$order->id)->where('approve_marker','=',2)->first();
                    ?>
                    @if($messages)
                     <i class="fa fa-envelope-o "></i></a>
                     @endif
                     </td>
                <td>Order #{{sprintf("%06s",$order->id)}}</td>
                <td>{{$order->due_date}}</td>
                <td style="text-align:left">
                @if($order->status_id==4)
                <small class="label label-danger">Unpaid</small>
                @elseif($order->status_id==2)
                <small class="label label-success">Completed</small>
                @elseif($order->status_id==3)
                <small class="label label-warning">In Revision</small>
                @else
                <small class="label label-primary">{{$order->status->title}} </small>
                @endif
                <td>
                
                @if(DB::table('active_orders')->where('order_id','=',$order->id)->where('approve_status','!=','3')->count()>0)
                    <?php
                    $job_applies = DB::table('active_orders')->where('order_id','=',$order->id)->where('approve_status','!=','3')->get();
                    ?>

                    @foreach($job_applies as $job_apply)

                    <?php 
                      $writer = \App\User::where('id','=',$job_apply->writer_id)->first();
                    ?>
                    <small>{{$writer->name}}</small>
                  
                    @if($job_apply->approve_status==1)
                        <a class="btn btn-xs btn-success" onclick="return confirm('Are you confirm ?')" href="{{ route('orders.approvejob',  [ $id=$order->id , $writer_id=$job_apply->writer_id]) }}"><i class="fa fa-check"></i> Approve</a>
                    @endif
 
                    <a class="btn btn-xs btn-danger" onclick="return confirm('Are you sure?')" href="{{ route('orders.decline',  [ $id=$order->id , $writer_id=$job_apply->writer_id]) }}"><i class="fa fa-times"></i> Cancel</a>
                    @endforeach

                @else
                <!-- <a class="btn btn-xs btn-primary" href=""><i class="fa fa-times"></i> Delete Writer -->
                @if($order->status_id == 5 || $order->status_id == 1 || $order->status_id == 3 )
                <form action="{{ route('orders.assign_writer' , $order->id ) }}" onsubmit="return confirm('Do you really want to change writer ?');" >
                <select name="writer_id">
                    <?php 
                        $users = \App\User::where('user_type', '=' , '2')->get();
                    ?>
                    @foreach ($users as $user)
                    <option value="{{ $user->id }}" >{{ $user->name }}</option>
                    @endforeach
                </select>
                <input type="submit" name="Assign" class="btn btn-xs btn-primary">
                </form>
                @endif
                @endif
                </td>
               <td>
                @if($order->attachment)
                    <!-- {{$order->attachment}} -->
                    <a href="/attachments/{{$order->id}}/{{$order->attachment}}" target="_BLANK"><i class="fa fa-3x fa-file"></i></a>
                @endif
               </td>
                <td>
                    <a class="btn btn-xs btn-primary" href="{{ route('orders.vieworder', $order->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>

                    @if($order->status_id==4)
                    
                    <a class="btn btn-xs btn-success" href="{{ route('orders.changetopaid', $order->id) }}"><i class="fa fa-paypal"></i>Paid</a>

                    @endif       

                                @if($order->attachment != NULL && $order->approve_final_doc == 1 )
                                <a class="btn btn-xs btn-success" href="{{ route('orders.approve_doc' , $order->id )  }}">Accept</a>
                                <a class="btn btn-xs btn-danger" href="{{ route('orders.reject_doc' , $order->id )  }}">Reject</a>
                                @endif

                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {!! $orders->render() !!}
    </div>
    @else

    <div class="col-xs-12 text-center">
      <i class="fa fa-times-circle fa-5x"></i>
      <h2>No Order Found</h2>
      <h4>Create new order now to get an A score!</h4>
      <a href="{{url('/placeorder')}}" class = "btn btn-success"><i class="fa fa-plus fa-md" aria-hidden="true"></i> Place Order</a>
    </div>
   

    @endif


</div>
</div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">

$("select").change(function(){
    var status = $(this).val();
    // console.log($(this).val());

    $.ajax({
        url: "{{url('ordersstatus/') }}" , 
        type: 'GET',
        data: {status:status} , 
        dataType: 'html' , 
        success: function(data){
            console.log(data);
            $('#orders').html(data);
        }
    }) 


})


</script>   

@endsection
@section('scripts')
<script type="text/javascript">
$("#nav-orders").addClass("active");



</script>

@endsection