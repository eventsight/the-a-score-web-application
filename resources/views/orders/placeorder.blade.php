@extends('layouts.app')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.min.css"/ >
  <link href="/css/progress-wizard.min.css" rel="stylesheet">
@endsection
@section('content')

<div class="row">
  <div class="col-xs-12" style="margin-top:15px">
    <form action="{{ route('orders.store') }}" method="POST">
      
      <div class="col-xs-12">
          <ul class="progress-indicator">

            <li class="ongoing"> <span class="bubble"></span>  <h4> Step 1. </h4><p>Create Order</p>

            </li>
              
            <li> <span class="bubble"></span>  <h4> Step 2. </h4><p>Upload Document</p>
             </li>
          
            <li> <span class="bubble"></span>  <h4> Step 3. </h4><p>Pay With PayPal</p> </li>
              
          </ul>

      </div>
      

      

      <div class="col-xs-12">
          <div class="page-header">
          
              <div class="col-sm-8"><h3> Step 1 - Create Orders </h3></div>
              <div class="col-sm-4 text-right"><button type="submit" class="btn btn-success">Create Order</button></div>
            
          </div>
      </div>

      @include('error')

      <div class="col-xs-12">
      
        <div class="box box-primary">
        <div class="box-header with-border">Place Order</div>

          <div class="box-body">
                  
            <div class="col-md-6">
            
                  
                     
                  
              
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">

                  
                      <div class="form-group @if($errors->has('service_id')) has-error @endif">
                         <label for="service_id-field">Service</label>
                    

                      <select id="service_id" name="service_id" class="form-control">
                        @foreach(\App\Service::get() as $service)
                        <option value="{{$service->id}}" data-value="{{$service->value}}" {{(old("service_id")==$service->id)? 'selected':''}} >{{$service->title}}</option>
                        @endforeach
                      </select>

                         @if($errors->has("service_id"))
                          <span class="help-block">{{ $errors->first("service_id") }}</span>
                         @endif
                      </div>
                      
                      <div class="form-group @if($errors->has('urgency_id')) has-error @endif">
                         <label for="urgency_id-field">Urgency</label>
                  

                      <select id="urgency_id" name="urgency_id" class="form-control">
                        @foreach(\App\Urgency::get() as $urgency)
                        <option value="{{$urgency->id}}" data-value="{{$urgency->value}}" {{(old("urgency_id")==$urgency->id)? 'selected':''}} >{{$urgency->title}}</option>
                        @endforeach
                      </select>

                         @if($errors->has("urgency_id"))
                          <span class="help-block">{{ $errors->first("urgency_id") }}</span>
                         @endif
                      </div>
                      <div class="form-group @if($errors->has('level_id')) has-error @endif">
                         <label for="level_id-field">Level</label>
                    
                        <select id="level_id" name="level_id" class="form-control">
                        @foreach(\App\Level::get() as $level)
                        <option value="{{$level->id}}" data-value="{{$level->value}}" {{(old("level_id")==$level->id)? 'selected':''}} >{{$level->title}}</option>
                        @endforeach
                      </select>


                         @if($errors->has("level_id"))
                          <span class="help-block">{{ $errors->first("level_id") }}</span>
                         @endif
                      </div>
                      <div class="form-group @if($errors->has('spacing_id')) has-error @endif">
                         <label for="spacing_id-field">Spacing</label>
                      
                      <select id="spacing_id"  name="spacing_id" class="form-control">
                        @foreach(\App\Spacing::get() as $spacing)
                        <option value="{{$spacing->id}}" data-value="{{$spacing->value}}" {{(old("spacing_id")==$spacing->id)? 'selected':''}} >{{$spacing->title}}</option>
                        @endforeach
                      </select>



                         @if($errors->has("spacing_id"))
                          <span class="help-block">{{ $errors->first("spacing_id") }}</span>
                         @endif
                      </div>
                      <div class="form-group @if($errors->has('page_id')) has-error @endif">
                         <label for="page_id-field">Page</label>
                      
                      <select id="page_id" name="page_id" class="form-control">
                        @foreach(\App\Page::get() as $page)
                        <option value="{{$page->id}}" data-value="{{$page->value}}" {{(old("page_id")==$page->id)? 'selected':''}} >{{$page->page}} Pages</option>
                        @endforeach
                      </select>


                         @if($errors->has("page_id"))
                          <span class="help-block">{{ $errors->first("page_id") }}</span>
                         @endif
                      </div>
                      <div class="form-group @if($errors->has('citation_id')) has-error @endif">
                         <label for="citation_id-field">Citation</label>
                    

                      <select id="citation_id" name="citation_id" class="form-control">
                        @foreach(\App\Citation::get() as $citation)
                        <option value="{{$citation->id}}" data-value="{{$citation->value}}" {{(old("citation_id")==$citation->id)? 'selected':''}} >{{$citation->title}}</option>
                        @endforeach
                      </select>


                         @if($errors->has("citation_id"))
                          <span class="help-block">{{ $errors->first("citation_id") }}</span>
                         @endif
                      </div>
                      
                      <div class="form-group @if($errors->has('source_id')) has-error @endif">
                         <label for="source_id-field">Source</label>
                   

                      <select id="source_id" name="source_id" class="form-control">
                        @foreach(\App\Source::get() as $source)
                        <option value="{{$source->id}}" data-value="{{$source->value}}" {{(old("source_id")==$source->id)? 'selected':''}} >{{$source->title}}</option>
                        @endforeach
                      </select>


                         @if($errors->has("source_id"))
                          <span class="help-block">{{ $errors->first("source_id") }}</span>
                         @endif
                      </div>
                  
              
              
          </div>
          <div class="col-md-6">
            <div class="form-group @if($errors->has('title')) has-error @endif">
                         <label for="title-field">Price per page</label>
                        <h2 id="price-per-page">$ 99.00</h2>
                         @if($errors->has("title"))
                          <span class="help-block">{{ $errors->first("title") }}</span>
                         @endif
                      </div>

            <div class="form-group @if($errors->has('title')) has-error @endif">
                         <label for="title-field">Total Price</label>
                        <h2 id="total-price">$ 99.00</h2>
                         @if($errors->has("title"))
                          <span class="help-block">{{ $errors->first("title") }}</span>
                         @endif
                      </div>

            <div class="form-group @if($errors->has('title')) has-error @endif">
                         <label for="title-field">Title</label>
                      <input type="text" id="title-field" name="title" class="form-control" value="{{ old("title") }}" maxlength="250" required/>
                         @if($errors->has("title"))
                          <span class="help-block">{{ $errors->first("title") }}</span>
                         @endif
                      </div>
                      <div class="form-group @if($errors->has('description')) has-error @endif">
                         <label for="description-field">Description</label>
                      <textarea class="form-control" id="description-field" rows="3" name="description">{{ old("description") }}</textarea>
                         @if($errors->has("description"))
                          <span class="help-block">{{ $errors->first("description") }}</span>
                         @endif
                      </div>
          </div>
         
            
          </div>
          <div class="box-footer">
                     
              <a class="btn btn-link" href="{{ url('/home') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>

               <button type="submit" class="btn btn-success pull-right">Create Order</button>
          </div>


        </div>
      </div>

    </form>

  </div>
</div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.full.js"></script>
  <script src="/js/jquery.number.min.js"></script>
  <script>
    $('.date-picker').datepicker({
      format:'yyyy-mm-dd'
    });

    $('#due_date-field').datetimepicker({
      format:'Y-m-d H:i'
    });

    var priceperpage = 0;

    pricepage();

    $('select').change(function(){
      pricepage();
    });

    
    function pricepage(){
      var serviceval = $("#service_id").val();
      var urgencyval = $("#urgency_id").val();
      var levelval = $("#level_id").val();
      
      console.log(serviceval)

      var getrate = "/getrate/"+serviceval+"/"+levelval+"/"+urgencyval+"/";
      console.log(getrate);
      $.getJSON(getrate, function(jsonData){
        
        priceperpage=jsonData;
        $("#price-per-page").html("$ "+priceperpage)
        totalprice();
      })
      
    }

    function totalprice(){
      var spacingval = $("#spacing_id").find(':selected').attr('data-value');
      var pageval = $("#page_id").find(':selected').attr('data-value');
      var citationval = $("#citation_id").find(':selected').attr('data-value');
      var sourceval = $("#source_id").find(':selected').attr('data-value');
      var totalprice = (priceperpage*spacingval*pageval);
      $("#total-price").html("$ "+totalprice)
    }
  </script>
@endsection
