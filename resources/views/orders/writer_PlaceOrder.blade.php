@extends('layouts.app')

@section('content')
    

<div class="row">
    <div class="col-xs-12">
    <div class="col-sm-12 left-container" style="padding-right:15px;">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="pull-left">Apply For Job</h3>
                
            </div>
                <div class="box-body">
                @if (session('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif
                @if(\App\Order::where('status_id','=','5')->where('writer_id','=',NULL)->count()!=0 )
                    <table class = "table table-hover">
                    <thead>
                        <th>Title</th>
                        <th>Order ID</th>
                        <th class="text-right">Amount</th>
                        <th class="text-right">Actions</th>
                    </thead>
                    <tbody>
                        
                        @foreach($orders as $order)
                            <?php
                                $active = DB::table('active_orders')->where('writer_id','=',Auth::user()->id)->where('order_id','=',$order->id)->get();
                            ?>
                            @if($active->isEmpty())
                            <tr data-toggle="collapse" data-target="#show_{{$order->id}}" class="accordion-toggle">
                                <td>{{$order->title}}</td>
                                <td>Order #{{sprintf("%06s",$order->id)}}</td>
                                <td class="text-right">$ {{number_format($order->totalprice,2)}}</td>
                                <td class="text-right">
                                    <?php
                                        $counts =\App\Order::where('writer_id','=', Auth::user()->id)
                                                           ->where('status_id','=','1')->count();
                                      ?>
                                      @if($counts > 4 )
                                        <p>You cannot find another job as having limit 5 jobs in active currently</p>
                                      @else
                                         <a  href="{{ route('writers.applyjob', $order->id) }}"><button type="submit" class="btn btn-xs btn-success">Apply</button></a>
                                       @endif 
                                       <!--  <a  href="{{ route('orders.vieworder', $order->id) }}"><button type="submit" class="btn btn-xs btn-success">View</button></a> -->
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div id="show_{{$order->id}}" class="accordian-body collapse">
                                    Service : {{$order->service->title}}<br>
                                    Level : {{$order->level->title}}<br>
                                    Page : {{$order->page->page}}<br>
                                    Citation : {{$order->citation->title}}
                                    </div>
                                </td>
                            </tr>
                      
                            
                        @endif

                        @endforeach

                        
                    </tbody>
                </table>
                @else

                <div class="col-xs-12 text-center">
                  <i class="fa fa-times-circle fa-5x"></i>
                  <h2>No Order Found</h2>
                  <!-- <h4>Create new order now to get an A score!</h4> -->
                  <!-- <a href="{{url('/placeorder')}}" class = "btn btn-success"><i class="fa fa-plus fa-md" aria-hidden="true"></i> Place Order</a> -->
                </div>
               
                @endif

            </div>
        </div>
    </div>
   
    
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
$("#nav-job-get").addClass("active");
</script>
@endsection