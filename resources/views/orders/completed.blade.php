@extends('layouts.user')

@section('subcontent')

<div class="panel panel-default">
    <div class="panel-heading">Completed Order</div>


    <div class="panel-body">
        <div class="row">
            <div class="col-xs-12">
                @if($orders->count())
                    @foreach($orders as $order)
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="panel panel-default panel-success">
                          <div class="panel-heading"><p>{{$order->title}}</p></div>
                          <div class="panel-body">
                            <h6 class="text-right">Order #{{sprintf("%06s",$order->id)}}</h6>
                            <h5>{{$order->due_date}}</h5>
                            <p>{{$order->description}}</p>
                            <p>{{$order->service->title}}</p>
                            <p>{{$order->urgency->title}}</p>
                            <p>{{$order->level->title}}</p>
                            <p>{{$order->spacing->title}} Spacing</p>
                            <p>{{$order->page->value}} Pages</p>
                            <p>Created at {{$order->date}}</p>
                            <h5>$ {{$order->priceperpage}}</h5>
                            <h6>$ {{$order->totalprice}}</h6>
                            <a class="btn btn-xs btn-primary" href="{{ route('orders.vieworder', $order->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                          </div>
                        </div>
                        
                    </div>
                    @endforeach

                    {!! $orders->render() !!}
                @else
                    <h3 class="text-center alert alert-info">You have no completed order.</h3>
                @endif

            </div>
        </div>
    </div>
</div>
       
@endsection

@section('scripts')
<script type="text/javascript">
$("#nav-completed").addClass("active");
</script>
@endsection