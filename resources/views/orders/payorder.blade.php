@extends('layouts.app')

@section('css')
  <link href="/css/progress-wizard.min.css" rel="stylesheet">
  <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection


@section('content')

<div class="row">
  <div class="col-xs-12" style="margin-top:15px">
    
    <div class="col-xs-12">
        <ul class="progress-indicator">

          <li class="completed"> <span class="bubble"></span>  <h4><i class="fa fa-check-circle"></i> Step 1. </h4><p>Create Order</p>

          </li>

          <li class="completed"> <span class="bubble"></span>  <h4><i class="fa fa-check-circle"></i> Step 2. </h4><p>Upload Document</p>
           </li>

          <li class="ongoing"> <span class="bubble"></span>  <h4> Step 3. </h4><p>Pay With PayPal</p> </li>

        </ul>

    </div>
    

    <div class="col-xs-12">
        <div class="page-header">
         
            <div class="col-sm-8"><h3> Step 3 - Pay with PayPal </h3></div>

            <div class="col-sm-4 text-right">
                <a href="{{url('/shoppingcart')}}" class="btn btn-link">View Cart</a>


            </div>
         
        </div>
    </div>


    <div class="col-xs-12">



            <div class="box box-primary">
                <div class="box-header with-border">Order #{{sprintf("%06s",$order->id)}}</div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                        @php
                          $orders = [] ; 
                          $orders[] = $order->id ;
                        @endphp
                            <h2>{{$order->title}}</h2>
                            

                                    <div class="col-sm-6 col-xs-12">
                                        <p>{{$order->description}}</p>
                                        <p>Due Date : {{$order->due_date}}</p>
                                        <p>Order Date : {{$order->date}}</p>
                                        <p>Status : {{$order->status->title}}</p>
                                        <h4>Price per page : $ {{$order->priceperpage}}</h4>
                                        <h3>Total Price : $ {{$order->totalprice}}</h3>
                                    </div>

                                    <div class="col-sm-6 col-xs-12">
                                        <p>Service : {{$order->service->title}}</p>
                                        <p>Urgency : {{$order->urgency->title}}</p>
                                        <p>Level : {{$order->level->title}}</p>
                                        <p>Spacing : {{$order->spacing->title}}</p>
                                        <p>Page : {{$order->page->page}}</p>
                                        <p>Citation : {{$order->citation->title}}</p>
                                        <p>Source : {{$order->source->title}}</p>
                                    </div>
                               



                            

                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="col-xs-12 text-right">
                        <div id="paypal-button"></div> 
                    </div>
                </div>
            </div>

    </div>

  </div>
</div>

@endsection
@section('scripts')
    <script src="https://www.paypalobjects.com/api/checkout.js"></script>

    <script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
$('#paypal-loading').hide();
        console.log("1");
        paypal.Button.render({
            
            env: 'sandbox', // Optional: specify 'sandbox' environment
        
            client: {
                sandbox:    'ARv0BONehXF1Bl-YOqCCBBqzOv0tkzN50F0uCxBC8S-sFCGualEbPNSld1fKqhuAue_DG3i8zMZ8k7Ah',
                production: 'AS-0IC4BK5GzIEnADq4uI5ksXx_MTuXJwTnPL9R4owM2nnJEtWP5ZGObZjZYF1LAxywL1Ik4bna-deXL'
            },

            payment: function() {
                  
                var env    = this.props.env;
                var client = this.props.client;

                return paypal.rest.payment.create(env, client, {
                    transactions: [
                        {
                            amount: { total: '{!!number_format($order->totalprice,2,'.','')!!}', currency: 'USD' }
                        }
                    ]

                });
            },

            commit: true, // Optional: show a 'Pay Now' button in the checkout flow

            onAuthorize: function(data, actions) {
            
                // Optional: display a confirmation page here
                console.log(data);
                return actions.payment.execute().then(function() {

                    var settings = {
                      "async": true,
                      "crossDomain": true,
                      "url": "/paypal_status",
                      "method": "POST",
                      "headers": {
                        "content-type": "application/x-www-form-urlencoded",
                        "cache-control": "no-cache",
                      },
                      "data": {
                        "order_id": "{{json_encode($orders)}}"
                      }
                    }

                    $.ajax(settings).done(function (response) {
                      response = JSON.parse(response);
                      if(response.success){
                        console.log("Success!");
                        window.location.href = "{!!url('/vieworder/'.$order->id)!!}";
                      }else{
                          $('#paypal-button').fadeIn();
                          $('#paypal-loading').hide();
                      }
                    });
                });
            }

        }, '#paypal-button');
    </script>
@endsection

