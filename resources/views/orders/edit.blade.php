@extends('layouts.admin')

@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.min.css"/ >
  <link href="/css/progress-wizard.min.css" rel="stylesheet">
@endsection
@section('subcontent')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">

    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> Orders / Edit #{{$order->id}}</h1>
    </div>
      <div class="row">
        <div class="col-sm-12" style="padding-left: 40px;">

            <form action="{{ route('orders.update', $order->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('title')) has-error @endif">
                       <label for="title-field">Title</label>
                    <input type="text" id="title-field" name="title" class="form-control" value="{{ is_null(old("title")) ? $order->title : old("title") }}"/>
                       @if($errors->has("title"))
                        <span class="help-block">{{ $errors->first("title") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('description')) has-error @endif">
                       <label for="description-field">Description</label>
                    <textarea class="form-control" id="description-field" rows="3" name="description">{{ is_null(old("description")) ? $order->description : old("description") }}</textarea>
                       @if($errors->has("description"))
                        <span class="help-block">{{ $errors->first("description") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('date')) has-error @endif">
                       <label for="date-field">Date</label>
                    <input type="text" id="date-field" name="date" class="form-control date-picker" value="{{ is_null(old("date")) ? $order->date : old("date") }}"/>
                       @if($errors->has("date"))
                        <span class="help-block">{{ $errors->first("date") }}</span>
                       @endif
                    </div>
                     <div class="form-group @if($errors->has('totalprice')) has-error @endif">
                       <label for="totalprice-field">Total Price</label>
                    <input type="text" id="totalprice-field" name="totalprice" class="form-control" value="{{ is_null(old("totalprice")) ? $order->totalprice : old("totalprice") }}"/>
                       @if($errors->has("totalprice"))
                        <span class="help-block">{{ $errors->first("totalprice") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('status_id')) has-error @endif">
                       <label for="status_id-field">Status</label>
                    <!-- <input type="text" id="status_id-field" name="status_id" class="form-control" value="{{ is_null(old("status_id")) ? $order->status_id : old("status_id") }}"/>
                       @if($errors->has("status_id"))
                        <span class="help-block">{{ $errors->first("status_id") }}</span>
                       @endif -->

                        <select id="status_id-field" name="status_id" class="form-control">
                        @foreach(\App\Status::get() as $status)
                        <option value="{{$status->id}}" data-value="{{$status->value}}" {{(old("status_id")==$status->id)? 'selected':''}} >{{$status->title}}</option>
                        @endforeach
                      </select>
                    </div>

                    <div class="form-group @if($errors->has('service_id')) has-error @endif">
                       <label for="service_id-field">Service_id</label>
                        <select id="service_id" name="service_id" class="form-control">
                        @foreach(\App\Service::get() as $service)
                        <option value="{{$service->id}}" data-value="{{$service->value}}" {{(old("service_id")==$service->id)? 'selected':''}} >{{$service->title}}</option>
                        @endforeach
                        </select>

                         @if($errors->has("service_id"))
                          <span class="help-block">{{ $errors->first("service_id") }}</span>
                         @endif
                    </div>

                    <div class="form-group @if($errors->has('due_date')) has-error @endif">
                       <label for="due_date-field">Due_date</label>
                    <input type="text" id="due_date-field" name="due_date" class="form-control" value="{{ is_null(old("due_date")) ? $order->due_date : old("due_date") }}"/>
                       @if($errors->has("due_date"))
                        <span class="help-block">{{ $errors->first("due_date") }}</span>
                       @endif
                    </div>

                    <div class="form-group @if($errors->has('urgency_id')) has-error @endif">
                       <label for="urgency_id-field">Urgency_id</label>
                        <select id="urgency_id" name="urgency_id" class="form-control">
                        @foreach(\App\Urgency::get() as $urgency)
                        <option value="{{$urgency->id}}" data-value="{{$urgency->value}}" {{(old("urgency_id")==$urgency->id)? 'selected':''}} >{{$urgency->title}}</option>
                        @endforeach
                      </select>

                         @if($errors->has("urgency_id"))
                          <span class="help-block">{{ $errors->first("urgency_id") }}</span>
                         @endif
                    </div>

                    <div class="form-group @if($errors->has('level_id')) has-error @endif">
                       <label for="level_id-field">Level_id</label>
                    <select id="level_id" name="level_id" class="form-control">
                        @foreach(\App\Level::get() as $level)
                        <option value="{{$level->id}}" data-value="{{$level->value}}" {{(old("level_id")==$level->id)? 'selected':''}} >{{$level->title}}</option>
                        @endforeach
                      </select>


                         @if($errors->has("level_id"))
                          <span class="help-block">{{ $errors->first("level_id") }}</span>
                         @endif
                    </div>

                    <div class="form-group @if($errors->has('spacing_id')) has-error @endif">
                       <label for="spacing_id-field">Spacing_id</label>
                     <select id="spacing_id"  name="spacing_id" class="form-control">
                        @foreach(\App\Spacing::get() as $spacing)
                        <option value="{{$spacing->id}}" data-value="{{$spacing->value}}" {{(old("spacing_id")==$spacing->id)? 'selected':''}} >{{$spacing->title}}</option>
                        @endforeach
                      </select>



                         @if($errors->has("spacing_id"))
                          <span class="help-block">{{ $errors->first("spacing_id") }}</span>
                         @endif
                    </div>

                    <div class="form-group @if($errors->has('page_id')) has-error @endif">
                       <label for="page_id-field">Page_id</label>
                     <select id="page_id" name="page_id" class="form-control">
                        @foreach(\App\Page::get() as $page)
                        <option value="{{$page->id}}" data-value="{{$page->value}}" {{(old("page_id")==$page->id)? 'selected':''}} >{{$page->page}} Pages</option>
                        @endforeach
                      </select>


                         @if($errors->has("page_id"))
                          <span class="help-block">{{ $errors->first("page_id") }}</span>
                         @endif
                    </div>

                    <div class="form-group @if($errors->has('citation_id')) has-error @endif">
                       <label for="citation_id-field">Citation_id</label>
                     <select id="citation_id" name="citation_id" class="form-control">
                        @foreach(\App\Citation::get() as $citation)
                        <option value="{{$citation->id}}" data-value="{{$citation->value}}" {{(old("citation_id")==$citation->id)? 'selected':''}} >{{$citation->title}}</option>
                        @endforeach
                      </select>


                         @if($errors->has("citation_id"))
                          <span class="help-block">{{ $errors->first("citation_id") }}</span>
                         @endif
                    </div>

                    <div class="form-group @if($errors->has('source_id')) has-error @endif">
                       <label for="source_id-field">Source_id</label>
                    <select id="source_id" name="source_id" class="form-control">
                        @foreach(\App\Source::get() as $source)
                        <option value="{{$source->id}}" data-value="{{$source->value}}" {{(old("source_id")==$source->id)? 'selected':''}} >{{$source->title}}</option>
                        @endforeach
                      </select>


                         @if($errors->has("source_id"))
                          <span class="help-block">{{ $errors->first("source_id") }}</span>
                         @endif
                    </div>

                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('orders.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
      </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.full.js"></script>
  <script src="/js/jquery.number.min.js"></script>
  <script>
     $('.date-picker').datepicker({
      format:'yyyy-mm-dd'
    });

    $('#due_date-field').datetimepicker({
      format:'Y-m-d H:i'
    });

    var priceperpage = 0;

    pricepage();

    $('select').change(function(){
      pricepage();
    });

  $("#nav-orders").addClass("active");
</script>
  
@endsection