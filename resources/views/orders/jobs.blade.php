@extends('layouts.app')

@section('content')
    

<div class="row">
    <div class="col-sm-3 left-container">
   
        <div class="col-xs-12">
                @if($orders->count())
                 
                    @foreach($orders as $order)
                       <?php
                            $active_order=\App\Order::findOrFail($order->order_id);  
                       ?>
                     
                         <div class="box 
                            @if($active_order->status_id==4)
                            box-danger
                            @elseif($active_order->status_id==2)
                            box-success
                            @elseif($active_order->status_id==3)
                            box-warning
                            @else
                            box-primary
                            @endif

                        ">
                          <div class="box-header with-border"><p class="pull-left">{{$active_order->title}}</p>
                          
                          @if($order->status_id == '4')
                            <small class="label label-danger pull-right">Unpaid</small>
                            @elseif($order->status_id =='2')
                            <small class="label label-success pull-right">Completed</small>
                            @elseif($order->status_id =='3')
                            <small class="label label-warning pull-right">In Revision</small>
                            @else
                            <small class="label label-primary pull-right">Active</small>
                            @endif



                          
                          </div>
                          <div class="box-body">
                            
                            <h6 class="text-left pull-left">Order #{{sprintf("%06s",$active_order->id)}}</h6>
                            <h6 class="text-right">{{$active_order->due_date}}</h6>
                            

                            <a class="btn btn-block btn-primary btn-xs" href="{{ route('orders.vieworder', $active_order->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                            

                          </div>
                        </div>
                        
                    @endforeach
                </div>
        
    </div>
    
    <div class="col-sm-9 right-container">
        @hasSection('subcontent')
        @yield('subcontent')
        @else
        <div class="col-xs-12"><h3 class="text-center alert alert-info">Please Select an order.</h3></div>
        @endif
    </div>
</div>
                  
                @else
                    <h3 class="text-center alert alert-info">You have no order in this category.</h3>
                @endif

        
@endsection

