@extends('layouts.app')

@section('css')
  <link href="/css/progress-wizard.min.css" rel="stylesheet">
@endsection

@section('content')
<div class="row">
  <div class="col-xs-12" style="margin-top:15px">
    <form action="{{ route('messages.upload') }}" method="POST" enctype="multipart/form-data">
   
        <div class="col-xs-12">
            <ul class="progress-indicator">

              <li class="completed"> <span class="bubble"></span>  <h4><i class="fa fa-check-circle"></i> Step 1. </h4><p>Create Order</p>

              </li>
                
              <li class="ongoing"> <span class="bubble"></span>  <h4>Step 2. </h4><p>Upload Document</p>
               </li>
            
              <li class="bubble"> <span class="bubble"></span>  <h4> Step 3. </h4><p>Pay With PayPal</p> </li>
                
            </ul>

        </div>
    

    <div class="col-xs-12">
        <div class="page-header">
          
            <div class="col-sm-8"><h3> Step 2 - Upload Document </h3></div>

            <div class="col-sm-4 text-right">
            <!-- <a class="btn btn-link" href="{{ url('/payorder/'.$order->id) }}">Skip</a>   -->
            <button type="submit" class="btn btn-success">Upload Document</button></div>
          
        </div>
    </div>


    <div class="col-xs-12">
        
        

        <div class="box box-primary">
            <div class="box-header with-border">Order #{{sprintf("%06s",$order->id)}}</div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1>Upload Doc</h1>
                            <h2>{{$order->title}}</h2>
                            <div class="panel panel-default">
                                <div class="panel-body">
                           
                                    <div class="col-sm-6 col-xs-12">
                                        <p>{{$order->description}}</p>
                                        <p>Due Date : {{$order->due_date}}</p>
                                        <p>Order Date : {{$order->date}}</p>
                                        <p>Status : {{$order->status->title}}</p>
                                        <h4>Price per page : $ {{$order->priceperpage}}</h4>
                                        <h3>Total Price : $ {{$order->totalprice}}</h3>
                                    </div>

                                    <div class="col-sm-6 col-xs-12">
                                        <p>Service : {{$order->service->title}}</p>
                                        <p>Urgency : {{$order->urgency->title}}</p>
                                        <p>Level : {{$order->level->title}}</p>
                                        <p>Spacing : {{$order->spacing->title}}</p>
                                        <p>Page : {{$order->page->title}}</p>
                                        <p>Citation : {{$order->citation->title}}</p>
                                        <p>Source : {{$order->source->title}}</p>
                                    </div>
                                </div>
                            </div>
                        

                            <div class="row">
                            <?php
                                $messages=\App\Message::where('order_id','=',$order->id)->get();
                            ?>
                            @foreach($messages as $message)
                            <div class="col-xs-9 
                            @if($message->user_id==Auth::user()->id)
                            pull-right
                            @endif
                            ">
                                <div class="panel panel-default panel-success">
                                  <div class="panel-heading"><p>{{$message->user->name}}</p></div>
                                  <div class="panel-body">
                                    <p>{{$message->message}}</p>
                                    <p><a href="/attachments/{{$order->id}}/{{$message->attachment}}" target="_BLANK">{{$message->attachment}}</a></p>
                                    <h6 class="text-right">{{$message->created_at}}</h6>
                                  </div>
                                </div>
                            </div>
                            @endforeach
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <label for="message-field">Message</label>
                                    <textarea class="form-control" id="message-field" rows="3" name="message">{{ old("message") }}</textarea>
                                    <input type="hidden" id="order_id-field" name="order_id" class="form-control" value="{{$order->id}}" />
                                    <input type="hidden" id="user_id-field" name="user_id" class="form-control" value="{{Auth::user()->id}}"  />
                                    <label for="attachment-field">Attachment</label>
                                    <input type="file" id="attachment-field" name="attachment" class="form-control" value="{{ old("attachment") }}"/><br>


                                </div>

                                
                            </div>
                                 
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                                           
                    <!-- <a class="btn btn-link" href="{{ url('/payorder/'.$order->id) }}"> Skip</a> -->

                    <button type="submit" class="btn btn-success pull-right">Upload Document</button>
                        <a href="{{ url('/home') }}" class="btn btn-link pull-right">Skip</a>
                </div>

            </div>
            </div>
        </form>
    </div>
</div>

@endsection