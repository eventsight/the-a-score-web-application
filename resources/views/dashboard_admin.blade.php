@extends('layouts.app')

@section('content')

<div class="col-xs-12" style="margin-top: 15px;">
    <div class="row">
        <div class="col-lg-2 col-xs-4">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{\App\Order::where('status_id','=','1')->count()}}</h3>

              <p>Active Orders</p>
            </div>
            <div class="icon">
              <i class="fa fa-pencil-square-o"></i>
            </div>
            <a href="{{ url('/ordersstatus/1') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-2 col-xs-4">
          <!-- small box -->
          <div class="small-box bg-orange">
            <div class="inner">
              <h3>{{\App\Order::where('status_id','=','5')->count()}}</h3>

              <p>Pending Writers</p>
            </div>
            <div class="icon">
              <i class="fa fa-spinner"></i>
            </div>
            <a href="{{ url('/ordersstatus/5') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->


        <!-- ./col -->
        <div class="col-lg-2 col-xs-4">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{\App\Order::where('status_id','=','6')->count()}}</h3>

              <p>Pending Approval</p>
            </div>
            <div class="icon">
              <i class="fa fa-spinner"></i>
            </div>
            <a href="{{ url('/ordersstatus/6') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 col-xs-4">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{\App\Order::where('status_id','=','2')->count()}}</h3>

              <p>Completed</p>
            </div>
            <div class="icon">
              <i class="fa fa-check-square-o"></i>
            </div>
            <a href="{{ url('/ordersstatus/2') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-2 col-xs-4">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{\App\Order::where('status_id','=','3')->count()}}</h3>

              <p>In Revision</p>
            </div>
            <div class="icon">
              <i class="fa fa-commenting-o"></i>
            </div>
            <a href="{{ url('/ordersstatus/3') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-2 col-xs-4">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{\App\Order::where('status_id','=','4')->count()}}</h3>

              <p>Unpaid</p>
            </div>
            <div class="icon">
              <i class="fa fa-shopping-cart "></i>
            </div>
            <a href="{{ url('/ordersstatus/4') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
      </div>
</div>
<div class="row">
<div class="col-xs-12">
  <div class="col-sm-4">
  <div class="box box-primary">
  <div class="box-header">
      <h3 class="pull-left">Active Jobs</h3>
  </div>
      <div class="box-body overflow-scroll">
          
      @if($orders=\App\Order::where('status_id','=',1)
                  ->count()!=0)
          <table class = "table table-hover">
          <thead>
              <th>Title</th>
              <th>Order ID</th>
              <th>Due Date</th>
              <th>Status</th>
              <th>Actions</th>
          </thead>
          <tbody>
              <?php $orders=\App\Order::where('status_id','=',1)
                  ->orderBy('id', 'desc')->paginate(5); ?>
              @foreach($orders as $order)
              <tr>
                  <td>{{$order->title}}</td>
                  <td>Order #{{sprintf("%06s",$order->id)}}</td>
                  <td>{{$order->due_date}}</td>
                  <td style="text-align:left">
                  @if($order->status_id==4)
                  <small class="label label-danger">Unpaid</small>
                  @elseif($order->status_id==2)
                  <small class="label label-success">Completed</small>
                  @elseif($order->status_id==3)
                  <small class="label label-warning">In Revision</small>
                  @else
                  <small class="label label-primary">Active</small>
                  @endif
                  </td>
                  <td>
                      @if($order->status_id==4)
                      <a href="payorder/{{$order->id}}" class = 'btn btn-success btn-xs'><i class="fa fa-paypal" aria-hidden="true"></i> Pay Now</a>
                      @else
                      <a href="vieworder/{{$order->id}}" class = 'btn btn-primary btn-xs'><i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i> View</a>
                      @endif
                  </td>
              </tr>
              @endforeach
          </tbody>
      </table>
      @else

      <div class="col-xs-12 text-center">
        <i class="fa fa-times-circle fa-5x"></i>
        <h2>No Active Job Found</h2>
      </div>
     
      @endif

  </div>
  </div>
  </div>
  <div class="col-sm-4">
  <div class="box box-warning">
  <div class="box-header">
      <h3 class="pull-left">Pending Writers</h3>
      
  </div>
      <div class="box-body overflow-scroll">
          
      @if($orders=\App\Order::where('status_id','=',5)
                  ->count()!=0)
          <table class = "table table-hover">
          <thead>
              <th>Title</th>
              <th>Order ID</th>
              <th>Due Date</th>
              <th>Status</th>
              <th>Actions</th>
          </thead>
          <tbody>
              <?php $orders=\App\Order::where('status_id','=',5)
                  ->orderBy('id', 'desc')->paginate(5); ?>
              @foreach($orders as $order)
              <tr>
                  <td>{{$order->title}}</td>
                  <td>Order #{{sprintf("%06s",$order->id)}}</td>
                  <td>{{$order->due_date}}</td>
                  <td style="text-align:left">
                  @if($order->status_id==4)
                  <small class="label label-danger">Unpaid</small>
                  @elseif($order->status_id==2)
                  <small class="label label-success">Completed</small>
                  @elseif($order->status_id==3)
                  <small class="label label-warning">In Revision</small>
                  @elseif($order->status_id==5)
                  <small class="label bg-orange">Pending Writer</small>
                  @else
                  <small class="label label-primary">Active</small>
                  @endif
                  </td>
                  <td>
                      @if($order->status_id==4)
                      <a href="payorder/{{$order->id}}" class = 'btn btn-success btn-xs'><i class="fa fa-paypal" aria-hidden="true"></i> Pay Now</a>
                      @else
                      <a href="vieworder/{{$order->id}}" class = 'btn btn-primary btn-xs'><i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i> View</a>
                      @endif
                  </td>
              </tr>
              @endforeach
          </tbody>
      </table>
      @else

      <div class="col-xs-12 text-center">
        <i class="fa fa-times-circle fa-5x"></i>
        <h2>No Pending Job Found</h2>
        <h4>Get a new job now!</h4>
        
      </div>
     
      @endif

  </div>
  </div>
  </div>
  <div class="col-sm-4">
  <div class="box box-warning">
  <div class="box-header">
      <h3 class="pull-left">Pending Jobs</h3>
      
  </div>
      <div class="box-body overflow-scroll">
          
      @if( DB::table('active_orders')->where('status_id','=','6')->count())
          <table class = "table table-hover">
          <thead>
              <th>Title</th>
              <th>Order ID</th>
              <th>Due Date</th>
              <th>Status</th>
              <th>Actions</th>
          </thead>
          <tbody>
                      <?php $orders=\App\Order::where('status_id','=',6)->orderBy('id', 'desc')->paginate(5); ?>
                   @foreach($orders as $order)
              <tr>
                  <td>{{$order->title}}</td>
                  <td>Order #{{sprintf("%06s",$order->id)}}</td>
                  <td>{{$order->due_date}}</td>
                  <td style="text-align:left">
                  @if($order->status_id==4)
                  <small class="label label-danger">Unpaid</small>
                  @elseif($order->status_id==2)
                  <small class="label label-success">Completed</small>
                  @elseif($order->status_id==3)
                  <small class="label label-warning">In Revision</small>
                  @elseif($order->status_id==6)
                  <small class="label bg-yellow">Pending Approve</small>
                  @else
                  <small class="label label-primary">Active</small>
                  @endif
                  </td>
                  <td>
                      @if($order->status_id==4)
                      <a href="payorder/{{$order->id}}" class = 'btn btn-success btn-xs'><i class="fa fa-paypal" aria-hidden="true"></i> Pay Now</a>
                      @else
                      <a href="vieworder/{{$order->id}}" class = 'btn btn-primary btn-xs'><i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i> View</a>
                      @endif
                  </td>
              </tr>
              @endforeach
          </tbody>
      </table>
      @else

      <div class="col-xs-12 text-center">
        <i class="fa fa-times-circle fa-5x"></i>
        <h2>No Pending Job Found</h2>
        <h4>Get a new job now!</h4>
        
      </div>
     
      @endif

  </div>
  </div>
  </div>
  </div>
</div>


<div class="col-xs-12" style="margin-top: 15px;">
    <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{\App\Message::all()->count()}}</h3>

              <p>Messages</p>
            </div>
            <div class="icon">
              <i class="fa fa-pencil-square-o"></i>
            </div>
            <a href="{{ url('/messages') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-orange">
            <div class="inner">
              <h3>{{\App\Message::where('approve_marker','=',3)->count()}}</h3>

              <p>Pending Message Approval</p>
            </div>
            <div class="icon">
              <i class="fa fa-spinner"></i>
            </div>
            <a href="{{ url('/messages') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->


        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{\App\User::all()->count()}}</h3>

              <p>Users</p>
            </div>
            <div class="icon">
              <i class="fa fa-user"></i>
            </div>
            <a href="{{ url('/users') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3> {{\App\Order::where('status_id','=',1)->sum('totalprice')}}</h3>

              <p>Revenue</p>
            </div>
            <div class="icon">
              <i class="fa fa-money"></i>
            </div>
            <a href="{{ url('/orders') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
      </div>
</div>

<div class="row">
<div class="col-xs-12">
  <div class="col-sm-6">
  <div class="box box-primary">
  <div class="box-header">
      <h3 class="pull-left">Recent Messages</h3>
  </div>
  <div class="box-body">
          
      @if($messages=\App\Message::all()
                  ->count()!=0)
          <table class = "table table-hover">
          <thead>
              <th>ID</th>
              <th>ORDER</th>
              <th>USER</th>
              <th>MESSAGE</th>
              <th class="text-right">OPTIONS</th>
          </thead>
          <tbody>
              <?php $messages=\App\Message::where('approve_marker','=',1)
                  ->orderBy('id', 'desc')->paginate(5); ?>
              @foreach($messages as $message)
             <td>{{$message->id}}</td>
                                <td>
                                Order #{{sprintf("%06s",$message->order_id)}}<br>
                                <a href="vieworder/{{$message->order_id}}">{{$message->order->title}}</a>
                                </td>

                                <td>{{$message->user->name}}</td>
                                <td>{{$message->message}}
                                    @if($message->attachment!='')
                                    <a href="/attachments/{{$message->order_id}}/{{$message->attachment}}" target="_BLANK"><i class="fa fa-3x fa-file"></i></a>
                                    @endif
                                    </td>
                                
                            
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('messages.show', $message->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                    <a class="btn btn-xs btn-warning" href="{{ route('messages.edit', $message->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>

                                    @if($message->approve_marker == '3')
                                    <a class="btn btn-xs btn-primary" href="{{ route('messages.approve' , $message->id) }}"><i class="fa fa-check"></i> Approve</a>
                                    <a class="btn btn-xs btn-primary" href="{{ route('messages.decline' , $message->id) }}"><i class="fa fa-times"></i> Decline</a>
                                    @endif

                                </td>
                            </tr>
                        @endforeach
          </tbody>
      </table>
      @else

      <div class="col-xs-12 text-center">
        <i class="fa fa-times-circle fa-5x"></i>
        <h2>No Recent Message Found</h2>
      </div>
     
      @endif

  </div>
  </div>
  </div>
  <div class="col-sm-6">
  <div class="box box-warning">
  <div class="box-header">
      <h3 class="pull-left">Pending Messages</h3>
  </div>
      <div class="box-body">
          
      @if($messages=\App\Message::all()
                  ->count()!=0)
          <table class = "table table-hover">
          <thead>
              <th>ID</th>
              <th>ORDER</th>
              <th>USER</th>
              <th>MESSAGE</th>
              <th class="text-right">OPTIONS</th>
          </thead>
          <tbody>
              <?php $messages=\App\Message::where('approve_marker','=',3)
                  ->orderBy('id', 'desc')->paginate(5); ?>
              @foreach($messages as $message)
             <td>{{$message->id}}</td>
                                <td>
                                Order #{{sprintf("%06s",$message->order_id)}}<br>
                                <a href="vieworder/{{$message->order_id}}">{{$message->order->title}}</a>
                                </td>

                                <td>{{$message->user->name}}</td>
                                <td>{{$message->message}}
                                    @if($message->attachment!='')
                                    <a href="/attachments/{{$message->order_id}}/{{$message->attachment}}" target="_BLANK"><i class="fa fa-3x fa-file"></i></a>
                                    @endif
                                    </td>
                                
                            
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('messages.show', $message->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>

                                    @if($message->approve_marker == '3')
                                    <a class="btn btn-xs btn-success" href="{{ route('messages.approve' , $message->id) }}" onclick="return confirm('Are you confirm ?')" ><i class="fa fa-check"></i> Approve</a>
                                    <a class="btn btn-xs btn-warning" href="{{ route('messages.decline' , $message->id) }}" onclick="return confirm('Are you confirm ?')" ><i class="fa fa-times" ></i> Decline</a>
                                    @endif

                                </td>
                            </tr>
                        @endforeach
          </tbody>
      </table>
      @else

      <div class="col-xs-12 text-center">
        <i class="fa fa-times-circle fa-5x"></i>
        <h2>No Recent Message Found</h2>
      </div>
     
      @endif

  </div>
  </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript">
$("#nav-dashboard-admin").addClass("active");
</script>
@endsection