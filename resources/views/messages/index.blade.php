@extends('layouts.admin')

@section('subcontent')

<div class="row">
    <div class="col-xs-12">
    <div class="col-xs-12" style="margin-top:15px">
    <div class="box box-primary">
        <div class="box-header">
            <div class="col-xs-12 col-sm-6 col-md-9">
                <h3 class="pull-left">Messages</h3>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <select class="form-control" name="name" onchange="location = this.value;">
                    <option value="{{url('messages')}}">View All Message</option>
                    <!-- @foreach(\App\Status::get() as $status)
                    <option value="{{url('ordersstatus/'.$status->id)}}" 
                        @if(isset($search) && $search==$status->id)
                        selected
                        @endif
                    >{{$status->title}}</option>
                    @endforeach -->
                </select>
               
            </div>
        </div>
        <div class="box-body">
            @if($messages->count())
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>ORDER</th>
                            <th>USER</th>
                            <th>MESSAGE</th>
                            <th>STATUS</th>
                            <th>ACTION</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($messages as $message)
                            <tr>
                                <td>{{$message->id}}</td>
                                <td>
                                Order #{{sprintf("%06s",$message->order_id)}}<br>
                                <a href="vieworder/{{$message->order_id}}">{{$message->order->title}}</a>
                                </td>

                                <td>{{$message->user->name}}</td>
                                <td>{{$message->message}}
                                    @if($message->attachment!='')
                                    <a href="/attachments/{{$message->order_id}}/{{$message->attachment}}" target="_BLANK"><i class="fa fa-3x fa-file"></i></a>
                                    @endif
                                    </td>
                               
                                  <td style="text-align:left">
                                        @if($message->approve_marker==1)
                                        <small class="label label-danger">Read</small>
                                        @elseif($message->approve_marker==2)
                                        <small class="label label-success">Unread</small>
                                        @elseif($message->approve_marker==4)
                                        <small class="label label-warning">Decline</small>
                                        @else
                                        <small class="label label-primary">Pending Approve</small>
                                        @endif
                                 
                                    </td>
                                <td>
                                    @if($message->approve_marker == '3')
                                    <a class="btn btn-xs btn-success" onclick="return confirm('Are you confirm ?')" href="{{ route('messages.approve' , $message->id) }}"><i class="fa fa-check"></i> Approve</a>
                                    <a class="btn btn-xs btn-warning" onclick="return confirm('Are you confirm ?')" href="{{ route('messages.decline' , $message->id) }}"><i class="fa fa-times"></i> Decline</a>
                                    @endif

                                </td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('messages.show', $message->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                    <a class="btn btn-xs btn-warning" href="{{ route('messages.edit', $message->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>

                                  

                                    <form action="{{ route('messages.destroy', $message->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $messages->render() !!}
            @else

    <div class="col-xs-12 text-center">
      <i class="fa fa-times-circle fa-5x"></i>
      <h2>No Order Found</h2>
      <h4>Create new order now to get an A score!</h4>
      <a href="{{url('/placeorder')}}" class = "btn btn-success"><i class="fa fa-plus fa-md" aria-hidden="true"></i> Place Order</a>
    </div>
   

    @endif
</div>
</div>
</div>
</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$("#nav-messages").addClass("active");
</script>
@endsection