@extends('layouts.admin')

@section('subcontent')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">

    <div class="page-header">
        <h1><i class="glyphicon glyphicon-plus"></i> Messages / Create </h1>
    </div>


    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('messages.store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('message')) has-error @endif">
                       <label for="message-field">Message</label>
                    <textarea class="form-control" id="message-field" rows="3" name="message">{{ old("message") }}</textarea>
                       @if($errors->has("message"))
                        <span class="help-block">{{ $errors->first("message") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('order_id')) has-error @endif">
                       <label for="order_id-field">Order_id</label>
                    <input type="text" id="order_id-field" name="order_id" class="form-control" value="{{ old("order_id") }}"/>
                       @if($errors->has("order_id"))
                        <span class="help-block">{{ $errors->first("order_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('user_id')) has-error @endif">
                       <label for="user_id-field">User_id</label>
                    <input type="text" id="user_id-field" name="user_id" class="form-control" value="{{ old("user_id") }}"/>
                       @if($errors->has("user_id"))
                        <span class="help-block">{{ $errors->first("user_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('attachment')) has-error @endif">
                       <label for="attachment-field">Attachment</label>
                    <input type="text" id="attachment-field" name="attachment" class="form-control" value="{{ old("attachment") }}"/>
                       @if($errors->has("attachment"))
                        <span class="help-block">{{ $errors->first("attachment") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('messages.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });

$("#nav-messages").addClass("active");
</script>
@endsection