@extends('layouts.app')

@section('content')

<div class="col-xs-12" style="margin-top: 15px;">
    <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{\App\Message::where('user_id','=',Auth::user()->id)->where('approve_marker','=','3')->count()}}</h3>

              <p>Messages</p>
            </div>
            <div class="icon">
              <i class="fa fa-pencil-square-o"></i>
            </div>
          </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{\App\Message::where('user_id','=',Auth::user()->id)->where('approve_marker','=','4')->count()}}</h3>

              <p>Pending Approval</p>
            </div>
            <div class="icon">
              <i class="fa fa-spinner"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
    </div>

</div>
<div class="col-xs-12">
<div class="box box-primary">
<div class="box-header">
    <h3 class="pull-left">All Messages</h3>
</div>
    <div class="box-body">
        
    @if(\App\Message::where('user_id','=',Auth::user()->id)->count()!=0)
        <table class = "table table-hover">
        <thead>
            <th>Message</th>
            <th>Order ID</th>
            <th>Status</th>
            <th>Action</th>
        </thead>
        <tbody>
            <?php $messages=\App\Message::where('user_id','=',Auth::user()->id)
                ->orderBy('id')->paginate(10); ?>
            @foreach($messages as $message)
            <tr>
                <td>{{$message->message}}</td>
                <td>Order #{{sprintf("%06s",$message->id)}}</td>
                <td style="text-align:left">
                @if($message->approve_marker==1)
                <small class="label label-danger">Read</small>
                @elseif($message->approve_marker==2)
                <small class="label label-success">Unread</small>
                @elseif($message->approve_marker==4)
                <small class="label label-warning">Decline</small>
                @else
                <small class="label label-primary">Pending Approve</small>
                @endif
                </td>
                <td>
                    @if($message->status_id==4)
                    <a href="payorder/{{$message->id}}" class = 'btn btn-success btn-xs'><i class="fa fa-paypal" aria-hidden="true"></i> Pay Now</a>
                    @else
                    <a href="vieworder/{{$message->id}}" class = 'btn btn-primary btn-xs'><i class="glyphicon glyphicon-eye-open" aria-hidden="true"></i> View</a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @else

    <div class="col-xs-12 text-center">
      <i class="fa fa-times-circle fa-5x"></i>
      <h2>No Job Found</h2>
      <h4>Get a new job now!</h4>
      <a href="{{url('/placeorder')}}" class = "btn btn-success"><i class="fa fa-plus fa-md" aria-hidden="true"></i> Get Job</a>
    </div>
   
    @endif

</div>
</div>
</div>


@endsection

@section('scripts')
<script type="text/javascript">
$("#nav-job-message").addClass("active");
</script>
@endsection