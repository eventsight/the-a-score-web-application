@extends('layouts.admin')

@section('subcontent')
<div class="page-header">
        <h1>Messages / Show #{{$message->id}}</h1>
        <form action="{{ route('messages.destroy', $message->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('messages.edit', $message->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>

    <div class="row">
        <div class="col-md-12">
                
                <div class="form-group">
                     <label for="message">MESSAGE</label>
                     <p class="form-control-static">{{$message->message}}</p>
                </div>
                    <div class="form-group">
                     <label for="order_id">ORDER_ID</label>
                     <p class="form-control-static">{{$message->order_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="user_id">USER_ID</label>
                     <p class="form-control-static">{{$message->user_id}}</p>
                </div>
                    <div class="form-group">
                     <label for="attachment">ATTACHMENT</label>
                     <p class="form-control-static">{{$message->attachment}}</p>
                </div>

            <a class="btn btn-link" href="{{ route('messages.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
$("#nav-messages").addClass("active");
</script>
@endsection