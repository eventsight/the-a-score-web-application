@extends('layouts.writer')

@section('subcontent')


<div class="panel panel-default">
    <div class="panel-heading">Dashboard</div>

    <div class="panel-body">
        <div class="row">
            <div class="col-xs-12">
                @if($orders->count())
                    @foreach($orders as $order)
                    @if($order->writer_id == Auth::user()->id  &&  $order->status_id == 1)
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="panel panel-default 
                        @if($order->status_id==1)
                        panel-primary
                        @elseif($order->status_id==2)
                        panel-success
                        @elseif($order->status_id==3)
                        panel-warning
                        @elseif($order->status_id==4)
                        panel-danger
                        @endif
                        ">
                          <div class="panel-heading"><p>{{$order->title}}</p></div>
                          <div class="panel-body">
                            <h6 class="text-right">Order #{{sprintf("%06s",$order->id)}}</h6>
                            <h5>{{$order->due_date}}</h5>
                            <p>{{$order->description}}</p>
                            <p>{{$order->service->title}}</p>
                            <p>{{$order->urgency->title}}</p>
                            <p>{{$order->level->title}}</p>
                            <p>{{$order->spacing->title}} Spacing</p>
                            <p>{{$order->page->value}} Pages</p>
                            <p>Created at {{$order->date}}</p>
                            <h5>$ {{$order->priceperpage}}</h5>
                            <h6>$ {{$order->totalprice}}</h6>
                            <a class="btn btn-xs btn-primary" href="{{ route('writers.vieworder', $order->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                          </div>
                        </div>
                        
                    </div>
                    @endif
                    @endforeach

                    {!! $orders->render() !!}
                @else
                    <h3 class="text-center alert alert-info">You have no order currently.</h3>
                @endif

            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts')
<script type="text/javascript">
$("#nav-dashboard").addClass("active");
</script>
@endsection