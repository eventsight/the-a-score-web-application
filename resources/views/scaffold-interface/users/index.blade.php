@extends('layouts.app')
@section('content')
<section class="content">
<div class="box box-primary">
<div class="box-header">
	<h3>All Users</h3>
</div>
	<div class="box-body">
		<a href="{{url('/add_writer')}}" class = "btn btn-success"><i class="fa fa-plus fa-md" aria-hidden="true"></i> New</a>
		<form name="form" action="{{ route('admins.searchrole') }}" >
			Search By Role:
			<select name="filter-role" onchange="document.forms.form.submit()" >
				<option > </option>
				<option value="1">User</option>
				<option value="2">Writer</option>
				<option value="3">Admin</option>
			</select>
		</form>
		<form action="{{ route('admins.searchname') }}" >	
			Search by Name:<input type="text" name="user-name">
			<input type="submit" name="submit">
		</form>
		<table class = "table table-hover">
		<thead>
			<th>Name</th>
			<th>Email</th>
			<th>Roles</th>
			<th>Change Role</th>
			<th>Actions</th>
		</thead>
		<tbody>
			@foreach($users as $user)
			<tr>
				<td>{{$user->name}}</td>
				<td>{{$user->email}}</td>
				<td>
				@if($user->user_type == '1')
					<small class = 'label label-default'>User</small>
				@elseif($user->user_type == '2')
					<small class = 'label label-default'>Writer</small>
				@elseif($user->user_type == '3')
					<small class = 'label label-default'>Admin</small>
				@else
					<small class = 'label label-default'>No Roles</small>
				@endif
				</td>
				<td>
				@if($user->user_type == '1')
					<a href="{{ route('admins.change_user',[ $id=$user->id , $type=2]) }}"><small class = 'label bg-blue'>Writer</small></a>
					<a href="{{ route('admins.change_user',[ $id=$user->id , $type=3]) }}"><small class = 'label bg-blue'>Admin</small></a>
				@elseif($user->user_type == '2')
					<a href="{{ route('admins.change_user',[ $id=$user->id , $type=1]) }}"><small class = 'label bg-blue'>User</small></a>
					<a href="{{ route('admins.change_user',[ $id=$user->id , $type=3]) }}"><small class = 'label bg-blue'>Admin</small></a>
				@elseif($user->user_type == '3')
					<a href="{{ route('admins.change_user',[ $id=$user->id , $type=1]) }}"><small class = 'label bg-blue'>User</small></a>
					<a href="{{ route('admins.change_user',[ $id=$user->id , $type=2]) }}"><small class = 'label bg-blue'>Writer</small></a>
				@endif
				</td>
				<td>
					<!-- <a href="{{url('/users/edit')}}/{{$user->id}}" class = 'btn btn-primary btn-sm'><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> -->
					<a href="{{url('users/delete')}}/{{$user->id}}" class = "btn btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
</div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
$("#nav-users").addClass("active");
</script>
@endsection
